package testscripts;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.text.ParseException;


import testcases.TestCase;
import jxl.read.biff.BiffException;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;


public class Keyword {
    public static String methodReturnResult = null;

    /*****************************
     * DischargeComplete
     * 
     * @throws WriteException
     * @throws RowsExceededException
     * 
     * @throws ParseException
     * 
     * @throws FindFailed
     *****************************************/

    public static String verifyPrimarycontactofapatientpreparesPML()  //1
	    throws BiffException, InterruptedException, IOException, RowsExceededException, WriteException {

	return TestCase.verifyPrimarycontactofapatientpreparesPML();

	
    }
    
        
    public static String verifySubscriberofapatientpreparesthePMLchoice()  //2
    	    throws BiffException, InterruptedException, IOException, RowsExceededException, WriteException, NoSuchAlgorithmException {

    	return TestCase.verifySubscriberofapatientpreparesthePMLchoice();

    	
        }
    public static String verifySimulatethereceivalofthepatientchoicefromPML()  //3
    	    throws BiffException, InterruptedException, IOException, RowsExceededException, WriteException, NoSuchAlgorithmException {

    	return TestCase.verifySimulatethereceivalofthepatientchoicefromPML();

    	
        }
        
}
