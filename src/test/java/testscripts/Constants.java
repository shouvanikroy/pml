/*
 * Constants.java contains all the constants need for the test  
 */

package testscripts;

public class Constants {

	public static final String EXPECTEDEDDALERTMESSAGE = "This date is in the past. Is this correct?";
	public static final String UPDATEREFERRALTEXT = "Update Booking(s) for";
	public static final String EXPECTEDUNCONNECTEDPROVIDERSTATUS = "QuickCase Pending";
	public static final String DISABLEDTEXT = "disabled";
	public static final String EXPECTEDERRORMESSAGE = "Please enter message here.";
	public static final String UNCONNECTEDPROVIDERSENDMESSAGEALERT = "Message can be sent to connected providers only.";
	public static final String LOGOUTMESSAGE = "You are now logged out.";
	public static final String EXPECTED_FORM_ALERT_MESSAGE = "You have opened a form. Before you leave this screen, you must save and close the form.";
	public static final String MESSAGE_ON_CANCELREFERRALS_CONFIRMATION_DIALOG = "Are you sure you want to cancel the selected referral(s)?\nNote that your selection includes booked provider(s).";
	public static final String MESSAGE_ON_MOVE_TO_WORKBOOK_OVERLAY = "Are you sure you want to move and pin this patient record to your Workbook?";
	public static final String TRAINING_PATIENTS_CREATED_SUCCESS_MSG= "Training patients created";
	public static final String TRAINING_PATIENTS_REMOVED_SUCCESS_MSG= "Training patients removed";
	public static final String ADD_PATIENT_SUCCESS_MSG = "You have successfully created the patient !";
}
