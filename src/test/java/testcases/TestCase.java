package testcases;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;

import org.openqa.selenium.By;

import jxl.read.biff.BiffException;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import testscripts.DischargeCompleteBaseLibrary;
import testscripts.DriverScript;
public class TestCase extends DriverScript{
	public static String toConvert; 
	public static String exchangeKey;
	
	
	
	public static String verifySimulatethereceivalofthepatientchoicefromPML()
			    throws BiffException, InterruptedException, IOException, RowsExceededException, WriteException, NoSuchAlgorithmException {

			APPLICATION_LOGS.debug(
				"Executing test case :  Simulate the receival of the patient choice from PML for the user being setup with notifications.");

			// Get the values from Excel sheet
			String dischargeCompleteHospitalName = testData.getCellData("DischargeComplete",
				"Discharge Complete Hospital Name", 4);
			
			String facilityId = testData.getCellData("loginToDischargeCentral", "FacilityID", 5);
			String dischargeConfig = testData.getCellData("loginToDischargeCentral", "ConfigName", 5);
			String dischargeConfigValue = testData.getCellData("loginToDischargeCentral", "ConfigValue", 5);
			String patientFirstName = testData.getCellData("DischargeComplete", "Patient First Name", 4);
			String patientLastName = testData.getCellData("DischargeComplete", "Patient Last Name", 4);
			String param_name=testData.getCellData("loginToDischargeCentral", "Parameter_Name", 5);
			
		
			// To verify and set show_ui_changes_discharge_complete config to true
			methodReturnResult = DischargeCompleteBaseLibrary.updateFacilityConfig(facilityId, dischargeConfig,
				dischargeConfigValue);
			if (methodReturnResult.contains(failTest)) {
			    return methodReturnResult;
			}

			// Navigate to Discharge Central, Login and Select the hospital from
			// dropdown
			methodReturnResult = DischargeCompleteBaseLibrary.selectHospitalInDischargeCentral(5, dischargeCompleteHospitalName);
			if (methodReturnResult.contains(failTest)) {
			    return methodReturnResult;
			}
			// Navigate to Discharge Central, Login and Select the hospital from
						// dropdown
			methodReturnResult = DischargeCompleteBaseLibrary.addPatient(patientFirstName, patientLastName);
			if (methodReturnResult.contains(failTest)) {
			return methodReturnResult;
			}
			
			//MRN
			String MRN= driver.findElement(By.xpath("//*[@id='medicalRecordNumber']")).getAttribute("value");
			System.out.println(MRN);
			
			//ACC no
			String AccNo= driver.findElement(By.xpath("//*[@id='accountNumber']")).getAttribute("value");
			System.out.println(AccNo);
						
			/*//Create encoded values
			String encodedValue= DischargeCompleteBaseLibrary.encodeValues(MRN, AccNo,facilityId);
			if (methodReturnResult.contains(failTest)) {
				return encodedValue;
			}*/
			
			String MRN64 = Base64.getEncoder().encodeToString(MRN.getBytes("utf-8"));
			System.out.println(MRN64); 
			String Acc64 = Base64.getEncoder().encodeToString(AccNo.getBytes("utf-8"));
			System.out.println(Acc64); 
			String FacilityID64 = Base64.getEncoder().encodeToString(facilityId.getBytes("utf-8"));
			System.out.println(FacilityID64); 
			
			String semiURl="&mrn="+MRN64+"&account="+Acc64+"&orgId="+FacilityID64;
			
			// To generate parameter value.
			String param_value = DischargeCompleteBaseLibrary.generateParamName(param_name);
			if (methodReturnResult.contains(failTest)) {
		    return param_value;
			}	
			
			 DateFormat dateFormat = new SimpleDateFormat("MMddyyyy");
			 
			 //get current date time with Date()
			 Date date = new Date();
			 
			 // Now format the date
			 String date1= dateFormat.format(date);
			 
			 // Print the Date
			 System.out.println(date1);
			toConvert=facilityId+param_value+date1;
			 
			System.out.println(toConvert);
			
			/*
			// To generate exchange Key
			String exchangeKey = DischargeCompleteBaseLibrary.generateExchangeKey(facilityId, param_value);
			if (methodReturnResult.contains(failTest)) {
		    return exchangeKey;
		    }*/	
			
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			byte[] hashInBytes = md.digest(toConvert.getBytes(StandardCharsets.UTF_8));

			// bytes to hex
			StringBuilder sb = new StringBuilder();
			for (byte b : hashInBytes) {
			sb.append(String.format("%02x", b));
			}
			exchangeKey=sb.toString();
			System.out.println(exchangeKey);
			
			// generate encription key 
			methodReturnResult = DischargeCompleteBaseLibrary.createURL(exchangeKey,semiURl);
			if (methodReturnResult.contains(failTest)) {
			return methodReturnResult;
}
			
			
			
			return "Verified :-  ";
	
	 }
	
	//1
	public static String verifyPrimarycontactofapatientpreparesPML()
		    throws BiffException, InterruptedException, IOException, RowsExceededException, WriteException {

		APPLICATION_LOGS.debug(
			"Executing test case : Primary contact of a patient prepares the PML choice to be sent for patient review.");

		// Get the values from Excel sheet
		String dischargeCompleteHospitalName = testData.getCellData("DischargeComplete",
			"Discharge Complete Hospital Name", 4);
		
		String facilityId = testData.getCellData("loginToDischargeCentral", "FacilityID", 5);
		String dischargeConfig = testData.getCellData("loginToDischargeCentral", "ConfigName", 5);
		String dischargeConfigValue = testData.getCellData("loginToDischargeCentral", "ConfigValue", 5);
		String patientFirstName = testData.getCellData("DischargeComplete", "Patient First Name", 4);
		String patientLastName = testData.getCellData("DischargeComplete", "Patient Last Name", 4);
		
		
		String SNFLOCOptions = testData.getCellData("DischargeComplete", "SNF LOC", 1);
		String state = testData.getCellData("DischargeComplete", "State", 1);
		String county = testData.getCellData("DischargeComplete", "County", 1);
		String city = testData.getCellData("DischargeComplete", "City", 1);
		String zip = testData.getCellData("DischargeComplete", "Zip", 1);
		String DCCompleteProvider1 = testData.getCellData("DischargeComplete", "Discharge Complete Provider", 1);
		
	
		// To verify and set show_ui_changes_discharge_complete config to true
		methodReturnResult = DischargeCompleteBaseLibrary.updateFacilityConfig(facilityId, dischargeConfig,
			dischargeConfigValue);
		if (methodReturnResult.contains(failTest)) {
		    return methodReturnResult;
		}


		// Navigate to Discharge Central, Login and Select the hospital from
		// dropdown
		methodReturnResult = DischargeCompleteBaseLibrary.selectHospitalInDischargeCentral(5, dischargeCompleteHospitalName);
		if (methodReturnResult.contains(failTest)) {
		    return methodReturnResult;
		}
		// Navigate to Discharge Central, Login and Select the hospital from
					// dropdown
		methodReturnResult = DischargeCompleteBaseLibrary.addPatient(patientFirstName, patientLastName);
		if (methodReturnResult.contains(failTest)) {
		return methodReturnResult;
		}
		
		//Navigate to Matching Page
		methodReturnResult = DischargeCompleteBaseLibrary.navigateToMatchingPage();
		if (methodReturnResult.contains(failTest)) {
		return methodReturnResult;
		}
		
		
		//Navigate to PML request
		methodReturnResult = DischargeCompleteBaseLibrary.PMLRequest(DCCompleteProvider1,SNFLOCOptions,state,
				 county,city,zip);
		if (methodReturnResult.contains(failTest)) {
		return methodReturnResult;
				}
							
				

		
		return "Verified :-  ";

 }
	
	//2
	public static String verifySubscriberofapatientpreparesthePMLchoice()//2
		    throws BiffException, InterruptedException, IOException, RowsExceededException, WriteException {

		APPLICATION_LOGS.debug(
			"Executing test case :   Subscriber of a patient prepares the PML choice to be sent for patient review");

		// Get the values from Excel sheet
		String dischargeCompleteHospitalName = testData.getCellData("DischargeComplete",
			"Discharge Complete Hospital Name", 4);
		
		String facilityId = testData.getCellData("loginToDischargeCentral", "FacilityID", 5);
		String dischargeConfig = testData.getCellData("loginToDischargeCentral", "ConfigName", 5);
		String dischargeConfigValue = testData.getCellData("loginToDischargeCentral", "ConfigValue", 5);
		String patientFirstName = testData.getCellData("DischargeComplete", "Patient First Name", 4);
		String patientLastName = testData.getCellData("DischargeComplete", "Patient Last Name", 4);
		String zip = testData.getCellData("DischargeComplete", "Zip", 1);
		
		String NeedABedPOSOptions = testData.getCellData("DischargeComplete", "Need A Bed POS", 1);
		String SNFLOCOptions = testData.getCellData("DischargeComplete", "SNF LOC", 1);
		String state = testData.getCellData("DischargeComplete", "State", 1);
		String county = testData.getCellData("DischargeComplete", "County", 1);
		String city = testData.getCellData("DischargeComplete", "City", 1);
		String DCCompleteProvider1 = testData.getCellData("DischargeComplete", "Discharge Complete Provider", 1);
		
		
		// To verify and set show_ui_changes_discharge_complete config to true
		methodReturnResult = DischargeCompleteBaseLibrary.updateFacilityConfig(facilityId, dischargeConfig,
			dischargeConfigValue);
		if (methodReturnResult.contains(failTest)) {
		    return methodReturnResult;
		}


		// Navigate to Discharge Central, Login and Select the hospital from
		// dropdown
		methodReturnResult = DischargeCompleteBaseLibrary.selectHospitalInDischargeCentral(5, dischargeCompleteHospitalName);
		if (methodReturnResult.contains(failTest)) {
		    return methodReturnResult;
		}
		// Navigate to Discharge Central, Login and Select the hospital from
					// dropdown
		methodReturnResult = DischargeCompleteBaseLibrary.addPatient(patientFirstName, patientLastName);
		if (methodReturnResult.contains(failTest)) {
		return methodReturnResult;
		}
		
		//Selecting subscriber
				methodReturnResult = DischargeCompleteBaseLibrary.selectingSubscriberformWorkbook();
				if (methodReturnResult.contains(failTest)) {
				return methodReturnResult;
				}
				//Navigating to Matching page
				methodReturnResult = DischargeCompleteBaseLibrary.navigateToMatchingPageFromWB();
				if (methodReturnResult.contains(failTest)) {
				return methodReturnResult;
				}
				//Navigate to PML request
				methodReturnResult = DischargeCompleteBaseLibrary.PMLRequest(DCCompleteProvider1,SNFLOCOptions,state,
						 county,city,zip);
				if (methodReturnResult.contains(failTest)) {
				return methodReturnResult;
						}
		
				
		return "Verified :-  ";

 }
	
	

}
