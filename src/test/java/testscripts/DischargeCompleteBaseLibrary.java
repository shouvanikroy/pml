package testscripts;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.Select;

import jxl.read.biff.BiffException;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import util.TestUtil;

public class DischargeCompleteBaseLibrary extends DriverScript {

    // All Hail Automation !

    // Stores current window handle
    public static String currentWindowHandle;
    // Store method return result
    public static String methodReturnResult = null;
    public static Boolean present;
    public static int dischargeCompleteDataRowNumber = 1;
    public static int businessCentralDataRowNumber = 1;

    // Expected page titles
    public static String expectedLoginTitle = null;
    public static String expectedTitle = null;
    public static String navigationBlockedTitle = "Certificate Error: Navigation Blocked";
    public static String dischargeCentralLoginPageTitle = "Login - naviHealth";
    public static String DischargeWorkbookPageTitle = "Discharge Workbook - naviHealth";
    public static String AcceptanceWorkbookPageTitle = "Acceptance Workbook - naviHealth";
    public static String ProfilePageTitle = "Profile - naviHealth";
    public static String ImplementationPageTitle = "Implementation - naviHealth";
    public static String AssessmentPageTitle = "Assessment - Form - naviHealth";
    public static String MatchingPageTitle = "Matching - naviHealth";
    public static String IntakePageTitle = "Patient Detail - naviHealth";
    public static String businessCentralLoginPageTitle = "User Login";
    public static String BASE64="Base64 Encode and Decode - Online";
    public static String Encoder="Generate a SHA-256 encrypted hash";
    public static String facilityCFGPageTitle = "Facility Config Search";
    public static String DischargeCompletionPageTitle = "Discharge Completion - naviHealth";
    public static String SearchPatientPageTitle = "Discharge Patient Search - naviHealth";
    public static String ProviderSetupPageTitle = "Provider Setup";
    public static String providerProfilePageTitle = "Provider Profile Administration";

    /* .............. Locators for the test ................. */

    public static By locatorConverterArea = By.xpath("//*[@id='hash_string']");
	public static By  locatorConvert= By.xpath("//*[@id='submit_button']");
	public static By locatorOutputConverterArea = By.xpath("//*[@id='uppercaseHex']");
    public static By locatorUserNameInputBox = By.xpath("//input[@id='userName']");
    public static By locatorPasswordInputBox = By.xpath("//input[@id='password']");
    public static By locatorUnsupportBrowserDialog = By.xpath("//a[@id='closeBrowserDialog']/span");
    public static By locatorDCUserNameInputBox = By.xpath("//input[@id='email']");
    public static By locatorDCPasswordInputBox = By.xpath("//input[@id='password']");
    public static By locatorDCSignInButton = By.xpath("//a[@id='loginSubmitBtn']/span");
    public static By locatorContinueBtn = By.id("buttonContinue2");
    public static By locatorDischargeTab = By.id("dischargeRedirectLink");
    public static By locatorHospitalDropdown = By.id("papDropDownBtn-1-button");
    public static By locatorPatientChkbox = By.xpath("//input[@name='patientSelectChk']");
    public static By locatorSearchPatientInput = By.id("patientFilterAutoCompleteInput");
    public static By locatorSearchLink = By.id("dischargeWorkbookSearchIcon");
    public static By locatorProcessingDiv = By.xpath("//div[text()='Processing, please wait...']");
    public static By locatorMatchingLink = By.linkText("Matching");
    public static By locatorMatchingLinkWBPage = By.xpath("//a[@name='matching']");
    public static By locatorIntakeLink = By.xpath("//a[contains(text(),'Intake')]");
    public static By locatorIntakePage = By.xpath("//*[@title='Intake']");
    public static By locatorAssessmentLink = By.xpath("//a[contains(text(),'Assessment')]");
    // public static By locatorImplementationLink =
    // By.xpath("//a[contains(text(),'Implementation')]");
    public static By locatorImplementationLink = By.xpath("//a[@class='ir-dc implementation']");
    public static By locatorPatientForm = By.id("patientForm");
    public static By locatorMatchingLayout = By.id("matchingworkbookLayout");
    public static By locatorCreateIBFCoverSheet = By.id("qrFaxCoverSheet");
    public static By locatorAddressbookLink = By.xpath(
	    "//a[contains(@onclick,'YAHOO.com.curaspan.implement.outBoundFax.loadGlobalAddressBookOverlay();')]");
    public static By locatorAddPatientBtn = By.xpath("//span[contains(text(),'Add Patient')]");
    public static By locatorCancelWithoutSaveBtn = By.xpath(".//*[@id='patientSaveCancelBtn']/a/span");
    public static By locatorAdmitDateCalendar = By.id("admitDate");
    public static By locatorDischargeDateCalendar = By.id("estimatedDischargeDate");
    public static By locatorDischargeMonthLink = By
	    .xpath(".//*[@id='caldivid_estimatedDischargeDate_t']/thead/tr[1]/th/div/a[2]");
    public static By locatorDOBMonthLink = By.xpath(".//*[@id='caldivid_patientDOB_t']/thead/tr[1]/th/div/a[2]");
    public static By locatorMonthDropdown = By.id("caldivid_estimatedDischargeDate_nav_month");
    public static By locatorDischargeYearInput = By.id("caldivid_estimatedDischargeDate_nav_year");
    public static By locatorSubmitBtn = By.id("caldivid_estimatedDischargeDate_nav_submit");
    public static By locatorDateRangeMsgTxt = By.id("dateRangeMsgForAdmitDate");
    public static By locatorDOBCalendar = By.id("patientDOB");
    public static By locatorDOBMonthDropdown = By.id("caldivid_patientDOB_nav_month");
    public static By locatorDOBYearInput = By.id("caldivid_patientDOB_nav_year");
    public static By locatorDOBSubmitBtn = By.id("caldivid_patientDOB_nav_submit");
    public static By locatorPatientFirstNameInput = By.id("user.firstName");
    public static By locatorPatientLastNameInput = By.id("user.lastName");
    public static By locatorDOBDate = By.xpath(
	    "//div[@id='caldivid_patientDOB']/table[@id='caldivid_patientDOB_t']/tbody/tr/td[contains(@class,'selectable')]/a[@class='selector']");
    public static By locatorGenderDropdown = By.id("gender");
    public static By locatorAdmitTypeDropdown = By.id("admitType");
    public static By locatorPrimaryDiagnosisDrpdwn = By.id("primaryConfirmedDiagnosis");
    public static By locatorPrimaryDiagnosisOtherInput = By.id("primaryDiagnosisOther");
    public static By locatorSecondaryDiagnosisDrpdwn = By.id("secondaryConfirmedDiagnosis");
    public static By locatorSecondaryDiagnosisOtherInput = By.id("secondaryDiagnosisOther");
    public static By locatorPlanId = By.id("insurance1PlanId");
    public static By locatorSaveBtn = By.xpath(".//*[@id='patientSaveBtn']/a/span[text()='Save Patient']");
    public static By locatorAddPatientSuccessText = By.xpath("//div[@id='content']/div[3]/p");
    public static By locatorAddPatientSuccess=By.xpath("//*[@id='content']/div[4]/p");
    public static By locatorDuplicatePatientOverlay = By.xpath(".//*[@id='duplicatePatientDialog_c']/div[2]");
    public static By locatorSavePatientBtn = By
	    .xpath(".//*[@id='submitPatientInformation']/span[text()='Save Patient']");
    public static By locatorSignInButton = By.xpath("//input[@id='login']");
    public static By locatorToolsTab = By.xpath("//a[contains(text(),'Tools')]");
    public static By locatorFacilityCFGMnu = By.xpath("//a[contains(text(),'Facility CFG')]");
    public static By locatorAccountNoInput = By.id("accountNumber");
    public static By locatorLoadingText = By.xpath("//div[contains(text(),'Loading...')]");
    public static By locatorReturnToWorkbook = By.id("returnToworkbookLink");
    public static By locatorDocumentTab = By.xpath("//span[text()='Documents']");
    public static By locatorAcceptanceTab = By.id("acceptanceRedirectLink");
    public static By locatorLevelDropdown = By.id("levelSourceBtn");
    public static By locatorStateDropdown = By.id("stateSourceBtn");
    public static By locatorCountyDropdown = By.id("countySourceBtn");
    public static By locatorCityDropdown = By.id("citySourceBtn");
    public static By locatorZipDropdown = By.id("zipSourceBtn");
    public static By locatorSearchBtn = By.id("matchBtn");
    public static By locatorSendBookingRequest = By.id("sendBookingRequestBtn");
    public static By locatorPMLRequest = By.xpath(" //*[@id='launchPmlDiv']/a");
    public static By locatorMaillink = By.xpath("//*[@id='pmlEmailId']");
    public static By locatorSendMail = By.xpath("//a[@id='pmlRequestSend']//span[@class='btnpad'][contains(text(),'Share')]");
    public static By locatorPrintLink = By.xpath(".//*[@id='content']/div[1]/div[3]/a");
    public static By locatorDashboardTable = By.id("referralWorkbookDashboardStatTable");
    public static By locatorFormCheckbox = By.xpath("//tbody[@class='yui-dt-data']/tr/td/div/input[@type='checkbox']");
    public static By locatorSavePatientRecordBtn = By.xpath(".//*[@id='attachformBtn']/span");
    public static By locatorConfirmBtn = By.id("shareDocsConfirmationSubmit");
    public static By locatorProfileTab = By.id("profileRedirectLink");
    public static By locatorActionDropdown = By
	    .xpath("//div[@id='Table']/div[3]/table/tbody[2]/tr[1]/td[5]//span[text()='Action']");
    public static By locatorArchiveReferralAction = By.id("referralactionArchiveLnk");
    public static By locatorSaveAndRemoveBtn = By.xpath("//a[@id='acceptanceCompeleteProcess']/span");
    public static By locatorSaveAndRemoveBtnWithoutDischargeDate = By
	    .xpath("(//a[@id='acceptanceCompeleteProcess']/span)[3]");
    public static By locatorAdmissionDateDiv = By.id("admissionDateDiv");
    public static By locatorAdmissionDateCalendar = By.id("admissionDate");
    public static By locatorAdmissionMonthLink = By
	    .xpath(".//*[@id='caldivid_admissionDate_t']/thead/tr[1]/th/div/a[2]");
    public static By locatorAdmissionMonthDropdown = By.id("caldivid_admissionDate_nav_month");
    public static By locatorAdmissionYearInput = By.id("caldivid_admissionDate_nav_year");
    public static By locatorAdmissionSubmitBtn = By.id("caldivid_admissionDate_nav_submit");
    public static By locatorSelectTimeButton = By.id("admissionTimeBtn-button");
    public static By locatorTimeValue = By
	    .xpath(".//*[@id='acceptanceCompletePanelForNotHomecareOnServiceReferral']/div[4]/div[2]/ul/li[2]/a");
    public static By locatorPatientLink = By.xpath("//div[@id='Table']/div[3]/table/tbody[2]/tr[1]/td[3]/div/div[1]");
    public static By locatorArchivePatientFilterInput = By.id("patientFilterAutoCompleteInput");
    public static By locatorFromDateCalendar = By.id("referralStatusFromDate");
    public static By locatorFromMonthLink = By
	    .xpath(".//*[@id='caldivid_referralStatusFromDate_t']/thead/tr[1]/th/div/a[2]");
    public static By locatorFromMonthDropdown = By.id("caldivid_referralStatusFromDate_nav_month");
    public static By locatorFromYearInput = By.id("caldivid_referralStatusFromDate_nav_year");
    public static By locatorFromSubmitBtn = By.id("caldivid_referralStatusFromDate_nav_submit");
    public static By locatorReferralFilter = By.id("referralFilterFind");
    public static By locatorPatientInArchivePage = By
	    .xpath("//div[@id='Table']/div[3]/table/tbody[2]/tr/td[2]/div/div");
    public static By locatorAcceptancePanelWithNoResponse = By.id("acceptanceCompeleteForReferralWithNoResponse_c");
    public static By locatorAcceptanceCancelBtn = By.xpath(".//*[@id='acceptanceCompeleteCancel']/span");
    public static By locatorReferralNameLink = By
	    .xpath("//div[@id='Table']/div[3]/table/tbody[2]/tr[1]/td[3]/div/div[1]");
    public static By locatorDischargeIcon = By.xpath("//a[@title='Discharge Patient']/span");
    public static By locatorActualDischargeDateCalendar = By.id("actualDischargeDate");
    public static By locatorActualDischargeMonthLink = By
	    .xpath(".//*[@id='caldivid_actualDischargeDate_t']/thead/tr[1]/th/div/a[2]");
    public static By locatorActualDischargeMonthDropdown = By.id("caldivid_actualDischargeDate_nav_month");
    public static By locatorActualDischargeYearInput = By.id("caldivid_actualDischargeDate_nav_year");
    public static By locatorActualDischargeSubmitBtn = By.id("caldivid_actualDischargeDate_nav_submit");
    public static By locatorSaveAndKeepBtn = By.id("dischargeCompletionKeepInWorkbook");
    public static By locatorConfirmDialog = By.id("showConfirmDialog_c");
    public static By locatorConfirmDischargeBtn = By.id("submitDischargeCompletion");
    public static By locatorDischargeDateText = By
	    .xpath("//div[@id='workbookPanelTable']/div[3]/table/tbody[2]/tr[1]/td[8]/div");
    public static By locatorConnectedProvider = By.xpath("//div[text()='Connected']/../../../td[1]/div/input");
    public static By locatorUnconnectedProvider = By.xpath("//div[text()='Unconnected']/../../../../td[1]/div/input");
    public static By locatorPOSDropdown = By.id("posSourceBtn");
    public static By locatorProviderRowsInImplementationPage = By
	    .xpath(".//*[@id='showProvidersByLoc']/div/div/div[2]/div/div[3]/table/tbody[2]/tr");
    // public static By locatorBookReferralButton =
    // By.xpath("//span[contains(text(),'Book Referral')]");
    public static By locatorBookReferralButton = By.id("implementBookReferral");
    public static By locatorBookReferralButtonInBookReferralDialog = By.id("bookTheReferral");
    public static By locatorMRNInput = By.id("medicalRecordNumber");
    public static By locatorMRNInputXpath = By.xpath("//*[@id='medicalRecordNumber']");
    public static By locatorAccountIDInput = By.id("accountNumber");
    public static By locatorConnectedProviderText = By.xpath("//span[contains(text(),'Connected Provider(s)')]");
    public static By locatorLogoutLink = By.id("logOutLink");
    public static By locatorLogoutMessage = By
	    .xpath("//p[@class='success-msg'][contains(text(),'You are now logged out.')]");
    public static By locatorRecentlyDischargedLink = By.id("recentlyDischargedLink");
    public static By locatorPatientNavigation = By.xpath(".//*[@class='recPatientNav']");
    public static By locatorSearchPatient = By.xpath("//div[@class='subnav']//a[@id='dischargeSearchPatientLink']");
    public static By locatorDischargeWorkbookLink = By.id("dischargeWorkBookLink");
    public static By locatorPrintRecentDischarge = By.id("printRecentDischarge");
    public static By locatorProviderTab = By.xpath("//a[contains(text(),'Providers')]");
    public static By locatorProviderSetupMnu = By.xpath("(//a[contains(text(),'Provider Setup')])");
    public static By locatorProviderSetupGoBtn = By.xpath("//input[@id='go']");
    public static By locatorProviderIDInputInProviderSetup = By.xpath("//input[@id='provider_id']");
    public static By locatorRealtimeFacilityUpdate = By.xpath("//a[contains(text(),'Real Time Facility Update')]");
    public static By locatorCreateTrainingPtnt = By
	    .xpath(".//*[@id='site_box']/table/tbody//a[contains(text(),'create training patients')]");
    public static By locatorInpatientbyUnit = By.xpath("//span[@id='patientUnitSection']/div");
    public static By locatorInpatientDropdown = By.xpath("//button[@id='patientUnitDropDownButton-button']");
    public static By locatorInpatientOption = By.xpath("//a[text()='TRAINING']");
    public static By locatorSearchbutton = By.xpath("//a[@id='patientAdvancedSearchBtn']");
    public static By locatorRemoveTrainingPtnt = By
	    .xpath(".//*[@id='site_box']/table/tbody//a[contains(text(),'remove training patients')]");
    public static By locatorAdmitDateMonthLink = By.xpath(".//*[@id='caldivid_admitDate_t']/thead/tr[1]/th/div/a[2]");
    public static By locatorAdmitDateMonthDrpDwn = By.id("caldivid_admitDate_nav_month");
    public static By locatorAdmitDateYearInput = By.id("caldivid_admitDate_nav_year");
    public static By locatorAdmitDateOkBtn = By.id("caldivid_admitDate_nav_submit");
    public static By locatorUnitRoom = By.id("roomNumber");
	public static By locatorInputArea = By.xpath("/html/body/div/main/div[1]/form/textarea");
	public static By locatorEncodeButton = By.xpath("/html/body/div/main/div[1]/form/button");
	public static By locatorOutput = By.xpath("/html/body/div/main/div[1]/textarea");
    /*
     * .... Name of the WebElements present on the WebPage ....
     */

    public static String nameUserNameInputBox = "'Username' Input-box";
    public static String namePasswordInputBox = "'Password' Input-box";
    public static String nameUnsupportBrowserDialog = "'Unsupport Browser' Dialog";
    public static String nameDCUserNameInputBox = "'Discharge Central Username' Input";
    public static String nameDCPasswordInputBox = "'Discharge Central Password' Input";
    public static String nameDCSignInButton = "'Discharge Central SignIn' Button";
    public static String nameContinueBtn = "'Continue' button";
    public static String nameDischargeTab = "'Discharge' Tab";
    public static String nameHospitalDropdown = "'Hospital' Dropdown";
    public static String nameSearchPatientInput = "'Search Patient' Input";
    public static String nameSearchLink = "'Search' Link";
    public static String nameProcessingText = "'Processing' Text";
    public static String nameMatchingLink = "'Matching' Link";
    public static String nameIntakeLink = "'Intake' Link";
    public static String nameImplementationLink = "'Implementation' Link";
    public static String nameAssessmentLink = "'Assessment' Link";
    public static String namePatientForm = "'Patient' form";
    public static String nameMatchingLayout = "'Matching' layout";
    public static String nameCreateIBFCoverSheet = "'Create IBF Cover sheet' Link";
    public static String nameAddressbookLink = "'Address book' Link";
    public static String nameAddPatientBtn = "'Add Patient' Button";
    public static String nameCancelWithoutSaveBtn = "'Cancel without save' Button";
    public static String nameAdmitDateCalendar = "'Admit date' Calendar";
    public static String nameDischargeDateCalendar = "'Discharge date' Calendar";
    public static String nameDischargeMonthLink = "'Discharge month' link";
    public static String nameMonthDropdown = "'Discharge month' Dropdown";
    public static String nameDischargeYearInput = "'Discharge year' Input";
    public static String nameSubmitBtn = "'Submit' Button";
    public static String nameDateRangeMsgTxt = "'Date Range message' text";
    public static String nameDOBCalendar = "'Date of birth' calendar";
    public static String nameDOBMonthDropdown = "'Date of birth' month dropdown";
    public static String nameDOBYearInput = "'Date of birth' year dropdown";
    public static String nameDOBSubmitBtn = "'Date of birth' submit button";
    public static String namePatientFirstNameInput = "'Patient first name' input";
    public static String namePatientLastNameInput = "'Patient last name' input";
    public static String nameDOBDate = "'DOB date' field";
    public static String nameGenderDropdown = "'Gender' dropdown";
    public static String nameAdmitTypeDropdown = "'Admit type' dropdown";
    public static String namePrimaryDiagnosisDrpdwn = "'Primary Diagnosis' dropdown";
    public static String namePrimaryDiagnosisOtherInput = "'Primary Diagnosis other' input";
    public static String nameSecondaryDiagnosisDrpdwn = "'Secondary Diagnosis' dropdown";
    public static String nameSecondaryDiagnosisOtherInput = "'Secondary Diagnosis other' input";
    public static String namePlanId = "'Plan ID' dropdown";
    public static String nameSaveBtn = "'save' button";
    public static String nameAddPatientSuccessText = "'Add Patient Success' Message";
    public static String nameDuplicatePatientOverlay = "'Duplicate Patient' Overlay";
    public static String nameSavePatientBtn = "'Save Patient' Button";
    public static String nameSignInButton = "'Sign in' Button";
    public static String nameToolsTab = "'Tools' Tab";
    public static String nameFacilityCFGMnu = "'Facility CFG' Menu";
    public static String nameAccountNoInput = "'Account no' Input";
    public static String nameLoadingText = "'Loading' Text";
    public static String nameReturnToWorkbook = "'Return To Workbook' link";
    public static String nameDocumentTab = "'Document' tab";
    public static String nameAcceptanceTab = "'Acceptance' tab";
    public static String nameLevelDropdown = "'Level of care' dropdown";
    public static String nameStateDropdown = "'State' dropdown";
    public static String nameCountyDropdown = "'County' dropdown";
    public static String nameCityDropdown = "'City' dropdown";
    public static String nameZipDropdown = "'Zip' dropdown";
    public static String nameSearchBtn = "'Search' button";
    public static String nameSendBookingRequest = "'Send Booking Request' Button";
    public static String namePrintLink = "'Print' link";
    public static String nameDashboardTable = "'Dashboard' table";
    public static String nameFirstFormCheckbox = "'First form' Checkbox";
    public static String nameSavePatientRecordBtn = "'Save patient Record' button";
    public static String nameConfirmBtn = "'Confirm' button";
    public static String nameProfileTab = "'Profile' Tab";
    public static String nameActionDropdown = "'Action' dropdown";
    public static String nameArchiveReferralAction = "'Archive Referral' dropdown";
    public static String nameAdmissionDateCalendar = "'Admission Date' Calender";
    public static String nameAdmissionMonthLink = "'Admission Month' Link";
    public static String nameAdmissionMonthDropdown = "'Admission Month' dropdown";
    public static String nameAdmissionYearInput = "'Admission Year' input";
    public static String nameAdmissionSubmitBtn = "'Admission Submit' button";
    public static String nameSelectTimeButton = "'Select Time' button";
    public static String nameTimeValue = "'Time value' in Select time dropdown";
    public static String nameAdmissionDateDiv = "'Admission date' div";
    public static String nameSaveAndRemoveBtn = "'Save and Remove' button";
    public static String namePatientLink = "'First Patient Name' Link";
    public static String nameArchivePatientFilterInput = "'Patient filter input' in archive page";
    public static String nameFromDateCalendar = "'From Date' Calender";
    public static String nameFromMonthLink = "'From Month' Link";
    public static String nameFromMonthDropdown = "'From Month' dropdown";
    public static String nameFromYearInput = "'From Year' input";
    public static String nameFromSubmitBtn = "'From Submit' button";
    public static String nameReferralFilter = "'Referral filter' button";
    public static String namePatientInArchivePage = "'Patient link' in archive page";
    public static String nameAcceptancePanelWithNoResponse = "'Acceptance panel with no response' in Referral workbook page";
    public static String nameAcceptanceCancelBtn = "'Acceptance Cancel button' in Referral workbook page";
    public static String nameReferralNameLink = "'Referral Name' Link";
    public static String nameDischargeIcon = "'Discharge Icon' in Discharge workbook page";
    public static String nameDischargeMonthDropdown = "'Discharge Month' dropdown";
    public static String nameDischargeSubmitBtn = "'Discharge Submit' button";
    public static String nameSaveAndKeepBtn = "'Save and keep' button";
    public static String nameConfirmDialog = "'Confirm Dialog' in Discharge page";
    public static String nameConfirmDischargeBtn = "'Confirm Discharge' button";
    public static String nameDischargeDateText = "'Discharge date' text";
    public static String nameConnectedProvider = "'Connected Provider' Checkbox";
    public static String nameUnconnectedProvider = "'Unconnected Provider' Checkbox";
    public static String namePOSDropdown = "'POS dropdown' in matching page";
    public static String nameBookReferralButton = "'Book Referral button' in implementation page";
    public static String nameBookReferralButtonInBookReferralDialog = "'Book Referral button' in book referral dialog";
    public static String nameMRNInput = "'MRN number input' in intake page";
    public static String nameConnectedProviderText = "'Connected Provider ' in intake page";
    public static String nameLogoutLink = "'Logout' link";
    public static String nameLogoutMessage = "'Logout message' in Discharge site";
    public static String nameRecentlyDischargedLink = "'Recently Discharge Link' in Dicharge central";
    public static String namePatientNavigation = "'Patient Navigation' in recently discharged page";
    public static String namesearchpatient = "Search Patients link";
    public static String nameDischargeWorkbookLink = "'Discharge Workbook link' in Dicharge central";
    public static String namePrintRecentDischarge = "'Print on Recently Discharged Page' Icon";
    public static String nameProviderTab = "'Provider' Tab";
    public static String nameProviderSetupMnu = "'Provider Setup' Menu";
    public static String nameRealtimeFacilityUpdate = "'Real time facility update' Link";
    public static String nameProviderSetupGoBtn = "'Go' Button";
    public static String nameProviderIDInputInProviderSetup = "'Provider ID' Input";
    public static String nameCreateTrainingPtnt = "'create training patient' link";
    public static String nameInpatienbyUnit = "Expanding Inpatient by unit search criteria";
    public static String nameInpatientDropdown = "Clicking on inpatient dropdown";
    public static String nameInpatientOption = "Selectig 'Training' option from inpatient dropdown";
    public static String nameSearchbutton = "Clicking on Search button";
    public static String nameRemoveTrainingPtnt = "'remove training patient' link";
    public static String nameUnitRoom = "Unit Room";
    public static String RUN_DATE = TestUtil.now("ddMMyyhhmm").toString();

    // ---------Locators
    public static By locatorIntakeUsernameField = By.id("username");
    public static By locatorIntakePasswordField = By.id("password");
    public static By locatorIntakeLoginButton = By.id("login");
    public static By locatorIntakeEmailVerificationModal = By.xpath("//*[@class='validate-email-modal ng-scope']");
    public static By locatorIntakeEmailVerificationVerifyButton = By.xpath("//*[contains(text(),'VERIFY')]");
    public static By locatorIntakeEmailVerificationCodeModal = By
	    .xpath("//*[@class='verification-code-modal ng-scope']");
    public static By locatorIntakeEmailVerificationCancelButton = By.xpath("//*[@analytics-event='Cancel']");
    public static By locatornHIntakeButton = By.xpath("//div[@analytics-label='Intake']");
    public static By locatorIntakeSearchPatientButton = By.xpath("//*[@analytics-event='SearchPatients']");
    public static By locatorIntakeSearchPatientField = By.xpath("//input[@name='searchText']");
    public static By locatorIntakeSearchPatientButton2 = By
	    .xpath("//*[contains(@ng-click,'searchModel.searchPatients')]");
    public static By locatorResopondButton = By.xpath("//*[contains(@ng-click,'openRespondModal')]");
    public static By locatorReferralStatusList = By.xpath("//*[contains(@ng-options,'referralStatusList')]");
    public static By locatorSendResponseButton = By.xpath("//*[contains(text(),'Send Response')]");

    public static By locatorIntakeRespondButton = By.xpath("//*[contains(text(),'Respond')]");
    public static By locatorIntakeSelectAStatusDropdown = By
	    .xpath("//*[@ng-model='statusModel.selectedReferralStatus']");
    public static By locatorIntakeSendResponse = By.xpath("//*[contains(text(),'Send Response')]");
    public static By locatorGoToDischargeButton = By.xpath("//*[contains(text(),'GO TO DISCHARGE')]");
    public static By locatorIntakeReferPageButton = By.xpath("//button[@id='discharge-header-matching-button']");
    public static By locatorReferConnectedAscendingOrderButton = By.xpath("//*[contains(@ng-click,'CONNECTED')]");
    public static By locatorReferSendReferralPacketButton = By
	    .xpath("//*[contains(@id,'send-referral-packet-button')]");
    public static By locatorIntakeConnectPageButton = By
	    .xpath("//button[@id='discharge-header-implementation-button']");

    public static By locatorWorkbookPage = By.xpath("//a[@id='dischargeWorkBookLink']");
    public static By locatorSelectPatient = By.xpath("/html[1]/body[1]/div[22]/div[2]/div[7]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/div[3]/table[1]/tbody[2]/tr[1]/td[3]/div[1]/div[2]/a[1]");
    public static By locatorSelectSubscriber = By.xpath("//button[contains(text(),'Select Action')]");
    public static By locatorTypeSubscriber = By.xpath("//input[@id='addSubscriberInput']");//
    public static By locatorUsersDropdown = By.xpath("//span[@id='workbookCaseManagerDropDownButton']//span[@class='first-child']");
    public static By locatorOfSubscriber = By.xpath("//span[@id='workbookCaseManagerDropDownButton']//span[@class='first-child']");
    public static By locatorOfMatchingButton = By.xpath("//tr[@class='yui-dt-rec yui-dt-first yui-dt-even']//a[@title='Matching']");
    
    public static By locatorBookButton = By.xpath("//li[contains(text(),'Book')]");
    public static By locatorSuspendButton = By.xpath("//li[contains(text(),'Suspend')]");

    public static String StatusReceivedAndRefer = "Accept";

    // Create a browser instance and navigate to the Discharge Central
    public static String selectHospitalInDischargeCentral(int Data_Row_No, String hospitalName)
	    throws MalformedURLException, InterruptedException {

	// System.out.println("Hi");

	APPLICATION_LOGS.debug("Creating a browser instance and navigating to the test site ...");

	// Disable log messages
	java.util.logging.Logger.getLogger("org.apache.http.impl.client").setLevel(java.util.logging.Level.WARNING);

	if (wbdv == null) {

	    try {

		if (CONFIG.getProperty("is_remote").equals("true")) {

		    // Generate Remote address
		    String remote_address = "http://" + CONFIG.getProperty("remote_ip") + ":4444/wd/hub";
		    remote_url = new URL(remote_address);

		    if (CONFIG.getProperty("test_browser").contains("Internet Explorer")) {

			dc = DesiredCapabilities.internetExplorer();
			dc.setCapability("silent", true);

		    }

		    else {

			ProfilesIni allProfiles = new ProfilesIni();
			// FirefoxProfile profile =
			// allProfiles.getProfile("default");
			// profile.setPreference("plugins.hide_infobar_for_missing_plugin",
			// true);
			dc = DesiredCapabilities.firefox();
			// dc.setCapability(FirefoxDriver.PROFILE, profile);
			// dc.setJavascriptEnabled(true);

		    }

		    wbdv = new RemoteWebDriver(remote_url, dc);
		    driver = new EventFiringWebDriver(wbdv);
		    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		}

		else {

		    if (CONFIG.getProperty("test_browser").toLowerCase().contains("internet explorer")
			    || CONFIG.getProperty("test_browser").toLowerCase().contains("ie")) {
			dc = DesiredCapabilities.internetExplorer();
			dc.setCapability("silent", true);
			wbdv = new InternetExplorerDriver(dc);
			driver = new EventFiringWebDriver(wbdv);

		    }

		    else if (CONFIG.getProperty("test_browser").toLowerCase().contains("firefox")
			    || CONFIG.getProperty("test_browser").toLowerCase().contains("ff")) {

			ProfilesIni allProfiles = new ProfilesIni();
			FirefoxProfile profile = allProfiles.getProfile("default");
			profile.setAcceptUntrustedCertificates(true);
			profile.setAssumeUntrustedCertificateIssuer(false);
			wbdv = new FirefoxDriver(profile);
			driver = new EventFiringWebDriver(wbdv);

		    }

		    else if (CONFIG.getProperty("test_browser").toLowerCase().contains("safari")) {

			dc = DesiredCapabilities.safari();

		    }

		    else if (CONFIG.getProperty("test_browser").toLowerCase().contains("chrome")) {

			System.setProperty("webdriver.chrome.driver",
				System.getProperty("user.dir") + "/chromedriver.exe");
			dc = DesiredCapabilities.chrome();
			wbdv = new ChromeDriver(dc);
			driver = new EventFiringWebDriver(wbdv);

		    }

		}

	    }

	    catch (Throwable initBrowserException) {

		APPLICATION_LOGS
			.debug("Error came while creating a browser instance : " + initBrowserException.getMessage());

		return failTest + " : Error came while creating a browser instance : "
			+ initBrowserException.getMessage();

	    }

	}

	APPLICATION_LOGS.debug("Created browser instance successfully");

	try {

	    // Implicitly wait for 30 seconds for browser to open
	    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

	    // Delete all browser cookies
	    driver.manage().deleteAllCookies();

	    // Navigate to Curaspan application
	    driver.navigate().to(CONFIG.getProperty("dischargeCentralURL"));

	    // Handle certificate error
	    if (CONFIG.getProperty("test_browser").contains("Internet Explorer")) {

		if (driver.getTitle().contains(navigationBlockedTitle)) {

		    driver.navigate().to("javaScript:document.getElementById('overridelink').click()");

		    FunctionLibrary.waitForPageToLoad();

		}

	    }

	    // Maximize browser window
	    APPLICATION_LOGS.debug("Maximizing Browser window...");
	    driver.manage().window().maximize();
	    APPLICATION_LOGS.debug("Browser window is maximized");
	    //
	    // if (CONFIG.getProperty("test_browser").contains("Firefox")) {
	    // WebElement html = driver.findElement(By.tagName("html"));
	    // html.sendKeys(Keys.chord(Keys.CONTROL, "0"));
	    // }

	    if (FunctionLibrary.isElementDisplayed(locatorUnsupportBrowserDialog, nameUnsupportBrowserDialog)) {
		methodReturnResult = FunctionLibrary.clickLink(locatorUnsupportBrowserDialog,
			nameUnsupportBrowserDialog);
		if (methodReturnResult.contains(failTest)) {
		    return methodReturnResult;
		}
	    }
	}

	catch (Throwable navigationError) {

	    APPLICATION_LOGS.debug(
		    "Error came while navigating to the Discharge Central site : " + navigationError.getMessage());
	    return failTest + " : Error came while navigating to the Discharge Central site.";
	}

	Thread.sleep(5000L);
	// Verify Login page appears
	expectedTitle = dischargeCentralLoginPageTitle;
	methodReturnResult = FunctionLibrary.assertTitle(expectedTitle);
	if (methodReturnResult.contains(failTest)) {

	    // Log result
	    APPLICATION_LOGS.debug("Not navigated to Login page");
	    return methodReturnResult;

	}

	APPLICATION_LOGS.debug("Navigated to Login page");

	APPLICATION_LOGS.debug("Logging in to Discharge Central ...");

	String userName = null;
	String password = null;

	try {

	    userName = testData.getCellData("loginToDischargeCentral", "UserName", Data_Row_No);
	    password = testData.getCellData("loginToDischargeCentral", "Password", Data_Row_No);

	    APPLICATION_LOGS.debug("Successfully Retrieved data from Xls File :-  Username : " + userName
		    + " and Password : " + password);

	}

	catch (Throwable fetchExcelDataError) {

	    APPLICATION_LOGS.debug("Error while retrieving data from xls file : " + fetchExcelDataError.getMessage());
	    return failTest + " : Error while retrieving data from xls file : " + fetchExcelDataError.getMessage();

	}

	// Clear Username input-box and input username
	methodReturnResult = FunctionLibrary.clearAndInput(locatorDCUserNameInputBox, nameDCUserNameInputBox, userName);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Clear Password input-box and input password
	methodReturnResult = FunctionLibrary.clearAndInput(locatorDCPasswordInputBox, nameDCPasswordInputBox, password);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Click on Sign In button
	methodReturnResult = FunctionLibrary.clickAndWait(locatorDCSignInButton, nameDCSignInButton);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Verify login was successful
	expectedTitle = dischargeCentralLoginPageTitle;
	String actualTitle = null;

	for (int i = 0; i < 3; i++) {

	    // Get page title
	    actualTitle = FunctionLibrary.getPageTitle();

	    if (actualTitle.equals(expectedTitle)) {

		// Log result
		APPLICATION_LOGS.debug("Failed to login. Retrying ...");

		// Clear Username input-box and input username
		FunctionLibrary.clearAndInput(locatorDCUserNameInputBox, nameUserNameInputBox, userName);

		// Clear Password input-box and input password
		FunctionLibrary.clearAndInput(locatorDCPasswordInputBox, namePasswordInputBox, password);

		// Click on Sign In button
		FunctionLibrary.clickAndWait(locatorDCSignInButton, nameDCSignInButton);

	    } else {
		break;
	    }

	}

	// Click on Continue button if present
	if (FunctionLibrary.isElementDisplayed(locatorContinueBtn, nameContinueBtn)) {
	    FunctionLibrary.clickLink(locatorContinueBtn, nameContinueBtn);

	}
	FunctionLibrary.waitForElementToLoad(locatorDischargeTab);

	// Click on Remind later button
	if (FunctionLibrary.isElementDisplayed(By.id("profileUpdateReminderCancel"), "Profile update Reminder")) {
	    FunctionLibrary.clickLink(By.id("profileUpdateReminderCancel"), "Remind Later Btn");
	}

	APPLICATION_LOGS.debug("Logged into Discharge Central");

	APPLICATION_LOGS.debug("Navigating to Discharge page ...");

	// Thread.sleep(5000L);

	// if
	// (CONFIG.getProperty("test_browser").toLowerCase().contains("internet
	// explorer")
	// || CONFIG.getProperty("test_browser").toLowerCase().contains("ie")) {
	// ((JavascriptExecutor) driver).executeScript(
	// "var el=arguments[0]; setTimeout(function() { el.click(); }, 500);",
	// driver.findElement(locatorDischargeTab));
	// Thread.sleep(2000);
	//
	// } else {
	// Click on 'Discharge' tab and wait for page to load
	System.out.println("-1-");
	Thread.sleep(2000);

	methodReturnResult = FunctionLibrary.clickAndWait(locatorDischargeTab, nameDischargeTab);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}
	// }

	// Wait for the result to appear
	if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
	    FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
	}

	// Verify we navigated to discharge page
	expectedTitle = DischargeWorkbookPageTitle;
	methodReturnResult = FunctionLibrary.assertTitle(expectedTitle);
	if (methodReturnResult.contains(failTest)) {

	    APPLICATION_LOGS.debug("Re-trying to go to the Discharge page ...");

	    // if
	    // (CONFIG.getProperty("test_browser").toLowerCase().contains("internet
	    // explorer")
	    // ||
	    // CONFIG.getProperty("test_browser").toLowerCase().contains("ie"))
	    // {
	    // ((JavascriptExecutor) driver).executeScript(
	    // "var el=arguments[0]; setTimeout(function() { el.click(); },
	    // 500);",
	    // driver.findElement(locatorDischargeTab));
	    // Thread.sleep(2000);
	    // } else {
	    // Click on 'Discharge' tab and wait for page to load
	    // ---------------------------------------

	    if (CONFIG.getProperty("test_browser").toLowerCase().contains("internet explorer")
		    || CONFIG.getProperty("test_browser").toLowerCase().contains("ie")) {
		((JavascriptExecutor) driver).executeScript(
			"var el=arguments[0]; setTimeout(function() { el.click(); }, 500);",
			driver.findElement(locatorDischargeTab));
		Thread.sleep(2000);

	    } else {
		// Click on 'Discharge' tab and wait for page to load
		methodReturnResult = FunctionLibrary.clickAndWait(locatorDischargeTab, nameDischargeTab);
		if (methodReturnResult.contains(failTest)) {
		    return methodReturnResult;
		}
	    }

	    // ---------------------------------------
	    /*
	     * methodReturnResult = FunctionLibrary.clickAndWait(locatorDischargeTab,
	     * nameDischargeTab); if (methodReturnResult.contains(failTest)) { return
	     * methodReturnResult; }
	     */
	    // }

	    // Wait for the result to appear
	    if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
		FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
	    }
	}

	methodReturnResult = FunctionLibrary.assertTitle(expectedTitle);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Select the hospital dropdown
	methodReturnResult = FunctionLibrary.clickLink(locatorHospitalDropdown, nameHospitalDropdown);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Select the hospital from the dropdown
	methodReturnResult = FunctionLibrary.clickLink(By.xpath("//a[contains(text(),'" + hospitalName + "')]"),
		"Hospital dropdown");
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Wait for the result to appear
	if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
	    FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
	}

	// Wait for the Loading text to disappear
	if (FunctionLibrary.isElementDisplayed(locatorLoadingText, nameLoadingText)) {
	    FunctionLibrary.waitForElementToDisappear(locatorLoadingText, nameLoadingText);
	}

	APPLICATION_LOGS.debug("Navigated to Discharge tab....");

	return "Pass : Navigated to Login page, Logged into Discharge Central and Navigated to Discharge tab and selected hospital from dropdown";

    }

    // Create a browser instance and navigate to the test site
    public static String navigateToBusinessCentral(int Data_Row_No) throws MalformedURLException, InterruptedException {

	APPLICATION_LOGS.debug("Creating a browser instance and navigating to the test site ...");

	// Disable log messages
	java.util.logging.Logger.getLogger("org.apache.http.impl.client").setLevel(java.util.logging.Level.WARNING);

	if (wbdv == null) {

	    try {

		if (CONFIG.getProperty("is_remote").equals("true")) {

		    // Generate Remote address
		    String remote_address = "http://" + CONFIG.getProperty("remote_ip") + ":4444/wd/hub";
		    remote_url = new URL(remote_address);

		    if (CONFIG.getProperty("test_browser").contains("Internet Explorer")) {

			dc = DesiredCapabilities.internetExplorer();
			dc.setCapability("silent", true);
		    }

		    else {

			ProfilesIni allProfiles = new ProfilesIni();
			FirefoxProfile profile = allProfiles.getProfile("default");
			profile.setPreference("plugins.hide_infobar_for_missing_plugin", true);
			dc = DesiredCapabilities.firefox();
			dc.setCapability(FirefoxDriver.PROFILE, profile);
			dc.setJavascriptEnabled(true);

		    }

		    wbdv = new RemoteWebDriver(remote_url, dc);
		    driver = new EventFiringWebDriver(wbdv);
		    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		}

		else {

		    if (CONFIG.getProperty("test_browser").toLowerCase().contains("internet explorer")
			    || CONFIG.getProperty("test_browser").toLowerCase().contains("ie")) {

			// System.setProperty("webdriver.ie.driver",
			// "C:\\Users\\IEDriver\\IEDriverServer.exe");
			dc = DesiredCapabilities.internetExplorer();
			dc.setCapability("silent", true);
			wbdv = new InternetExplorerDriver(dc);
			driver = new EventFiringWebDriver(wbdv);

		    }

		    else if (CONFIG.getProperty("test_browser").toLowerCase().contains("firefox")
			    || CONFIG.getProperty("test_browser").toLowerCase().contains("ff")) {

			ProfilesIni allProfiles = new ProfilesIni();
			FirefoxProfile profile = allProfiles.getProfile("default");
			profile.setAcceptUntrustedCertificates(true);
			profile.setAssumeUntrustedCertificateIssuer(false);
			wbdv = new FirefoxDriver(profile);
			driver = new EventFiringWebDriver(wbdv);

		    }

		    else if (CONFIG.getProperty("test_browser").toLowerCase().contains("safari")) {

			dc = DesiredCapabilities.safari();

		    }

		    else if (CONFIG.getProperty("test_browser").toLowerCase().contains("chrome")) {

			System.setProperty("webdriver.chrome.driver",
				System.getProperty("user.dir") + "/chromedriver.exe");
			dc = DesiredCapabilities.chrome();
			wbdv = new ChromeDriver(dc);
			driver = new EventFiringWebDriver(wbdv);

		    }

		}

	    }

	    catch (Throwable initBrowserException) {

		APPLICATION_LOGS
			.debug("Error came while creating a browser instance : " + initBrowserException.getMessage());

		return "Fail : Error came while creating a browser instance : " + initBrowserException.getMessage();

	    }

	}

	APPLICATION_LOGS.debug("Created browser instance successfully");

	try {

	    // Implicitly wait for 30 seconds for browser to open
	    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

	    // Delete all browser cookies
	    driver.manage().deleteAllCookies();

	    // Navigate to Curaspan application
	    driver.navigate().to(CONFIG.getProperty("testSiteURL"));

	    // Handle certificate error
	    if (CONFIG.getProperty("test_browser").contains("Internet Explorer")) {

		if (driver.getTitle().contains(navigationBlockedTitle)) {

		    driver.navigate().to("javaScript:document.getElementById('overridelink').click()");

		    FunctionLibrary.waitForPageToLoad();

		}

	    }

	    // Maximize browser window
	    APPLICATION_LOGS.debug("Maximizing Browser window...");
	    driver.manage().window().maximize();
	    APPLICATION_LOGS.debug("Browser window is maximized");

	    if (CONFIG.getProperty("test_browser").contains("Firefox")) {
		WebElement html = driver.findElement(By.tagName("html"));
		html.sendKeys(Keys.chord(Keys.CONTROL, "0"));
	    }
	}

	catch (Throwable navigationError) {

	    APPLICATION_LOGS.debug("Error came while navigating to the test site : " + navigationError.getMessage());

	}

	// Verify Login page appears
	expectedTitle = businessCentralLoginPageTitle;
	methodReturnResult = FunctionLibrary.assertTitle(expectedTitle);
	if (methodReturnResult.contains(failTest)) {

	    // Log result
	    APPLICATION_LOGS.debug("Not navigated to Login page");
	    return methodReturnResult;

	}

	APPLICATION_LOGS.debug("Navigated to Login page");

	APPLICATION_LOGS.debug("Logging in to Business Central ...");

	String userName = null;
	String password = null;

	try {
	    System.out.println(Data_Row_No);
	    userName = testData.getCellData("loginToBusinessCentral", "UserName", Data_Row_No);
	    System.out.println(userName);
	    password = testData.getCellData("loginToBusinessCentral", "Password", Data_Row_No);

	    APPLICATION_LOGS.debug("Successfully Retrieved data from Xls File :-  Username : " + userName
		    + " and Password : " + password);

	}

	catch (Throwable fetchExcelDataError) {

	    APPLICATION_LOGS.debug("Error while retrieving data from xls file" + fetchExcelDataError.getMessage());
	    return "Error while retrieving data from xls file" + fetchExcelDataError.getMessage();

	}

	// Clear Username input-box and input username
	methodReturnResult = FunctionLibrary.clearAndInput(locatorUserNameInputBox, nameUserNameInputBox, userName);

	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Clear Password input-box and input password
	methodReturnResult = FunctionLibrary.clearAndInput(locatorPasswordInputBox, namePasswordInputBox, password);

	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Click on Sign In button
	methodReturnResult = FunctionLibrary.clickAndWait(locatorSignInButton, nameSignInButton);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Verify login was successful
	expectedTitle = businessCentralLoginPageTitle;
	String actualTitle = null;

	for (int i = 0; i < 3; i++) {

	    // Get page title
	    actualTitle = FunctionLibrary.getPageTitle();

	    if (actualTitle.equals(expectedTitle)) {

		// Log result
		APPLICATION_LOGS.debug("Failed to login. Retrying ...");

		// Clear Username input-box and input username
		FunctionLibrary.clearAndInput(locatorUserNameInputBox, nameUserNameInputBox, userName);

		// Clear Password input-box and input password
		FunctionLibrary.clearAndInput(locatorPasswordInputBox, namePasswordInputBox, password);

		// Click on Sign In button
		FunctionLibrary.clickAndWait(locatorSignInButton, nameSignInButton);

	    } else {
		break;
	    }

	}

	return "Pass : Logged into business central site";

    }

    // Discharge Add patient function
    public static String addPatient(String patientFirstName, String patientLastName)
	    throws InterruptedException, BiffException, IOException, RowsExceededException, WriteException {

	APPLICATION_LOGS.debug("Add a patient in Discharge Central site...");
	String RUN_DATE = TestUtil.now("ddMMyyhhmmss").toString();

	// Click on Add patient button
	methodReturnResult = FunctionLibrary.clickAndWait(locatorAddPatientBtn, nameAddPatientBtn);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Verify whether the page is navigated to Add patient page
	if (!FunctionLibrary.isElementPresent(locatorPatientForm, namePatientForm)) {
	    methodReturnResult = FunctionLibrary.clickAndWait(locatorAddPatientBtn, nameAddPatientBtn);
	    if (methodReturnResult.contains(failTest)) {
		return methodReturnResult;
	    }
	}

	// Input the admission date as today's date
	Format formatter = new SimpleDateFormat("MMMM");

	patientFirstName = patientFirstName + " " + RUN_DATE;
	patientLastName = patientLastName + " " + RUN_DATE;

	// Input valid first name
	methodReturnResult = FunctionLibrary.clearAndInput(locatorPatientFirstNameInput, namePatientFirstNameInput,
		patientFirstName);
	if (methodReturnResult.contains("Fail")) {
	    return methodReturnResult;
	}

	// Input space in patient's last name
	methodReturnResult = FunctionLibrary.clearAndInput(locatorPatientLastNameInput, namePatientLastNameInput,
		patientLastName);
	if (methodReturnResult.contains("Fail")) {
	    return methodReturnResult;
	}

	// Store the patient full name in the test data sheet
	testData.writeIntoExcel("DischargeComplete", "Patient Full Name", 1, patientLastName + ", " + patientFirstName);

	// Select the gender from drop down
	methodReturnResult = FunctionLibrary.selectValueByIndex(locatorGenderDropdown, 1, nameGenderDropdown);
	if (methodReturnResult.contains("Fail")) {
	    return methodReturnResult;
	}

	// Input Random MRN
	randomMRNNo = FunctionLibrary.generateRandomNumber(8);
	methodReturnResult = FunctionLibrary.clearAndInput(locatorMRNInputXpath, nameMRNInput, randomMRNNo);
	if (methodReturnResult.contains("Fail")) {
	    return methodReturnResult;
	}

	// Input Random accountID
	String accountID = FunctionLibrary.generateRandomNumber(8);
	methodReturnResult = FunctionLibrary.clearAndInput(locatorAccountIDInput, "Account ID", accountID);
	if (methodReturnResult.contains("Fail")) {
	    return methodReturnResult;
	}

	// Select the Admit type from drop down
	methodReturnResult = FunctionLibrary.selectValueByIndex(locatorAdmitTypeDropdown, 1, nameAdmitTypeDropdown);
	if (methodReturnResult.contains("Fail")) {
	    return methodReturnResult;
	}

	// Select the Date of birth with a valid date
	Date newDOBDate = FunctionLibrary.subtractDate(370);
	String dobMonth = formatter.format(newDOBDate);
	Format yearFormatter = new SimpleDateFormat("yyyy");
	String dobYear = yearFormatter.format(newDOBDate);
	Format dateFormatter = new SimpleDateFormat("d");
	String dobDate = dateFormatter.format(newDOBDate);

	// Select the Calendar
	methodReturnResult = FunctionLibrary.clickLink(locatorDOBCalendar, nameDOBCalendar);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Click on month link
	methodReturnResult = FunctionLibrary.clickLink(locatorDOBMonthLink, nameDischargeMonthLink);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Select the month from dropdown
	methodReturnResult = FunctionLibrary.selectValueByVisibleText(locatorDOBMonthDropdown, dobMonth,
		nameDOBMonthDropdown);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Type the year in year text box
	methodReturnResult = FunctionLibrary.clearAndInput(locatorDOBYearInput, nameDOBYearInput, dobYear);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Click on ok button
	methodReturnResult = FunctionLibrary.clickLink(locatorDOBSubmitBtn, nameDOBSubmitBtn);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Select the date
	methodReturnResult = FunctionLibrary.clickLink(By.linkText(dobDate), "Birth date");
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Click on Admit date
	// Select the Calendar
	methodReturnResult = FunctionLibrary.clickLink(locatorAdmitDateCalendar, nameAdmitDateCalendar);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	Date newDate = new Date();
	newDate.setDate(newDate.getDate() - 2);

	String reqMonth = formatter.format(newDate);
	String reqYear = yearFormatter.format(newDate);
	String reqDate = dateFormatter.format(newDate);

	// Click the month on calendar
	methodReturnResult = FunctionLibrary.clickLink(locatorAdmitDateMonthLink, "'Admit Date Month' link");
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Select the month from drop down
	methodReturnResult = FunctionLibrary.selectValueByVisibleText(locatorAdmitDateMonthDrpDwn, reqMonth,
		nameDischargeMonthLink);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Type the year in year text box
	methodReturnResult = FunctionLibrary.clearAndInput(locatorAdmitDateYearInput, nameDischargeYearInput, reqYear);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Click on ok button
	methodReturnResult = FunctionLibrary.clickLink(locatorAdmitDateOkBtn, nameSubmitBtn);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Select the date
	methodReturnResult = FunctionLibrary.clickLink(By.linkText(reqDate), "Discharge Date");
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	Date estimatedDischargeDate = new Date();
	estimatedDischargeDate.setDate(estimatedDischargeDate.getDate() - 1);
	String nextDaysMonth = formatter.format(estimatedDischargeDate);
	String nextDaysYear = yearFormatter.format(estimatedDischargeDate);
	String nextDate = dateFormatter.format(estimatedDischargeDate);

	// Click Estimated Discharge date calendar
	methodReturnResult = FunctionLibrary.clickLink(locatorDischargeDateCalendar, nameDischargeDateCalendar);
	if (methodReturnResult.contains("Fail")) {
	    return methodReturnResult;
	}

	// Click Month
	methodReturnResult = FunctionLibrary.clickLink(locatorDischargeMonthLink, nameDischargeMonthLink);
	if (methodReturnResult.contains("Fail")) {
	    return methodReturnResult;
	}

	// Select the month from drop down
	methodReturnResult = FunctionLibrary.selectValueByVisibleText(locatorMonthDropdown, nextDaysMonth,
		nameMonthDropdown);
	if (methodReturnResult.contains("Fail")) {
	    return methodReturnResult;
	}

	// Type the year in year text box
	methodReturnResult = FunctionLibrary.clearAndInput(locatorDischargeYearInput, nameDischargeYearInput,
		nextDaysYear);
	if (methodReturnResult.contains("Fail")) {
	    return methodReturnResult;
	}

	// Click on ok button
	methodReturnResult = FunctionLibrary.clickLink(locatorSubmitBtn, nameSubmitBtn);
	if (methodReturnResult.contains("Fail")) {
	    return methodReturnResult;
	}

	// Select the date
	methodReturnResult = FunctionLibrary.clickLink(By.linkText(nextDate), "Discharge Date");
	if (methodReturnResult.contains("Fail")) {
	    return methodReturnResult;
	}

	// Select the value from primary diagnosis
	methodReturnResult = FunctionLibrary.selectValueByIndex(locatorPrimaryDiagnosisDrpdwn, 1,
		namePrimaryDiagnosisDrpdwn);
	if (methodReturnResult.contains("Fail")) {
	    return methodReturnResult;
	}

	// Select the insurance plan ID
	methodReturnResult = FunctionLibrary.selectValueByIndex(locatorPlanId, 1, namePlanId);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Click on save button
	methodReturnResult = FunctionLibrary.clickAndWait(locatorSaveBtn, nameSaveBtn);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Wait for the result to appear
	if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
	    FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
	}

	// Check if the duplicate patient dialog is present or not
	if (FunctionLibrary.isElementDisplayed(locatorDuplicatePatientOverlay, nameDuplicatePatientOverlay)) {
	    methodReturnResult = FunctionLibrary.clickAndWait(locatorSavePatientBtn, nameSavePatientBtn);
	    if (methodReturnResult.contains(failTest)) {
		return methodReturnResult;
	    }
	}

	// Assert the success text
	String addPatientSuccessText = FunctionLibrary.retrieveText(locatorAddPatientSuccess,
		nameAddPatientSuccessText);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	return "Pass : Added a new patient in Discharge Central site";

    }

    // Discharge Add patient function
    public static String addPatientWithVariedData(String patientFirstName, String patientLastName, String MRN,
	    String AccountID)
	    throws InterruptedException, BiffException, IOException, RowsExceededException, WriteException {

	APPLICATION_LOGS.debug("Add a patient in Discharge Central site...");
	String RUN_DATE = TestUtil.now("ddMMyyhhmmss").toString();

	// Click on Add patient button
	methodReturnResult = FunctionLibrary.clickAndWait(locatorAddPatientBtn, nameAddPatientBtn);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Verify whether the page is navigated to Add patient page
	if (!FunctionLibrary.isElementPresent(locatorPatientForm, namePatientForm)) {
	    methodReturnResult = FunctionLibrary.clickAndWait(locatorAddPatientBtn, nameAddPatientBtn);
	    if (methodReturnResult.contains(failTest)) {
		return methodReturnResult;
	    }
	}

	// Input the admission date as today's date
	Format formatter = new SimpleDateFormat("MMMM");

	patientFirstName = patientFirstName;
	patientLastName = patientLastName;

	// Input valid first name
	methodReturnResult = FunctionLibrary.clearAndInput(locatorPatientFirstNameInput, namePatientFirstNameInput,
		patientFirstName);
	if (methodReturnResult.contains("Fail")) {
	    return methodReturnResult;
	}

	// Input space in patient's last name
	methodReturnResult = FunctionLibrary.clearAndInput(locatorPatientLastNameInput, namePatientLastNameInput,
		patientLastName);
	if (methodReturnResult.contains("Fail")) {
	    return methodReturnResult;
	}

	// Store the patient full name in the test data sheet
	// testData.writeIntoExcel("DischargeComplete", "Patient Full Name", 1,
	// patientLastName + ", " + patientFirstName);

	// Select the gender from drop down
	methodReturnResult = FunctionLibrary.selectValueByIndex(locatorGenderDropdown, 1, nameGenderDropdown);
	if (methodReturnResult.contains("Fail")) {
	    return methodReturnResult;
	}

	System.out.println("--12--");

	if (MRN != null) {

	    System.out.println("--1--");
	    // Input Random MRN
	    methodReturnResult = FunctionLibrary.clearAndInput(locatorMRNInput, nameMRNInput, MRN);
	    if (methodReturnResult.contains("Fail")) {
		return methodReturnResult;
	    }
	}

	if (AccountID != null) {
	    // Input Random Account Number
	    methodReturnResult = FunctionLibrary.clearAndInput(locatorAccountNoInput, nameAccountNoInput, AccountID);
	    if (methodReturnResult.contains("Fail")) {
		return methodReturnResult;
	    }
	}

	Thread.sleep(1000L);

	// Select the Admit type from drop down
	methodReturnResult = FunctionLibrary.selectValueByIndex(locatorAdmitTypeDropdown, 1, nameAdmitTypeDropdown);
	if (methodReturnResult.contains("Fail")) {
	    return methodReturnResult;
	}

	// Select the Date of birth with a valid date
	Date newDOBDate = FunctionLibrary.subtractDate(370);
	String dobMonth = formatter.format(newDOBDate);
	Format yearFormatter = new SimpleDateFormat("yyyy");
	String dobYear = yearFormatter.format(newDOBDate);
	Format dateFormatter = new SimpleDateFormat("d");
	String dobDate = dateFormatter.format(newDOBDate);

	// Select the Calendar
	methodReturnResult = FunctionLibrary.clickLink(locatorDOBCalendar, nameDOBCalendar);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Click on month link
	methodReturnResult = FunctionLibrary.clickLink(locatorDOBMonthLink, nameDischargeMonthLink);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Select the month from dropdown
	methodReturnResult = FunctionLibrary.selectValueByVisibleText(locatorDOBMonthDropdown, dobMonth,
		nameDOBMonthDropdown);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Type the year in year text box
	methodReturnResult = FunctionLibrary.clearAndInput(locatorDOBYearInput, nameDOBYearInput, dobYear);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Click on ok button
	methodReturnResult = FunctionLibrary.clickLink(locatorDOBSubmitBtn, nameDOBSubmitBtn);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Select the date
	methodReturnResult = FunctionLibrary.clickLink(By.linkText(dobDate), "Birth date");
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Click on Admit date
	// Select the Calendar
	methodReturnResult = FunctionLibrary.clickLink(locatorAdmitDateCalendar, nameAdmitDateCalendar);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	Date newDate = new Date();
	newDate.setDate(newDate.getDate() - 2);

	String reqMonth = formatter.format(newDate);
	String reqYear = yearFormatter.format(newDate);
	String reqDate = dateFormatter.format(newDate);

	// Click the month on calendar
	methodReturnResult = FunctionLibrary.clickLink(locatorAdmitDateMonthLink, "'Admit Date Month' link");
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Select the month from drop down
	methodReturnResult = FunctionLibrary.selectValueByVisibleText(locatorAdmitDateMonthDrpDwn, reqMonth,
		nameDischargeMonthLink);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Type the year in year text box
	methodReturnResult = FunctionLibrary.clearAndInput(locatorAdmitDateYearInput, nameDischargeYearInput, reqYear);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Click on ok button
	methodReturnResult = FunctionLibrary.clickLink(locatorAdmitDateOkBtn, nameSubmitBtn);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Select the date
	methodReturnResult = FunctionLibrary.clickLink(By.linkText(reqDate), "Discharge Date");
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	Date estimatedDischargeDate = new Date();
	estimatedDischargeDate.setDate(estimatedDischargeDate.getDate() - 1);
	String nextDaysMonth = formatter.format(estimatedDischargeDate);
	String nextDaysYear = yearFormatter.format(estimatedDischargeDate);
	String nextDate = dateFormatter.format(estimatedDischargeDate);

	// Click Estimated Discharge date calendar
	methodReturnResult = FunctionLibrary.clickLink(locatorDischargeDateCalendar, nameDischargeDateCalendar);
	if (methodReturnResult.contains("Fail")) {
	    return methodReturnResult;
	}

	// Click Month
	methodReturnResult = FunctionLibrary.clickLink(locatorDischargeMonthLink, nameDischargeMonthLink);
	if (methodReturnResult.contains("Fail")) {
	    return methodReturnResult;
	}

	// Select the month from drop down
	methodReturnResult = FunctionLibrary.selectValueByVisibleText(locatorMonthDropdown, nextDaysMonth,
		nameMonthDropdown);
	if (methodReturnResult.contains("Fail")) {
	    return methodReturnResult;
	}

	// Type the year in year text box
	methodReturnResult = FunctionLibrary.clearAndInput(locatorDischargeYearInput, nameDischargeYearInput,
		nextDaysYear);
	if (methodReturnResult.contains("Fail")) {
	    return methodReturnResult;
	}

	// Click on ok button
	methodReturnResult = FunctionLibrary.clickLink(locatorSubmitBtn, nameSubmitBtn);
	if (methodReturnResult.contains("Fail")) {
	    return methodReturnResult;
	}

	// Select the date
	methodReturnResult = FunctionLibrary.clickLink(By.linkText(nextDate), "Discharge Date");
	if (methodReturnResult.contains("Fail")) {
	    return methodReturnResult;
	}

	// Select the value from primary diagnosis
	methodReturnResult = FunctionLibrary.selectValueByIndex(locatorPrimaryDiagnosisDrpdwn, 1,
		namePrimaryDiagnosisDrpdwn);
	if (methodReturnResult.contains("Fail")) {
	    return methodReturnResult;
	}

	// Select the insurance plan ID
	methodReturnResult = FunctionLibrary.selectValueByIndex(locatorPlanId, 1, namePlanId);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Click on save button
	methodReturnResult = FunctionLibrary.clickAndWait(locatorSaveBtn, nameSaveBtn);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Wait for the result to appear
	if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
	    FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
	}

	// Check if the duplicate patient dialog is present or not
	if (FunctionLibrary.isElementDisplayed(locatorDuplicatePatientOverlay, nameDuplicatePatientOverlay)) {
	    methodReturnResult = FunctionLibrary.clickAndWait(locatorSavePatientBtn, nameSavePatientBtn);
	    if (methodReturnResult.contains(failTest)) {
		return methodReturnResult;
	    }
	}

	// Assert the success text
	String addPatientSuccessText = FunctionLibrary.retrieveText(locatorAddPatientSuccess,
		nameAddPatientSuccessText);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	System.out.println(addPatientSuccessText);

	return addPatientSuccessText;

    }

    // Discharge Add patient function
    public static String updatePatientWithVariedData(String MRN, String AccountID)
	    throws InterruptedException, BiffException, IOException, RowsExceededException, WriteException {

	APPLICATION_LOGS.debug("Updating patient info in Intake Page...");

	// Verify whether the page is navigated to Add patient page
	if (FunctionLibrary.isElementPresent(locatorIntakePage, "Intake Page")) {
	    methodReturnResult = FunctionLibrary.clickAndWait(locatorIntakePage, "Intake Page");
	    if (methodReturnResult.contains(failTest)) {
		return methodReturnResult;
	    }
	}

	// Verify if the page is navigated to Intake page
	if (!FunctionLibrary.isElementPresent(locatorPatientForm, namePatientForm)) {
	    return failTest + "Patient form is not loaded, Page is not navigated to Intake page";
	}

	if (MRN != null) {

	    // Input Random MRN
	    methodReturnResult = FunctionLibrary.clearAndInput(locatorMRNInput, nameMRNInput, MRN);
	    if (methodReturnResult.contains("Fail")) {
		return methodReturnResult;
	    }
	}

	if (MRN == null) {

	    // Clear MRN field
	    methodReturnResult = FunctionLibrary.clearField(locatorMRNInput, nameMRNInput);
	    if (methodReturnResult.contains("Fail")) {
		return methodReturnResult;
	    }
	}

	if (AccountID != null) {
	    // Input Random Account Number
	    methodReturnResult = FunctionLibrary.clearAndInput(locatorAccountNoInput, nameAccountNoInput, AccountID);
	    if (methodReturnResult.contains("Fail")) {
		return methodReturnResult;
	    }
	}

	if (AccountID == null) {
	    // Clear Account Number
	    methodReturnResult = FunctionLibrary.clearField(locatorAccountNoInput, nameAccountNoInput);
	    if (methodReturnResult.contains("Fail")) {
		return methodReturnResult;
	    }
	}

	Thread.sleep(1000L);

	// Click on save button
	methodReturnResult = FunctionLibrary.clickAndWait(locatorSaveBtn, nameSaveBtn);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Wait for the result to appear
	if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
	    FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
	}

	// Check if the duplicate patient dialog is present or not
	if (FunctionLibrary.isElementDisplayed(locatorDuplicatePatientOverlay, nameDuplicatePatientOverlay)) {
	    methodReturnResult = FunctionLibrary.clickAndWait(locatorSavePatientBtn, nameSavePatientBtn);
	    if (methodReturnResult.contains(failTest)) {
		return methodReturnResult;
	    }
	}

	// Assert the success text
	String addPatientSuccessText = FunctionLibrary.retrieveText(locatorAddPatientSuccess,
		nameAddPatientSuccessText);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	System.out.println(addPatientSuccessText);

	return addPatientSuccessText;

    }

    // Search patient in Discharge page
    public static String searchPatient(String patientFullName) throws InterruptedException, BiffException, IOException {

	APPLICATION_LOGS.debug("Searching for patient in Discharge page ...");

	// Search for patient in Discharge tab
	methodReturnResult = FunctionLibrary.clearAndInput(locatorSearchPatientInput, nameSearchPatientInput,
		patientFullName);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Click on Search link
	methodReturnResult = FunctionLibrary.clickLink(locatorSearchLink, nameSearchLink);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Wait for the result to appear
	if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
	    FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
	}

	// Wait for the Loading text to disappear
	if (FunctionLibrary.isElementDisplayed(locatorLoadingText, nameLoadingText)) {
	    FunctionLibrary.waitForElementToDisappear(locatorLoadingText, nameLoadingText);
	}

	return "Pass : Searched for patient in Discharge page";

    }

    // Navigate to Intake page
    public static String navigateToIntakePage() throws InterruptedException, BiffException, IOException {

	APPLICATION_LOGS.debug("Navigating to Intake page page ...");

	highlightElement = false;
	// Click on Intake Link
	methodReturnResult = FunctionLibrary.clickLink(locatorIntakeLink, nameIntakeLink);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Wait for the waiting div to disappear
	if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, "Wait Div")) {
	    FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, "Wait Div");
	}

	// Wait for page to load
	FunctionLibrary.waitForPageToLoad();

	// Verify if the page is navigated to Intake page
	if (!FunctionLibrary.isElementPresent(locatorPatientForm, namePatientForm)) {
	    return failTest + "Patient form is not loaded, Page is not navigated to Intake page";
	}

	highlightElement = true;
	return "Pass : Navigated to Intake Page";

    }

    // Navigate to Discharge page
    public static String navigateToMatchingPage() throws InterruptedException, BiffException, IOException {

	APPLICATION_LOGS.debug("Navigating to Matching page page ...");

	highlightElement = false;
	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

	// Click on Matching Link
	methodReturnResult = FunctionLibrary.clickLink(locatorMatchingLink, nameMatchingLink);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Wait for the waiting div to disappear
	if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, "Wait Div")) {
	    FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, "Wait Div");
	}

	// Wait for page to load
	FunctionLibrary.waitForPageToLoad();

	// Verify the presence of matching layout
	if (!FunctionLibrary.isElementPresent(locatorMatchingLayout, nameMatchingLayout)) {
	    return failTest + " : Matching layout is not loaded, Page is not navigated to Matching page";
	}

	highlightElement = true;
	return "Pass : Navigated to Matching Page";

    }
    
    
   
    
    
    // Navigate to Selecting Subscriber
    public static String selectingSubscriberformWorkbook() throws InterruptedException, BiffException, IOException {

	APPLICATION_LOGS.debug("Navigating to workbook page ...");

	
	
	
	highlightElement = false;
	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

	// Click on Workbook page
	methodReturnResult = FunctionLibrary.clickLink(locatorWorkbookPage, "Navigating to workbook");
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Wait for page to load
	FunctionLibrary.waitForPageToLoad();
	
	
		// Select Patient
				methodReturnResult = FunctionLibrary.clickLink(locatorSelectPatient, "Selecting Action");
				if (methodReturnResult.contains(failTest)) {
				    return methodReturnResult;		
		}
		/*
				//Select Dropdown
				methodReturnResult = FunctionLibrary.clickLink(locatorSelectSubscriber, "Selecting Action");
				if (methodReturnResult.contains(failTest)) {
				    return methodReturnResult;		
		}*/
				// Select Subscriber
				methodReturnResult = FunctionLibrary.clearAndInput(locatorTypeSubscriber, "Subscriber", "Pml, Subscriber");
				if (methodReturnResult.contains(failTest)) {
				    return methodReturnResult;		
		}
				driver.findElement(locatorTypeSubscriber).sendKeys(Keys.ENTER);
		
		/*		// Click Dropdoen
				methodReturnResult = FunctionLibrary.clickLink(locatorSelectPatient, "Selecting Action");
				if (methodReturnResult.contains(failTest)) {
				    return methodReturnResult;		
		}*/
		
				Select subscriber = new Select(driver.findElement(locatorUsersDropdown));
				subscriber.selectByVisibleText("PML, Subscriber");
		
				// Wait for the result to appear
				if (FunctionLibrary.driver2IsElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
				    FunctionLibrary.driver2WaitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
				}
		
				// Select Matching Page
				methodReturnResult = FunctionLibrary.clickLink(locatorOfMatchingButton, "Selecting Matching Page");
				if (methodReturnResult.contains(failTest)) {
				    return methodReturnResult;		
		}
	
	
	highlightElement = true;
	return "Pass : Navigated to Matching Page";

    }
    // Navigate to Discharge page
    public static String PMLRequest() throws InterruptedException, BiffException, IOException {

	APPLICATION_LOGS.debug("Clicking On PML");

	highlightElement = false;
	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

	// Click on Matching Link
	methodReturnResult = FunctionLibrary.clickLink(locatorMatchingLink, nameMatchingLink);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Wait for the waiting div to disappear
	if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, "Wait Div")) {
	    FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, "Wait Div");
	}

	// Wait for page to load
	FunctionLibrary.waitForPageToLoad();

	// Verify the presence of matching layout
	if (!FunctionLibrary.isElementPresent(locatorMatchingLayout, nameMatchingLayout)) {
	    return failTest + " : Matching layout is not loaded, Page is not navigated to Matching page";
	}

	highlightElement = true;
	return "Pass : Navigated to Matching Page";

    }

    
    
    
    
    
    // Navigate to Discharge page
    public static String navigateToMatchingPageFromWB() throws InterruptedException, BiffException, IOException {

	APPLICATION_LOGS.debug("Navigating to Matching page page ...");

	highlightElement = false;
	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

	// Click on Matching Link
	methodReturnResult = FunctionLibrary.clickLink(locatorMatchingLinkWBPage, nameMatchingLink);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Wait for the waiting div to disappear
	if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, "Wait Div")) {
	    FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, "Wait Div");
	}

	// Wait for page to load
	FunctionLibrary.waitForPageToLoad();

	// Verify the presence of matching layout
	if (!FunctionLibrary.isElementPresent(locatorMatchingLayout, nameMatchingLayout)) {
	    return failTest + " : Matching layout is not loaded, Page is not navigated to Matching page";
	}

	highlightElement = true;
	return "Pass : Navigated to Matching Page";

    }

    // Navigate to Assessment page
    public static String navigateToAssessmentPage() throws InterruptedException, BiffException, IOException {

	APPLICATION_LOGS.debug("Navigating to Assessment page ...");

	highlightElement = false;
	// Click on assessment link
	methodReturnResult = FunctionLibrary.clickLink(locatorAssessmentLink, nameAssessmentLink);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Wait for the waiting div to disappear
	if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, "Wait Div")) {
	    FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, "Wait Div");
	}

	// Wait for page to load
	FunctionLibrary.waitForPageToLoad();

	// Verify if the page is navigated to Assessment page
	if (!FunctionLibrary.isElementPresent(locatorDocumentTab, nameDocumentTab)) {
	    return failTest + "Document Tab is not loaded, Page is not navigated to Assessment page";
	}

	highlightElement = true;
	return "Pass : Navigated to Assessment Page";

    }

    // Navigate to Implementation page
    public static String navigateToImplementationPage() throws InterruptedException, BiffException, IOException {

	APPLICATION_LOGS.debug("Navigating to Implementation page ...");

	highlightElement = false;
	// // Handle certificate error
	// if (CONFIG.getProperty("test_browser").contains("Internet Explorer"))
	// {
	// // Click on Implementation link
	// driver.executeScript("var el=arguments[0]; setTimeout(function() {
	// el.click(); }, 100);",
	// driver.findElement(locatorImplementationLink));
	// } else {
	// Click on Implementation link
	methodReturnResult = FunctionLibrary.clickLink(locatorImplementationLink, nameImplementationLink);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}
	// }

	// Wait for the waiting div to disappear
	if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, "Wait Div")) {
	    FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, "Wait Div");
	}

	// Wait for page to load
	FunctionLibrary.waitForPageToLoad();

	// Wait for the Loading text to disappear
	if (FunctionLibrary.isElementDisplayed(locatorLoadingText, nameLoadingText)) {
	    FunctionLibrary.waitForElementToDisappear(locatorLoadingText, nameLoadingText);
	}

	// Verify Login page appears
	expectedTitle = ImplementationPageTitle;
	methodReturnResult = FunctionLibrary.assertTitle(expectedTitle);
	if (methodReturnResult.contains(failTest)) {

	    // Log result
	    APPLICATION_LOGS.debug("Not navigated to Implementation page");
	    return methodReturnResult;

	}
	highlightElement = true;
	return "Pass : Navigated to Implementation Page";

    }

    public static int getNumberofPrimaryContact(String hospitalName, String userName) {
	ResultSet rs = null;
	int PrimaryContactCount = 0;

	APPLICATION_LOGS.debug("Getting the count of Primary contact count for the user = " + userName
		+ " and Facility name = " + hospitalName);

	String query = "SELECT id FROM discharge WHERE security_id = (SELECT id FROM security WHERE login_name = '"
		+ userName + "' AND facility_id = (SELECT id FROM facility WHERE name = '" + hospitalName
		+ "') AND active = 'yes')";

	try {

	    // Connecting to Database
	    con = java.sql.DriverManager.getConnection(connectionString, dbUsername, dbPassword);

	    con.setAutoCommit(false);

	    // Create a Statement object for sending SQL statements to the
	    // database
	    stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);

	    // Execute the query
	    rs = stmt.executeQuery(query);

	    while (rs.next()) {
		PrimaryContactCount++;
	    }

	}

	catch (SQLException sqlException) {

	    APPLICATION_LOGS.debug(
		    "Error came while trying to fetch number of outfax search count : " + sqlException.getMessage());

	}

	APPLICATION_LOGS.debug("The count for Primary contact for the user =" + userName + " and facility = "
		+ hospitalName + " is =" + PrimaryContactCount);

	// Return the result set

	return PrimaryContactCount;

    }

    public static int getNumberofSubscribersContact(String userName) {
	ResultSet rs = null;
	int SubscribersContactCount = 0;

	APPLICATION_LOGS.debug("Getting the count of Subscribers contact count for the user = " + userName);

	String query = "SELECT * FROM discharge_subscriber WHERE security_id = (SELECT id FROM security WHERE login_name = '"
		+ userName + "')";

	try {

	    // Connecting to Database
	    con = java.sql.DriverManager.getConnection(connectionString, dbUsername, dbPassword);

	    con.setAutoCommit(false);

	    // Create a Statement object for sending SQL statements to the
	    // database
	    stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);

	    // Execute the query
	    rs = stmt.executeQuery(query);

	    while (rs.next()) {
		SubscribersContactCount++;
	    }

	}

	catch (SQLException sqlException) {

	    APPLICATION_LOGS.debug(
		    "Error came while trying to fetch number of outfax search count : " + sqlException.getMessage());

	}

	APPLICATION_LOGS.debug(
		"The count for Subscribers contact for the user =" + userName + "is =" + SubscribersContactCount);

	// Return the result set

	return SubscribersContactCount;

    }

    public static ResultSet getFacilityIdOfHospital(String hospitalName) {
	ResultSet rs = null;

	APPLICATION_LOGS.debug("Getting the facility id for hospital = " + hospitalName);

	String query = "Select id FROM facility WHERE name = '" + hospitalName + "'";

	try {

	    // Connecting to Database
	    con = java.sql.DriverManager.getConnection(connectionString, dbUsername, dbPassword);

	    con.setAutoCommit(false);

	    // Create a Statement object for sending SQL statements to the
	    // database
	    stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);

	    // Execute the query
	    rs = stmt.executeQuery(query);

	}

	catch (SQLException sqlException) {

	    APPLICATION_LOGS.debug(
		    "Error came while trying to fetch facility id for the hospital : " + sqlException.getMessage());

	}

	APPLICATION_LOGS.debug("The facility id for the hospital = " + hospitalName + " is =" + rs);

	// Return the result set

	return rs;

    }

    public static String navigateToFacilityCFG() throws InterruptedException, BiffException, IOException {

	APPLICATION_LOGS.debug("Navigating to Facility CFG submenu under Tools ...");

	// Click on 'Tools' tab and wait for page to load
	methodReturnResult = FunctionLibrary.clickLink(locatorToolsTab, nameToolsTab);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Click on Facility CFG sub menu
	methodReturnResult = FunctionLibrary.clickAndWait(locatorFacilityCFGMnu, nameFacilityCFGMnu);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Verify we navigated to Facility CFG page
	expectedTitle = facilityCFGPageTitle;
	methodReturnResult = FunctionLibrary.assertTitle(expectedTitle);
	if (methodReturnResult.contains(failTest)) {

	    APPLICATION_LOGS.debug("Re-trying to go to the facility CFG page ...");

	    // Click on 'Tools' tab and wait for page to load
	    methodReturnResult = FunctionLibrary.clickLink(locatorToolsTab, nameToolsTab);
	    if (methodReturnResult.contains(failTest)) {
		return methodReturnResult;
	    }

	    // Click on Facility CFG sub menu
	    methodReturnResult = FunctionLibrary.clickAndWait(locatorFacilityCFGMnu, nameFacilityCFGMnu);
	    if (methodReturnResult.contains(failTest)) {
		return methodReturnResult;
	    }

	    // Verify we navigated to Facility CFG page
	    expectedTitle = facilityCFGPageTitle;
	    methodReturnResult = FunctionLibrary.assertTitle(expectedTitle);
	    if (methodReturnResult.contains(failTest)) {
		return methodReturnResult;
	    }
	}
	return "Pass : Navigated to Facility CFG page";

    }

    public static int getNumberofSubscribers(String MRN) {
	ResultSet rs = null;
	int subscribersCount = 0;

	APPLICATION_LOGS.debug("Getting the count of sbubscribers for MRN = " + MRN);

	String query = "(SELECT con.last_name || ', ' || con.first_name FROM discharge_subscriber ds, security sec, contact con WHERE ds.security_id = sec.id AND sec.contact_id = con.id AND ds.discharge_id = (SELECT id FROM discharge WHERE mrn = '"
		+ MRN
		+ "')) UNION (SELECT last_name || ', ' || first_name FROM contact WHERE id = (SELECT contact_id FROM security WHERE id = (SELECT security_id FROM discharge WHERE mrn = '"
		+ MRN + "')))";

	try {

	    // Connecting to Database
	    con = java.sql.DriverManager.getConnection(connectionString, dbUsername, dbPassword);

	    con.setAutoCommit(false);

	    // Create a Statement object for sending SQL statements to the
	    // database
	    stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);

	    // Execute the query
	    rs = stmt.executeQuery(query);

	    while (rs.next()) {
		subscribersCount++;
	    }

	}

	catch (SQLException sqlException) {

	    APPLICATION_LOGS.debug(
		    "Error came while trying to fetch number of subscribers count : " + sqlException.getMessage());

	}

	APPLICATION_LOGS
		.debug("The count for subscribers for the patient with MRN =" + MRN + " is =" + subscribersCount);

	// Return subscribers count
	return subscribersCount;

    }

    public static ArrayList<String> getNamesOfSubscribers(String MRN) {
	ResultSet rs = null;
	int subscribersCount = 0;
	ArrayList<String> results = new ArrayList<String>();

	APPLICATION_LOGS.debug("Getting the names of sbubscribers for MRN = " + MRN);

	String query = "(SELECT con.last_name || ', ' || con.first_name FROM discharge_subscriber ds, security sec, contact con WHERE ds.security_id = sec.id AND sec.contact_id = con.id AND ds.discharge_id = (SELECT id FROM discharge WHERE mrn = '"
		+ MRN
		+ "')) UNION (SELECT last_name || ', ' || first_name FROM contact WHERE id = (SELECT contact_id FROM security WHERE id = (SELECT security_id FROM discharge WHERE mrn = '"
		+ MRN + "')))";

	try {

	    // Connecting to Database
	    con = java.sql.DriverManager.getConnection(connectionString, dbUsername, dbPassword);

	    con.setAutoCommit(false);

	    // Create a Statement object for sending SQL statements to the
	    // database
	    stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);

	    // Execute the query
	    rs = stmt.executeQuery(query);
	    ResultSetMetaData metadata = rs.getMetaData();
	    int numcols = metadata.getColumnCount();

	    while (rs.next()) {
		int i = 1;
		while (i < numcols) {
		    results.add(rs.getString(i++));
		}
	    }

	}

	catch (SQLException sqlException) {

	    APPLICATION_LOGS
		    .debug("Error came while trying to fetch names of subscribers : " + sqlException.getMessage());

	}

	APPLICATION_LOGS.debug("The names of subscribers for the patient with MRN =" + MRN + " is retrieved");

	// Return subscribers count
	return results;

    }

    // Navigate to Implementation page
    public static String launchMidasSSO(String userName, String hospitalID, String episode, String launchType)
	    throws InterruptedException, BiffException, IOException {

	APPLICATION_LOGS.debug("Launch Discharge Central site via Midas SSO ...");

	try {

	    // Navigate to Curaspan application
	    driver.navigate().to("https://s-qa-vm-build-01.curaspan.local/midaslogin.cfm?login_name=" + userName
		    + "&hospital_id=" + hospitalID + "&episode=" + episode + "&launch_type=" + launchType + "");

	    // Handle certificate error
	    if (CONFIG.getProperty("test_browser").contains("Internet Explorer")) {

		if (driver.getTitle().contains(navigationBlockedTitle)) {

		    driver.navigate().to("javaScript:document.getElementById('overridelink').click()");

		    FunctionLibrary.waitForPageToLoad();

		}

	    }

	    // Maximize browser window
	    APPLICATION_LOGS.debug("Maximizing Browser window...");
	    driver.manage().window().maximize();
	    APPLICATION_LOGS.debug("Browser window is maximized");

	    if (CONFIG.getProperty("test_browser").contains("Firefox")) {
		WebElement html = driver.findElement(By.tagName("html"));
		html.sendKeys(Keys.chord(Keys.CONTROL, "0"));
	    }

	    if (FunctionLibrary.isElementDisplayed(locatorUnsupportBrowserDialog, nameUnsupportBrowserDialog)) {
		methodReturnResult = FunctionLibrary.clickLink(locatorUnsupportBrowserDialog,
			nameUnsupportBrowserDialog);
		if (methodReturnResult.contains(failTest)) {
		    return methodReturnResult;
		}
	    }
	}

	catch (Throwable navigationError) {

	    APPLICATION_LOGS.debug(
		    "Error came while navigating to the Discharge Central site : " + navigationError.getMessage());

	}

	return "Pass : Navigated to Implementation Page";

    }

    // Navigate to Implementation page
    public static String launchSAMLSSO(String hospitalID, String episode)
	    throws InterruptedException, BiffException, IOException {

	APPLICATION_LOGS.debug("Launch Discharge Central site via SAML SSO ...");

	try {

	    System.out.println(
		    "https://s-qa-vm-build-01.curaspan.local/CuraspanApplicationSuite/index.cfm?event=public.common.sys.ssoLogin&ssoType="
			    + hospitalID + "&episode=" + episode + "");
	    // Navigate to Curaspan application
	    driver.navigate().to(
		    "https://s-qa-vm-build-01.curaspan.local/CuraspanApplicationSuite/index.cfm?event=public.common.sys.ssoLogin&ssoType="
			    + hospitalID + "&episode=" + episode + "");

	    // Handle certificate error
	    if (CONFIG.getProperty("test_browser").contains("Internet Explorer")) {

		if (driver.getTitle().contains(navigationBlockedTitle)) {

		    driver.navigate().to("javaScript:document.getElementById('overridelink').click()");

		    FunctionLibrary.waitForPageToLoad();

		}

	    }

	    // Maximize browser window
	    APPLICATION_LOGS.debug("Maximizing Browser window...");
	    driver.manage().window().maximize();
	    APPLICATION_LOGS.debug("Browser window is maximized");

	    if (CONFIG.getProperty("test_browser").contains("Firefox")) {
		WebElement html = driver.findElement(By.tagName("html"));
		html.sendKeys(Keys.chord(Keys.CONTROL, "0"));
	    }

	    if (FunctionLibrary.isElementDisplayed(locatorUnsupportBrowserDialog, nameUnsupportBrowserDialog)) {
		methodReturnResult = FunctionLibrary.clickLink(locatorUnsupportBrowserDialog,
			nameUnsupportBrowserDialog);
		if (methodReturnResult.contains(failTest)) {
		    return methodReturnResult;
		}
	    }
	}

	catch (Throwable navigationError) {

	    APPLICATION_LOGS.debug("Error came while launching SAML SSO : " + navigationError.getMessage());

	}

	return "Pass : Launched SAML SSO Page";

    }

    public static ResultSet getFormLockedUser(String formID) {
	ResultSet rs = null;

	APPLICATION_LOGS.debug("Getting the name of the user who locked the form = " + formID);

	String query = "SELECT login_name FROM security WHERE id IN(SELECT security_id FROM form_lock WHERE form_id = '"
		+ formID + "')";

	System.out.println(
		"SELECT login_name FROM security WHERE id = (SELECT security_id FROM form_lock WHERE form_id = '"
			+ formID + "')");
	try {

	    // Connecting to Database
	    con = java.sql.DriverManager.getConnection(connectionString, dbUsername, dbPassword);

	    con.setAutoCommit(false);

	    // Create a Statement object for sending SQL statements to the
	    // database
	    stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);

	    // Execute the query
	    rs = stmt.executeQuery(query);

	}

	catch (SQLException sqlException) {

	    APPLICATION_LOGS
		    .debug("Error came while trying to fetch user who locked form: " + sqlException.getMessage());

	}

	APPLICATION_LOGS.debug("The user who locked form = " + formID + " is =" + rs);

	// Return the result set

	return rs;

    }

    // Search patient in Discharge page
    public static String driver2SearchPatient(String patientFullName)
	    throws InterruptedException, BiffException, IOException {

	APPLICATION_LOGS.debug("Searching for patient in Discharge page ...");

	// Search for patient in Discharge tab
	methodReturnResult = FunctionLibrary.driver2ClearAndInput(locatorSearchPatientInput, nameSearchPatientInput,
		patientFullName);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Click on Search link
	methodReturnResult = FunctionLibrary.driver2ClickLink(locatorSearchLink, nameSearchLink);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Wait for the result to appear
	if (FunctionLibrary.driver2IsElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
	    FunctionLibrary.driver2WaitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
	}

	// Wait for the Loading text to disappear
	if (FunctionLibrary.driver2IsElementDisplayed(locatorLoadingText, nameLoadingText)) {
	    FunctionLibrary.driver2WaitForElementToDisappear(locatorLoadingText, nameLoadingText);
	}

	// Check for the presence of searched patient
	if (!FunctionLibrary.driver2IsElementDisplayed(By.xpath("//a[contains(text(),'" + patientFullName + "')]"),
		"Patient Link")) {
	    return failTest + "Searched patient doesnot exist";
	}

	return "Pass : Searched for patient in Discharge page";

    }

    // Check for the patient in workbook page, if patient is not present add a
    // new patient
    public static String preRequisiteCaseLockingAddPatient(String patientFirstName, String patientLastName)
	    throws BiffException, InterruptedException, IOException, RowsExceededException, WriteException {

	APPLICATION_LOGS.debug("Check for the patient in workbook page, if patient is not present add a new patient");

	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

	// Check for the presence of patient under workbook
	String totalPatient = FunctionLibrary.getTableRowCount(
		By.xpath("//div[@id='workbookPanelTable']/div[3]/table/tbody[2]/tr"), "'Workbook Patient Count'");
	if (totalPatient.contains(failTest)) {
	    return totalPatient;
	}

	if (Integer.parseInt(totalPatient) == 0) {
	    // Add a patient if the total patient under workbook is zero
	    addPatient(patientFirstName, patientLastName);

	    // Click Discharge tab to return to workbook
	    methodReturnResult = FunctionLibrary.clickAndWait(locatorDischargeTab, nameDischargeTab);
	    if (methodReturnResult.contains(failTest)) {
		return methodReturnResult;
	    }

	    // Wait for the result to appear
	    if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
		FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
	    }

	    // Wait for the Loading text to disappear
	    if (FunctionLibrary.isElementDisplayed(locatorLoadingText, nameLoadingText)) {
		FunctionLibrary.waitForElementToDisappear(locatorLoadingText, nameLoadingText);
	    }

	}

	else {
	    APPLICATION_LOGS.debug("Patient is already present under workbook");
	}

	return "Pass : Checked for the patient in workbook page, if patient is not present added a new patient.";
    }

    // Delete test data created during automation scripts execution
    public static void purgeDischargeCentralTestData(String facilityId) {

	APPLICATION_LOGS.debug("Purging test data created during automation scripts execution ...");

	// Purge test data created
	// Store the query
	String purgeDischargeWorkbookQuery = "UPDATE discharge SET active = 'no' WHERE active = 'yes' AND facility_id = "
		+ facilityId;

	try {

	    // Connecting to Database
	    con = java.sql.DriverManager.getConnection(connectionString, dbUsername, dbPassword);
	    con.setAutoCommit(false);

	    // Create a Statement object for sending SQL statements
	    // to the database
	    Statement stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);

	    // Execute the delete query
	    int purged = stmt.executeUpdate(purgeDischargeWorkbookQuery);
	    APPLICATION_LOGS.debug("Purged : " + purged + " rows");

	    // Commit the changes
	    APPLICATION_LOGS.debug("Commiting changes ...");
	    con.commit();
	    APPLICATION_LOGS.debug("Committed changes successfully");

	}

	catch (SQLException sqlException) {

	    APPLICATION_LOGS.debug("Error came while trying to purge patient records : " + sqlException.getMessage());

	}

    }

    // Navigate to Discharge page
    public static String selectHospitalInDischargeTab(String dischargeHospitalName)
	    throws InterruptedException, BiffException, IOException {

	APPLICATION_LOGS.debug("Navigating to Discharge tab and select a hospital from the dropdown ...");

	if (CONFIG.getProperty("test_browser").toLowerCase().contains("internet explorer")
		|| CONFIG.getProperty("test_browser").toLowerCase().contains("ie")) {
	    ((JavascriptExecutor) driver).executeScript(
		    "var el=arguments[0]; setTimeout(function() { el.click(); }, 500);",
		    driver.findElement(locatorDischargeTab));
	} else {
	    // Click on 'Discharge' tab and wait for page to load
	    methodReturnResult = FunctionLibrary.clickAndWait(locatorDischargeTab, nameDischargeTab);
	    if (methodReturnResult.contains(failTest)) {
		return methodReturnResult;
	    }
	}

	// Wait for the result to appear
	if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
	    FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
	}

	// Verify we navigated to Workbook page
	expectedTitle = DischargeWorkbookPageTitle;
	methodReturnResult = FunctionLibrary.assertTitle(expectedTitle);
	if (methodReturnResult.contains(failTest)) {

	    APPLICATION_LOGS.debug("Re-trying to go to the Discharge page ...");

	    // Click on 'Discharge' tab and wait for page to load
	    methodReturnResult = FunctionLibrary.clickAndWait(locatorDischargeTab, nameDischargeTab);
	    if (methodReturnResult.contains(failTest)) {
		return methodReturnResult;
	    }

	    // Wait for the result to appear
	    if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
		FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
	    }
	}

	methodReturnResult = FunctionLibrary.assertTitle(expectedTitle);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Select the hospital dropdown
	methodReturnResult = FunctionLibrary.clickLink(locatorHospitalDropdown, nameHospitalDropdown);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Select the hospital from the dropdown
	methodReturnResult = FunctionLibrary
		.clickLink(By.xpath("//a[contains(text(),'" + dischargeHospitalName + "')]"), "Hospital dropdown");
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Wait for the result to appear
	if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
	    FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
	}

	// Wait for the Loading text to disappear
	if (FunctionLibrary.isElementDisplayed(locatorLoadingText, nameLoadingText)) {
	    FunctionLibrary.waitForElementToDisappear(locatorLoadingText, nameLoadingText);
	}

	return "Pass : Navigated to Discharge tab and selected hospital from the dropdown";

    }
    
    // Navigate to base64encode
    public static String encodeValues(String MRN,String Accno ,String facilityID)
	    throws InterruptedException, BiffException, IOException {

	APPLICATION_LOGS.debug("Navigating to https://www.base64encode.org/.");


	// Disable log messages
	java.util.logging.Logger.getLogger("org.apache.http.impl.client").setLevel(java.util.logging.Level.WARNING);
	if (wbdv2 == null) {
	    try {

		if (CONFIG.getProperty("is_remote").equals("true")) {

		    // Generate Remote address
		    String remote_address = "http://" + CONFIG.getProperty("remote_ip") + ":4444/wd/hub";
		    remote_url = new URL(remote_address);

		    ProfilesIni allProfiles = new ProfilesIni();
		    FirefoxProfile profile = allProfiles.getProfile("default");
		    profile.setPreference("plugins.hide_infobar_for_missing_plugin", true);
		    dc = DesiredCapabilities.firefox();
		    dc.setCapability(FirefoxDriver.PROFILE, profile);
		    dc.setJavascriptEnabled(true);

		    wbdv2 = new RemoteWebDriver(remote_url, dc);
		    driver2 = new EventFiringWebDriver(wbdv2);
		    driver2.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		}

		else {

		    ProfilesIni allProfiles = new ProfilesIni();
		    FirefoxProfile profile = allProfiles.getProfile("default");
		    profile.setAcceptUntrustedCertificates(true);
		    profile.setAssumeUntrustedCertificateIssuer(false);
		    wbdv2 = new FirefoxDriver(profile);
		    driver2 = new EventFiringWebDriver(wbdv2);

		}

	    }

	    catch (Throwable initBrowserException) {

		APPLICATION_LOGS
			.debug("Error came while creating a browser instance : " + initBrowserException.getMessage());

		return "Fail : Error came while creating a browser instance : " + initBrowserException.getMessage();

	    }

	}

	APPLICATION_LOGS.debug("Created browser instance successfully");

	try {

	    // Implicitly wait for 30 seconds for browser to open
	    driver2.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

	    // Delete all browser cookies
	    driver2.manage().deleteAllCookies();

	    // Navigate to Curaspan application
	    driver2.navigate().to(CONFIG.getProperty("baseencode"));

	    // Maximize browser window
	    APPLICATION_LOGS.debug("Maximizing Browser window...");
	    driver2.manage().window().maximize();
	    APPLICATION_LOGS.debug("Browser window is maximized");

	    if (CONFIG.getProperty("test_browser").contains("Firefox")) {
		WebElement html = driver2.findElement(By.tagName("html"));
		html.sendKeys(Keys.chord(Keys.CONTROL, "0"));
	    }
	}

	catch (Throwable navigationError) {

	    APPLICATION_LOGS.debug("Error came while navigating to the test site : " + navigationError.getMessage());

	}

	// Verify Login page appears
	expectedTitle = BASE64;
	methodReturnResult = FunctionLibrary.driver2AssertTitle(expectedTitle);
	if (methodReturnResult.contains(failTest)) {

	    // Log result
	    APPLICATION_LOGS.debug("Not navigated to Login page");
	    return methodReturnResult;

	}

	APPLICATION_LOGS.debug("Navigated to Login page");

	APPLICATION_LOGS.debug("Logging in to Base64");


	
	// Clear Username input-box and input username
	methodReturnResult = FunctionLibrary.driver2ClearAndInput(locatorInputArea, "Input Area",
		MRN);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}


	// Click on Encode button
	methodReturnResult = FunctionLibrary.driver2ClickAndWait(locatorEncodeButton, "Clicking On Encode");
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	
	String MrnEncode=driver2.findElement(locatorOutput).getAttribute("value");
	
	// Clear Username input-box and input username
	methodReturnResult = FunctionLibrary.driver2ClearAndInput(locatorInputArea, "Input Area",
		Accno);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}


	// Click on Encode button
	methodReturnResult = FunctionLibrary.driver2ClickAndWait(locatorEncodeButton, "Clicking On Encode");
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	String AccEncode=driver2.findElement(locatorOutput).getAttribute("value");
	
	// Clear Username input-box and input username
	methodReturnResult = FunctionLibrary.driver2ClearAndInput(locatorInputArea, "Input Area",
		facilityID);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}


	// Click on Encode button
	methodReturnResult = FunctionLibrary.driver2ClickAndWait(locatorEncodeButton, "Clicking On Encode");
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	String facilityIDEncode=driver2.findElement(locatorOutput).getAttribute("value");
	
	String semiurl="mrn="+MrnEncode+"&account="+AccEncode+"&orgId="+facilityIDEncode;
	
	System.out.println(semiurl);
	
	driver2.close();
	
	return semiurl;

    }
    
    // Navigate to Encoder URL
    public static String generateExchangeKey(String facilityId, String param_name)
	    throws InterruptedException, BiffException, IOException {

	APPLICATION_LOGS.debug("Navigating to https://hash.online-convert.com/sha256-generator");


	// Disable log messages
	java.util.logging.Logger.getLogger("org.apache.http.impl.client").setLevel(java.util.logging.Level.WARNING);
	if (wbdv2 == null) {
	    try {

		if (CONFIG.getProperty("is_remote").equals("true")) {

		    // Generate Remote address
		    String remote_address = "http://" + CONFIG.getProperty("remote_ip") + ":4444/wd/hub";
		    remote_url = new URL(remote_address);

		    ProfilesIni allProfiles = new ProfilesIni();
		    FirefoxProfile profile = allProfiles.getProfile("default");
		    profile.setPreference("plugins.hide_infobar_for_missing_plugin", true);
		    dc = DesiredCapabilities.firefox();
		    dc.setCapability(FirefoxDriver.PROFILE, profile);
		    dc.setJavascriptEnabled(true);

		    wbdv2 = new RemoteWebDriver(remote_url, dc);
		    driver2 = new EventFiringWebDriver(wbdv2);
		    driver2.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		}

		else {

		    ProfilesIni allProfiles = new ProfilesIni();
		    FirefoxProfile profile = allProfiles.getProfile("default");
		    profile.setAcceptUntrustedCertificates(true);
		    profile.setAssumeUntrustedCertificateIssuer(false);
		    wbdv2 = new FirefoxDriver(profile);
		    driver2 = new EventFiringWebDriver(wbdv2);

		}

	    }

	    catch (Throwable initBrowserException) {

		APPLICATION_LOGS
			.debug("Error came while creating a browser instance : " + initBrowserException.getMessage());

		return "Fail : Error came while creating a browser instance : " + initBrowserException.getMessage();

	    }

	}

	APPLICATION_LOGS.debug("Created browser instance successfully");

	try {

	    // Implicitly wait for 30 seconds for browser to open
	    driver2.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

	    // Delete all browser cookies
	    driver2.manage().deleteAllCookies();

	    // Navigate to Curaspan application
	    driver2.navigate().to(CONFIG.getProperty("exchangeURl"));

	    // Maximize browser window
	    APPLICATION_LOGS.debug("Maximizing Browser window...");
	    driver2.manage().window().maximize();
	    APPLICATION_LOGS.debug("Browser window is maximized");

	    if (CONFIG.getProperty("test_browser").contains("Firefox")) {
		WebElement html = driver2.findElement(By.tagName("html"));
		html.sendKeys(Keys.chord(Keys.CONTROL, "0"));
	    }
	}

	catch (Throwable navigationError) {

	    APPLICATION_LOGS.debug("Error came while navigating to the test site : " + navigationError.getMessage());

	}

	// Verify Login page appears
	expectedTitle = Encoder;
	methodReturnResult = FunctionLibrary.driver2AssertTitle(expectedTitle);
	if (methodReturnResult.contains(failTest)) {

	    // Log result
	    APPLICATION_LOGS.debug("Not navigated to Login page");
	    return methodReturnResult;

	}

	APPLICATION_LOGS.debug("Navigated to Login page");

	APPLICATION_LOGS.debug("Logging in to sha256-generator");

	
	 DateFormat dateFormat = new SimpleDateFormat("MMddyyyy ");
	 
	 //get current date time with Date()
	 Date date = new Date();
	 
	 // Now format the date
	 String date1= dateFormat.format(date);
	 
	 // Print the Date
	 System.out.println(date1);
	
	String convert=facilityId+param_name+date1;
	
	
	
	
	

	System.out.println("Exchange Key "+convert);
	
	// Clear Username input-box and input username
	methodReturnResult = FunctionLibrary.driver2ClearAndInput(locatorConverterArea, "Input Area",
			convert);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}


	// Click on Encode button
	methodReturnResult = FunctionLibrary.driver2ClickAndWait(locatorConvert, "Clicking On Encode");
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}
  
	driver2.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
	
	
	String op=FunctionLibrary.retrieveText(locatorOutputConverterArea, "Extracting Values");
	
	String output=driver2.findElement(locatorOutputConverterArea).getAttribute("value");
	
	/*StringBuilder temp = new StringBuilder(output);
	for( int i = temp.length() - 1 ; i >= 0; i--){
	     if(temp.charAt(i) == ' '){
	          temp.deleteCharAt(i);
	     }else{
	          break;
	     }
	}

	String a = temp.toString();
	
	
	System.out.println("Exchange Key "+a);*/
	System.out.println("Exchange Key "+output);
	System.out.println("Exchange Key2 "+op);
	driver2.close();
	
	return output;

    }
    
   //Create URL
    public static String createURL(String exchangeKey,String encodedValue)
	    throws InterruptedException, BiffException, IOException {

	APPLICATION_LOGS.debug("Creating URL");


	// Disable log messages
	java.util.logging.Logger.getLogger("org.apache.http.impl.client").setLevel(java.util.logging.Level.WARNING);
	if (wbdv2 == null) {
	    try {

		if (CONFIG.getProperty("is_remote").equals("true")) {

		    // Generate Remote address
		    String remote_address = "http://" + CONFIG.getProperty("remote_ip") + ":4444/wd/hub";
		    remote_url = new URL(remote_address);

		    ProfilesIni allProfiles = new ProfilesIni();
		    FirefoxProfile profile = allProfiles.getProfile("default");
		    profile.setPreference("plugins.hide_infobar_for_missing_plugin", true);
		    dc = DesiredCapabilities.firefox();
		    dc.setCapability(FirefoxDriver.PROFILE, profile);
		    dc.setJavascriptEnabled(true);

		    wbdv2 = new RemoteWebDriver(remote_url, dc);
		    driver2 = new EventFiringWebDriver(wbdv2);
		    driver2.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		}

		else {

		    ProfilesIni allProfiles = new ProfilesIni();
		    FirefoxProfile profile = allProfiles.getProfile("default");
		    profile.setAcceptUntrustedCertificates(true);
		    profile.setAssumeUntrustedCertificateIssuer(false);
		    wbdv2 = new FirefoxDriver(profile);
		    driver2 = new EventFiringWebDriver(wbdv2);

		}

	    }

	    catch (Throwable initBrowserException) {

		APPLICATION_LOGS
			.debug("Error came while creating a browser instance : " + initBrowserException.getMessage());

		return "Fail : Error came while creating a browser instance : " + initBrowserException.getMessage();

	    }

	}

	//Creating url
	
	String url1=CONFIG.getProperty("APIURL");
	String URL=url1+exchangeKey+encodedValue;
	System.out.println(URL);
	
	APPLICATION_LOGS.debug("Created browser instance successfully");

	try {

	    // Implicitly wait for 30 seconds for browser to open
	    driver2.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

	    // Delete all browser cookies
	    driver2.manage().deleteAllCookies();

	    // Navigate to Curaspan application
	    driver2.navigate().to(URL);

	    // Maximize browser window
	    APPLICATION_LOGS.debug("Maximizing Browser window...");
	    driver2.manage().window().maximize();
	    APPLICATION_LOGS.debug("Browser window is maximized");

	    if (CONFIG.getProperty("test_browser").contains("Firefox")) {
		WebElement html = driver2.findElement(By.tagName("html"));
		html.sendKeys(Keys.chord(Keys.CONTROL, "0"));
	    }
	}

	catch (Throwable navigationError) {

	    APPLICATION_LOGS.debug("Error came while navigating to the test site : " + navigationError.getMessage());

	}

	/*// Verify Login page appears
	expectedTitle = BASE64;
	methodReturnResult = FunctionLibrary.driver2AssertTitle(expectedTitle);
	if (methodReturnResult.contains(failTest)) {

	    // Log result
	    APPLICATION_LOGS.debug("Not navigated to Login page");
	    return methodReturnResult;

	}*/

	APPLICATION_LOGS.debug("Navigated to the Response Page");

	APPLICATION_LOGS.debug("Extracting Text");

	String op=FunctionLibrary.driver2RetrieveText(By.xpath("/html/body/pre"), "Value");
	System.out.println(op);

	op=op.replaceAll("[^0-9]", "");
	System.out.println(op);	
	
	return "Response";
	
    }
    // Navigate to Discharge page
    public static String selectHospitalInAcceptanceTab(String referralHospitalName)
	    throws InterruptedException, BiffException, IOException {

	APPLICATION_LOGS.debug("Navigating to Acceptance tab and select a hospital from the dropdown ...");

	// Click on 'Discharge' tab and wait for page to load
	methodReturnResult = FunctionLibrary.clickAndWait(locatorAcceptanceTab, nameAcceptanceTab);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Wait for the result to appear
	if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
	    FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
	}

	// Verify we navigated to Contract page
	expectedTitle = AcceptanceWorkbookPageTitle;
	methodReturnResult = FunctionLibrary.assertTitle(expectedTitle);
	if (methodReturnResult.contains(failTest)) {

	    APPLICATION_LOGS.debug("Re-trying to go to the Acceptance page ...");

	    // Click on 'Acceptance' tab and wait for page to load
	    methodReturnResult = FunctionLibrary.clickAndWait(locatorAcceptanceTab, nameAcceptanceTab);
	    if (methodReturnResult.contains(failTest)) {
		return methodReturnResult;
	    }

	    // Wait for the result to appear
	    if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
		FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
	    }
	}

	methodReturnResult = FunctionLibrary.assertTitle(expectedTitle);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Select the hospital dropdown
	methodReturnResult = FunctionLibrary.clickLink(locatorHospitalDropdown, nameHospitalDropdown);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Select the hospital from the dropdown
	methodReturnResult = FunctionLibrary.clickLink(By.xpath("//a[contains(text(),'" + referralHospitalName + "')]"),
		"Hospital dropdown");
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Wait for the result to appear
	if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
	    FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
	}

	// Wait for the Loading text to disappear
	if (FunctionLibrary.isElementDisplayed(locatorLoadingText, nameLoadingText)) {
	    FunctionLibrary.waitForElementToDisappear(locatorLoadingText, nameLoadingText);
	}

	return "Pass : Navigated to Acceptance tab and selected hospital from the dropdown";

    }
    
    // Navigate to Discharge page
    public static String PMLRequest(String referralHospitalName, String levelOfCare, String state,
	    String county, String city, String zip) throws InterruptedException, BiffException, IOException {

	APPLICATION_LOGS.debug("Navigating to Acceptance tab and select a hospital from the dropdown ...");
	// select level dropdown
	methodReturnResult = FunctionLibrary.selectValueByVisibleText(locatorLevelDropdown, levelOfCare,
		nameLevelDropdown);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Wait for the result to appear
	if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
	    FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
	}

	// select state dropdown
	methodReturnResult = FunctionLibrary.selectValueByVisibleText(locatorStateDropdown, state, nameStateDropdown);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Wait for the result to appear
	if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
	    FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
	}

	// select city dropdown
	methodReturnResult = FunctionLibrary.selectValueByVisibleText(locatorCityDropdown, "All", nameCityDropdown);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Wait for the result to appear
	if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
	    FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
	}

	// select county dropdown
	methodReturnResult = FunctionLibrary.selectValueByVisibleText(locatorCountyDropdown, county,
		nameCountyDropdown);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Wait for the result to appear
	if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
	    FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
	}

	// select city dropdown
	methodReturnResult = FunctionLibrary.selectValueByVisibleText(locatorCityDropdown, city, nameCityDropdown);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Wait for the result to appear
	if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
	    FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
	}

	// select zip dropdown
	methodReturnResult = FunctionLibrary.selectValueByVisibleText(locatorZipDropdown, zip, nameZipDropdown);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Wait for the result to appear
	if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
	    FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
	}

	// Click on search
	methodReturnResult = FunctionLibrary.clickLink(locatorSearchBtn, nameSearchBtn);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Wait for the result to appear
	if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
	    FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
	}

	// Check checkbox which contains referral automation hospital
	methodReturnResult = FunctionLibrary.checkCheckBox(
		By.xpath("//div[contains(text(),'" + referralHospitalName + "')]/../../../../../td[1]/div/input"),
		"Connected Facility");
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Click On PML
	methodReturnResult = FunctionLibrary.clickLink(locatorPMLRequest, "PML request");
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}
	// Enter Mail ID
		methodReturnResult = FunctionLibrary.clearAndInput(locatorMaillink, "Providing Mail", "testgoing@gmail.com");
		if (methodReturnResult.contains(failTest)) {
		    return methodReturnResult;
		}
		// Click On Send
		methodReturnResult = FunctionLibrary.clickLink(locatorSendMail, "Sending PML request");
		if (methodReturnResult.contains(failTest)) {
		    return methodReturnResult;
		}

	// Wait for the result to appear
	if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
	    FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
	}

	return "Pass : Sent a PML from matching page";
    }

    // Navigate to Discharge page
    public static String sendBookingRequest(String referralHospitalName, String levelOfCare, String state,
	    String county, String city, String zip) throws InterruptedException, BiffException, IOException {

	APPLICATION_LOGS.debug("Navigating to Acceptance tab and select a hospital from the dropdown ...");
	// select level dropdown
	methodReturnResult = FunctionLibrary.selectValueByVisibleText(locatorLevelDropdown, levelOfCare,
		nameLevelDropdown);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Wait for the result to appear
	if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
	    FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
	}

	// select state dropdown
	methodReturnResult = FunctionLibrary.selectValueByVisibleText(locatorStateDropdown, state, nameStateDropdown);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Wait for the result to appear
	if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
	    FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
	}

	// select city dropdown
	methodReturnResult = FunctionLibrary.selectValueByVisibleText(locatorCityDropdown, "All", nameCityDropdown);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Wait for the result to appear
	if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
	    FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
	}

	// select county dropdown
	methodReturnResult = FunctionLibrary.selectValueByVisibleText(locatorCountyDropdown, county,
		nameCountyDropdown);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Wait for the result to appear
	if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
	    FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
	}

	// select city dropdown
	methodReturnResult = FunctionLibrary.selectValueByVisibleText(locatorCityDropdown, city, nameCityDropdown);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Wait for the result to appear
	if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
	    FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
	}

	// select zip dropdown
	methodReturnResult = FunctionLibrary.selectValueByVisibleText(locatorZipDropdown, zip, nameZipDropdown);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Wait for the result to appear
	if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
	    FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
	}

	// Click on search
	methodReturnResult = FunctionLibrary.clickLink(locatorSearchBtn, nameSearchBtn);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Wait for the result to appear
	if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
	    FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
	}

	// Check checkbox which contains referral automation hospital
	methodReturnResult = FunctionLibrary.checkCheckBox(
		By.xpath("//div[contains(text(),'" + referralHospitalName + "')]/../../../../../td[1]/div/input"),
		"Connected Facility");
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// send booking request
	methodReturnResult = FunctionLibrary.clickLink(locatorSendBookingRequest, nameSendBookingRequest);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Wait for the result to appear
	if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
	    FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
	}

	return "Pass : Sent a booking request from matching page";
    }

    // Navigate to Discharge page
    public static String sendBookingRequestBasedOnLOC(String POS, String levelOfCare, String state, String county,
	    String city, String providerName) throws InterruptedException, BiffException, IOException {

	APPLICATION_LOGS.debug("Send a booking request based on LOC ...");

	// Select the POS
	methodReturnResult = FunctionLibrary.selectValueByVisibleText(locatorPOSDropdown, POS, namePOSDropdown);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Wait for the result to appear
	if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
	    FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
	}

	// select level dropdown
	methodReturnResult = FunctionLibrary.selectValueByVisibleText(locatorLevelDropdown, levelOfCare,
		nameLevelDropdown);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Wait for the result to appear
	if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
	    FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
	}

	// select state dropdown
	methodReturnResult = FunctionLibrary.selectValueByVisibleText(locatorStateDropdown, state, nameStateDropdown);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Wait for the result to appear
	if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
	    FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
	}

	// select city dropdown
	methodReturnResult = FunctionLibrary.selectValueByVisibleText(locatorCityDropdown, "All", nameCityDropdown);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Wait for the result to appear
	if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
	    FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
	}

	// select county dropdown
	methodReturnResult = FunctionLibrary.selectValueByVisibleText(locatorCountyDropdown, county,
		nameCountyDropdown);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Wait for the result to appear
	if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
	    FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
	}

	// select city dropdown
	methodReturnResult = FunctionLibrary.selectValueByVisibleText(locatorCityDropdown, city, nameCityDropdown);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Wait for the result to appear
	if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
	    FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
	}

	// Click on search
	methodReturnResult = FunctionLibrary.clickLink(locatorSearchBtn, nameSearchBtn);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Wait for the result to appear
	if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
	    FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
	}

	// Check checkbox which contains referral automation hospital
	methodReturnResult = FunctionLibrary.checkCheckBox(
		By.xpath("//div[contains(text(),'" + providerName + "')]/../../../../../td[1]/div/input"),
		"Connected Facility");
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// send booking request
	methodReturnResult = FunctionLibrary.clickLink(locatorSendBookingRequest, nameSendBookingRequest);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Wait for the result to appear
	if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
	    FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
	}

	return "Pass : Sent a booking request from matching page";
    }

    // Navigate to Discharge page
    public static String sendBookingRequestBasedOnLOCAndProvider(String POS, String levelOfCare, String state,
	    String county, String city, String providerName) throws InterruptedException, BiffException, IOException {

	APPLICATION_LOGS.debug("Send a booking request based on LOC ...");

	// Select the POS
	methodReturnResult = FunctionLibrary.selectValueByVisibleText(locatorPOSDropdown, POS, namePOSDropdown);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Wait for the result to appear
	if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
	    FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
	}

	// select level dropdown
	methodReturnResult = FunctionLibrary.selectValueByVisibleText(locatorLevelDropdown, levelOfCare,
		nameLevelDropdown);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Wait for the result to appear
	if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
	    FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
	}

	// select state dropdown
	methodReturnResult = FunctionLibrary.selectValueByVisibleText(locatorStateDropdown, state, nameStateDropdown);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Wait for the result to appear
	if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
	    FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
	}

	// select city dropdown
	methodReturnResult = FunctionLibrary.selectValueByVisibleText(locatorCityDropdown, "All", nameCityDropdown);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Wait for the result to appear
	if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
	    FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
	}

	// select county dropdown
	methodReturnResult = FunctionLibrary.selectValueByVisibleText(locatorCountyDropdown, county,
		nameCountyDropdown);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Wait for the result to appear
	if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
	    FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
	}

	// select city dropdown
	methodReturnResult = FunctionLibrary.selectValueByVisibleText(locatorCityDropdown, city, nameCityDropdown);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Wait for the result to appear
	if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
	    FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
	}

	if (FunctionLibrary.isElementDisplayed(By.xpath("//*[@id='providerName']"), "Provider Name Field")) {
	    FunctionLibrary.clearAndInput(By.xpath("//*[@id='providerName']"), "Provider Name Field", providerName);
	}

	// Click on search
	methodReturnResult = FunctionLibrary.clickLink(locatorSearchBtn, nameSearchBtn);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Wait for the result to appear
	if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
	    FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
	}

	// Check checkbox which contains referral automation hospital
	methodReturnResult = FunctionLibrary.checkCheckBox(
		By.xpath("//div[contains(text(),'" + providerName + "')]/../../../../../td[1]/div/input"),
		"Connected Facility");
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// send booking request
	methodReturnResult = FunctionLibrary.clickLink(locatorSendBookingRequest, nameSendBookingRequest);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Wait for the result to appear
	if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
	    FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
	}

	return "Pass : Sent a booking request from matching page";
    }

    // Create a browser instance and navigate to the Discharge Central
    public static String createANewTabAndNavigateAndLoginToIntake(String PatientLastName, String Message, String Login,
	    String Password) throws MalformedURLException, InterruptedException {

	driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL + "t");

	// Navigate to Curaspan application
	driver.navigate().to(CONFIG.getProperty("IntakeTestSiteURL"));

	// Handle certificate error
	if (CONFIG.getProperty("test_browser").contains("Internet Explorer")) {

	    if (driver.getTitle().contains(navigationBlockedTitle)) {

		driver.navigate().to("javaScript:document.getElementById('overridelink').click()");

		FunctionLibrary.waitForPageToLoad();
	    }
	}

	if (FunctionLibrary.isElementDisplayed(locatorIntakeUsernameField, "Username Field")) {
	    methodReturnResult = FunctionLibrary.clearAndInput(locatorIntakeUsernameField, "Username Field", Login);
	    if (methodReturnResult.contains(failTest)) {
		return methodReturnResult;
	    }
	}

	if (FunctionLibrary.isElementDisplayed(locatorIntakePasswordField, "password Field")) {
	    methodReturnResult = FunctionLibrary.clearAndInput(locatorIntakePasswordField, "password Field", Password);
	    if (methodReturnResult.contains(failTest)) {
		return methodReturnResult;
	    }
	}

	if (FunctionLibrary.isElementDisplayed(locatorIntakeLoginButton, "Login Button")) {
	    methodReturnResult = FunctionLibrary.clickAndWait(locatorIntakeLoginButton, "Login Button");
	    if (methodReturnResult.contains(failTest)) {
		return methodReturnResult;
	    }
	}

	FunctionLibrary.waitForPageToLoad();

	FunctionLibrary.waitForElementToLoad(locatorIntakeEmailVerificationModal);

	if (FunctionLibrary.isElementDisplayed(locatorIntakeEmailVerificationModal, "Email verification Modal")) {
	    methodReturnResult = FunctionLibrary.clickAndWait(locatorIntakeEmailVerificationVerifyButton,
		    "VERIFY Button");
	    if (methodReturnResult.contains(failTest)) {
		return methodReturnResult;
	    }
	}

	FunctionLibrary.waitForPageToLoad();

	FunctionLibrary.waitForElementToLoad(locatorIntakeEmailVerificationCodeModal);

	if (FunctionLibrary.isElementDisplayed(locatorIntakeEmailVerificationCodeModal,
		"Email Verification Code Modal")) {
	    methodReturnResult = FunctionLibrary.clickAndWait(locatorIntakeEmailVerificationCancelButton,
		    "CANCEl Button");
	    if (methodReturnResult.contains(failTest)) {
		return methodReturnResult;
	    }
	}

	Thread.sleep(2000);

	FunctionLibrary.waitForElementToLoad(locatornHIntakeButton);

	Thread.sleep(2000);

	FunctionLibrary.waitForPageToLoad();
	methodReturnResult = FunctionLibrary.clickAndWait(locatornHIntakeButton, "Intake Button");
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	FunctionLibrary.waitForPageToLoad();

	Thread.sleep(1000);

	FunctionLibrary.waitForElementToLoad(locatorIntakeSearchPatientButton);

	Thread.sleep(3000);

	methodReturnResult = FunctionLibrary.clickAndWait(locatorIntakeSearchPatientButton, "Search Patient Button");
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	FunctionLibrary.waitForElementToLoad(locatorIntakeSearchPatientField);

	Thread.sleep(2000);

	// Search patient and navigate to select 'Received and Refer' status
	if (FunctionLibrary.isElementDisplayed(locatorIntakeSearchPatientField, "Search Patient Modal")) {
	    Thread.sleep(2000);
	    methodReturnResult = FunctionLibrary.clearAndInput(locatorIntakeSearchPatientField, "Patient Name field",
		    PatientLastName);

	    System.out.println(PatientLastName);
	    if (methodReturnResult.contains(failTest)) {
		return methodReturnResult;
	    }

	    methodReturnResult = FunctionLibrary.clickAndWait(locatorIntakeSearchPatientButton2,
		    "Search Patient Button");
	    if (methodReturnResult.contains(failTest)) {
		return methodReturnResult;
	    }

	    Thread.sleep(3000);

	    methodReturnResult = FunctionLibrary
		    .clickAndWait(By.xpath("//*[contains(text(),'" + PatientLastName + "')]"), "Patient Name");
	    if (methodReturnResult.contains(failTest)) {
		return methodReturnResult;
	    }

	} else {
	    return "Fail: No Seacrh Patient displayed";
	}

	FunctionLibrary.waitForPageToLoad();

	FunctionLibrary.waitForElementToLoad(locatorIntakeRespondButton);

	Thread.sleep(2000);

	methodReturnResult = FunctionLibrary.clickAndWait(locatorIntakeRespondButton, "Respond Button");
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	FunctionLibrary.waitForElementToLoad(locatorIntakeSelectAStatusDropdown);

	if (FunctionLibrary.isElementDisplayed(locatorIntakeSelectAStatusDropdown, "Select A Status Dropdown")) {
	    FunctionLibrary.selectValueByVisibleText(locatorIntakeSelectAStatusDropdown, StatusReceivedAndRefer,
		    "Accept");
	    FunctionLibrary.clickLink(By.xpath(
		    "//label[span[@class='circle'] and text()[normalize-space()='Skilled Nursing Facility (SNF)']]"),
		    "SNF");
	    FunctionLibrary.clickLink(
		    By.xpath("//label[span[@class='circle'] and text()[normalize-space()='Medicare']]"), "Bed Type");

	    methodReturnResult = FunctionLibrary.clickAndWait(locatorIntakeSendResponse, "Send Response Button");
	    if (methodReturnResult.contains(failTest)) {
		return methodReturnResult;
	    }

	} else {
	    return "Fail: ";
	}

	Thread.sleep(1000);

	FunctionLibrary.refreshPage();

	FunctionLibrary.waitForElementToLoad(By.xpath("//*[@ng-click='providersModel.openMessageModal(provider)']"));

	Thread.sleep(4000);

	FunctionLibrary.clickAndWait(By.xpath("//*[@ng-click='providersModel.openMessageModal(provider)']"),
		"Messages");

	Thread.sleep(2000);

	FunctionLibrary.waitForElementToLoad(By.xpath("//*[@id='patient-detail-messages-modal']"));

	FunctionLibrary.clearAndInput(By.xpath("//textarea[@placeholder='Write a message']"), "Message Text Field",
		Message);

	FunctionLibrary.clickAndWait(By.xpath("//*[@id='send-button']"), "Send Button");

	FunctionLibrary.waitForPageToLoad();

	FunctionLibrary.clickAndWait(By.xpath("//button[contains(text(),'Close')]"), "Close Button");

	FunctionLibrary.refreshPage();

	FunctionLibrary.waitForPageToLoad();

	return "Booked Referrals";
    }

    // Create a browser instance and navigate to the Discharge Central
    public static String createANewTabAndNavigateToIntake(String PatientLastName, String Message, String Login,
	    String Password) throws MalformedURLException, InterruptedException {

	driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL + "t");

	// Navigate to Curaspan application
	driver.navigate().to(CONFIG.getProperty("IntakeTestSiteURL"));

	// Handle certificate error
	if (CONFIG.getProperty("test_browser").contains("Internet Explorer")) {

	    if (driver.getTitle().contains(navigationBlockedTitle)) {

		driver.navigate().to("javaScript:document.getElementById('overridelink').click()");

		FunctionLibrary.waitForPageToLoad();
	    }
	}

	FunctionLibrary.waitForPageToLoad();

	FunctionLibrary.waitForElementToLoad(locatorIntakeEmailVerificationModal);

	if (FunctionLibrary.isElementDisplayed(locatorIntakeEmailVerificationModal, "Email verification Modal")) {
	    methodReturnResult = FunctionLibrary.clickAndWait(locatorIntakeEmailVerificationVerifyButton,
		    "VERIFY Button");
	    if (methodReturnResult.contains(failTest)) {
		return methodReturnResult;
	    }
	}

	FunctionLibrary.waitForPageToLoad();

	FunctionLibrary.waitForElementToLoad(locatorIntakeEmailVerificationCodeModal);

	if (FunctionLibrary.isElementDisplayed(locatorIntakeEmailVerificationCodeModal,
		"Email Verification Code Modal")) {
	    methodReturnResult = FunctionLibrary.clickAndWait(locatorIntakeEmailVerificationCancelButton,
		    "CANCEl Button");
	    if (methodReturnResult.contains(failTest)) {
		return methodReturnResult;
	    }
	}

	Thread.sleep(2000);

	FunctionLibrary.waitForElementToLoad(locatornHIntakeButton);

	Thread.sleep(2000);

	FunctionLibrary.waitForPageToLoad();
	methodReturnResult = FunctionLibrary.clickAndWait(locatornHIntakeButton, "Intake Button");
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	FunctionLibrary.waitForPageToLoad();

	Thread.sleep(1000);

	FunctionLibrary.waitForElementToLoad(locatorIntakeSearchPatientButton);

	Thread.sleep(3000);

	methodReturnResult = FunctionLibrary.clickAndWait(locatorIntakeSearchPatientButton, "Search Patient Button");
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	FunctionLibrary.waitForElementToLoad(locatorIntakeSearchPatientField);

	Thread.sleep(2000);

	// Search patient and navigate to select 'Received and Refer' status
	if (FunctionLibrary.isElementDisplayed(locatorIntakeSearchPatientField, "Search Patient Modal")) {
	    Thread.sleep(2000);
	    methodReturnResult = FunctionLibrary.clearAndInput(locatorIntakeSearchPatientField, "Patient Name field",
		    PatientLastName);

	    System.out.println(PatientLastName);
	    if (methodReturnResult.contains(failTest)) {
		return methodReturnResult;
	    }

	    methodReturnResult = FunctionLibrary.clickAndWait(locatorIntakeSearchPatientButton2,
		    "Search Patient Button");
	    if (methodReturnResult.contains(failTest)) {
		return methodReturnResult;
	    }

	    Thread.sleep(3000);

	    methodReturnResult = FunctionLibrary
		    .clickAndWait(By.xpath("//*[contains(text(),'" + PatientLastName + "')]"), "Patient Name");
	    if (methodReturnResult.contains(failTest)) {
		return methodReturnResult;
	    }

	} else {
	    return "Fail: No Seacrh Patient displayed";
	}

	FunctionLibrary.waitForPageToLoad();

	FunctionLibrary.waitForElementToLoad(locatorIntakeRespondButton);

	Thread.sleep(2000);

	methodReturnResult = FunctionLibrary.clickAndWait(locatorIntakeRespondButton, "Respond Button");
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	FunctionLibrary.waitForElementToLoad(locatorIntakeSelectAStatusDropdown);

	if (FunctionLibrary.isElementDisplayed(locatorIntakeSelectAStatusDropdown, "Select A Status Dropdown")) {
	    FunctionLibrary.selectValueByVisibleText(locatorIntakeSelectAStatusDropdown, StatusReceivedAndRefer,
		    "Accept");
	    FunctionLibrary.clickLink(By.xpath(
		    "//label[span[@class='circle'] and text()[normalize-space()='Skilled Nursing Facility (SNF)']]"),
		    "SNF");
	    FunctionLibrary.clickLink(
		    By.xpath("//label[span[@class='circle'] and text()[normalize-space()='Medicare']]"), "Bed Type");

	    methodReturnResult = FunctionLibrary.clickAndWait(locatorIntakeSendResponse, "Send Response Button");
	    if (methodReturnResult.contains(failTest)) {
		return methodReturnResult;
	    }

	} else {
	    return "Fail: ";
	}

	Thread.sleep(1000);

	FunctionLibrary.refreshPage();

	FunctionLibrary.waitForElementToLoad(By.xpath("//*[@ng-click='providersModel.openMessageModal(provider)']"));

	Thread.sleep(4000);

	FunctionLibrary.clickAndWait(By.xpath("//*[@ng-click='providersModel.openMessageModal(provider)']"),
		"Messages");

	Thread.sleep(2000);

	FunctionLibrary.waitForElementToLoad(By.xpath("//*[@id='patient-detail-messages-modal']"));

	FunctionLibrary.clearAndInput(By.xpath("//textarea[@placeholder='Write a message']"), "Message Text Field",
		Message);

	FunctionLibrary.clickAndWait(By.xpath("//*[@id='send-button']"), "Send Button");

	FunctionLibrary.waitForPageToLoad();

	FunctionLibrary.clickAndWait(By.xpath("//button[contains(text(),'Close')]"), "Close Button");

	FunctionLibrary.refreshPage();

	FunctionLibrary.waitForPageToLoad();

	return "Booked Referrals";
    }

    // Navigate to Patient Details page
    public static String navigateToPatientDetailsPage(By locatorReferralLink, String nameReferralLink)
	    throws InterruptedException, BiffException, IOException {

	APPLICATION_LOGS.debug("Navigating to Patient Details page ...");

	// Click on assessment link
	methodReturnResult = FunctionLibrary.clickAndWait(locatorReferralLink, nameReferralLink);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Verify if the page is navigated to Assessment page
	if (!FunctionLibrary.isElementPresent(locatorPrintLink, namePrintLink)) {
	    return failTest + "Print link is not loaded, Page is not navigated to Patient Details page";
	}

	// Check if the loading text present
	if (FunctionLibrary.isElementDisplayed(locatorLoadingText, nameLoadingText)) {
	    FunctionLibrary.waitForElementToDisappear(locatorLoadingText, nameLoadingText);
	}

	return "Pass : Navigated to Patient Details Page";

    }

    // Navigate to Acceptance workbook page
    public static String navigateToAcceptancePage() throws InterruptedException, BiffException, IOException {

	APPLICATION_LOGS.debug("Navigating to Acceptance workbook page ...");

	// Click on Acceptance tab
	methodReturnResult = FunctionLibrary.clickAndWait(locatorAcceptanceTab, nameAcceptanceTab);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Wait for the result to appear
	if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
	    FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
	}

	// Verify the page is navigated to Acceptance
	if (!FunctionLibrary.isElementDisplayed(locatorDashboardTable, nameDashboardTable)) {
	    return failTest
		    + " : Dashboard table is not loaded in the page. Page is not navigated to Referral workbook";
	}

	return "Pass : Navigated to Acceptance Workbook Page";

    }

    // Navigate to Acceptance workbook page
    public static String attachFormInAssessmentPage() throws InterruptedException, BiffException, IOException {

	APPLICATION_LOGS.debug("Attach a form in assessment page ...");

	// Get the size of form checkbox
	int totalChkBox = driver.findElements(locatorFormCheckbox).size();

	int rowToSearch = 0;
	for (int i = 1; i <= totalChkBox; i++) {
	    if (!FunctionLibrary.isChecked(
		    By.xpath("//tbody[@class='yui-dt-data']/tr[" + i + "]/td/div/input[@type='checkbox']"),
		    "Form check box")) {
		rowToSearch = i;
		break;
	    }
	}

	methodReturnResult = FunctionLibrary.checkCheckBox(
		(By.xpath("//tbody[@class='yui-dt-data']/tr[" + rowToSearch + "]/td/div/input[@type='checkbox']")),
		nameFirstFormCheckbox);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Click on save patient record button
	methodReturnResult = FunctionLibrary.clickLink(locatorSavePatientRecordBtn, nameSavePatientRecordBtn);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Check whether the confirmation dialogue is present or not
	if (FunctionLibrary.isElementDisplayed(locatorConfirmBtn, nameConfirmBtn)) {
	    methodReturnResult = FunctionLibrary.clickLink(locatorConfirmBtn, nameConfirmBtn);
	    if (methodReturnResult.contains(failTest)) {
		return methodReturnResult;
	    }
	}

	// Wait for the result to appear
	if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
	    FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
	}

	return "Pass : Attached form in Assessment page";

    }

    // Navigate to Referral central
    public static String referralCentralLogin(String userName, String password, String referralHospitalName)
	    throws MalformedURLException, InterruptedException {

	APPLICATION_LOGS.debug("Login to referral Central ...");

	// Verify Login page appears
	expectedTitle = dischargeCentralLoginPageTitle;
	methodReturnResult = FunctionLibrary.assertTitle(expectedTitle);
	if (methodReturnResult.contains(failTest)) {

	    // Log result
	    APPLICATION_LOGS.debug("Not navigated to Login page");
	    return methodReturnResult;

	}

	APPLICATION_LOGS.debug("Navigated to Login page");

	APPLICATION_LOGS.debug("Logging in to Referral Central ...");

	try {

	    APPLICATION_LOGS.debug("Successfully Retrieved data from Xls File :-  Username : " + userName
		    + " and Password : " + password);

	}

	catch (Throwable fetchExcelDataError) {

	    APPLICATION_LOGS.debug("Error while retrieving data from xls file" + fetchExcelDataError.getMessage());
	    return "Error while retrieving data from xls file" + fetchExcelDataError.getMessage();

	}

	// Clear Username input-box and input username
	methodReturnResult = FunctionLibrary.clearAndInput(locatorDCUserNameInputBox, nameDCUserNameInputBox, userName);

	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Clear Password input-box and input password
	methodReturnResult = FunctionLibrary.clearAndInput(locatorDCPasswordInputBox, nameDCPasswordInputBox, password);

	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Click on Sign In button
	methodReturnResult = FunctionLibrary.clickAndWait(locatorDCSignInButton, nameDCSignInButton);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Verify login was successful
	expectedTitle = dischargeCentralLoginPageTitle;
	String actualTitle = null;

	for (int i = 0; i < 3; i++) {

	    // Get page title
	    actualTitle = FunctionLibrary.getPageTitle();

	    if (actualTitle.equals(expectedTitle)) {

		// Log result
		APPLICATION_LOGS.debug("Failed to login. Retrying ...");

		// Clear Username input-box and input username
		FunctionLibrary.clearAndInput(locatorDCUserNameInputBox, nameUserNameInputBox, userName);

		// Clear Password input-box and input password
		FunctionLibrary.clearAndInput(locatorDCPasswordInputBox, namePasswordInputBox, password);

		// Click on Sign In button
		FunctionLibrary.clickAndWait(locatorDCSignInButton, nameDCSignInButton);

	    } else {
		break;
	    }

	}

	// Click on Continue button if present
	if (FunctionLibrary.isElementDisplayed(locatorContinueBtn, nameContinueBtn)) {
	    methodReturnResult = FunctionLibrary.clickLink(locatorContinueBtn, nameContinueBtn);
	    if (methodReturnResult.contains(failTest)) {
		return methodReturnResult;
	    }

	}
	FunctionLibrary.waitForElementToLoad(locatorDischargeTab);

	// Click on Remind later button
	if (FunctionLibrary.isElementDisplayed(By.id("profileUpdateReminderCancel"), "Profile update Reminder")) {
	    FunctionLibrary.clickLink(By.id("profileUpdateReminderCancel"), "Remind Later Btn");
	}

	APPLICATION_LOGS.debug("Logged into Discharge Central");

	APPLICATION_LOGS.debug("Navigating to Referral page ...");

	// Click on 'Acceptance' tab and wait for page to load
	methodReturnResult = FunctionLibrary.clickAndWait(locatorAcceptanceTab, nameAcceptanceTab);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Wait for the result to appear
	if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
	    FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
	}

	// Verify we navigated to Contract page
	expectedTitle = AcceptanceWorkbookPageTitle;
	methodReturnResult = FunctionLibrary.assertTitle(expectedTitle);
	if (methodReturnResult.contains(failTest)) {

	    APPLICATION_LOGS.debug("Re-trying to go to the Acceptance page ...");

	    methodReturnResult = FunctionLibrary.clickAndWait(locatorAcceptanceTab, nameAcceptanceTab);
	    if (methodReturnResult.contains(failTest)) {
		return methodReturnResult;
	    }

	    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

	    // Wait for the result to appear
	    if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
		FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
	    }
	}

	methodReturnResult = FunctionLibrary.assertTitle(expectedTitle);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Select the hospital dropdown
	methodReturnResult = FunctionLibrary.clickLink(locatorHospitalDropdown, nameHospitalDropdown);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Select the hospital from the dropdown
	methodReturnResult = FunctionLibrary.clickLink(By.xpath("//a[contains(text(),'" + referralHospitalName + "')]"),
		"Hospital dropdown");
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Wait for the result to appear
	if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
	    FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
	}

	// Wait for the Loading text to disappear
	if (FunctionLibrary.isElementDisplayed(locatorLoadingText, nameLoadingText)) {
	    FunctionLibrary.waitForElementToDisappear(locatorLoadingText, nameLoadingText);
	}

	APPLICATION_LOGS.debug("Navigated to Acceptance tab");

	return "Pass : Navigated to Login page, Logged into Discharge Central and Navigated to Acceptance tab and selected hospital from dropdown";

    }

    // Navigate to Discharge page
    public static String navigateToProfilePage() throws InterruptedException, BiffException, IOException {

	APPLICATION_LOGS.debug("Navigating to Profile page ...");

	// Click on Profile tab
	methodReturnResult = FunctionLibrary.clickAndWait(locatorProfileTab, nameProfileTab);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Check if the page is navigated to profile page
	expectedTitle = ProfilePageTitle;
	methodReturnResult = FunctionLibrary.assertTitle(expectedTitle);
	if (methodReturnResult.contains(failTest)) {

	    APPLICATION_LOGS.debug("Re-trying to go to the Profile page ...");

	    // Click on Profile tab
	    methodReturnResult = FunctionLibrary.clickAndWait(locatorProfileTab, nameProfileTab);
	    if (methodReturnResult.contains(failTest)) {
		return methodReturnResult;
	    }

	    methodReturnResult = FunctionLibrary.assertTitle(expectedTitle);
	    if (methodReturnResult.contains(failTest)) {
		return methodReturnResult;
	    }
	}
	return "Pass : Navigated to Profile Page";

    }

    // Create a browser instance and navigate to the Profile page
    public static String selectHospitalInProfilePage(int Data_Row_No, String referralHospitalName)
	    throws InterruptedException, BiffException, IOException {

	APPLICATION_LOGS.debug("Creating a browser instance and navigating to the test site ...");

	// Disable log messages
	java.util.logging.Logger.getLogger("org.apache.http.impl.client").setLevel(java.util.logging.Level.WARNING);

	if (wbdv == null) {

	    try {

		if (CONFIG.getProperty("is_remote").equals("true")) {

		    // Generate Remote address
		    String remote_address = "http://" + CONFIG.getProperty("remote_ip") + ":4444/wd/hub";
		    remote_url = new URL(remote_address);

		    if (CONFIG.getProperty("test_browser").contains("Internet Explorer")) {

			dc = DesiredCapabilities.internetExplorer();
			dc.setCapability("silent", true);

		    }

		    else {

			ProfilesIni allProfiles = new ProfilesIni();
			// FirefoxProfile profile =
			// allProfiles.getProfile("default");
			// profile.setPreference("plugins.hide_infobar_for_missing_plugin",
			// true);
			dc = DesiredCapabilities.firefox();
			// dc.setCapability(FirefoxDriver.PROFILE, profile);
			// dc.setJavascriptEnabled(true);

		    }

		    wbdv = new RemoteWebDriver(remote_url, dc);
		    driver = new EventFiringWebDriver(wbdv);
		    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		}

		else {

		    if (CONFIG.getProperty("test_browser").toLowerCase().contains("internet explorer")
			    || CONFIG.getProperty("test_browser").toLowerCase().contains("ie")) {

			System.setProperty("webdriver.ie.driver", "C:\\Users\\IEDriver\\IEDriverServer.exe");
			dc = DesiredCapabilities.internetExplorer();
			dc.setCapability("silent", true);
			wbdv = new InternetExplorerDriver(dc);
			driver = new EventFiringWebDriver(wbdv);

		    }

		    else if (CONFIG.getProperty("test_browser").toLowerCase().contains("firefox")
			    || CONFIG.getProperty("test_browser").toLowerCase().contains("ff")) {

			ProfilesIni allProfiles = new ProfilesIni();
			FirefoxProfile profile = allProfiles.getProfile("default");
			profile.setAcceptUntrustedCertificates(true);
			profile.setAssumeUntrustedCertificateIssuer(false);
			wbdv = new FirefoxDriver(profile);
			driver = new EventFiringWebDriver(wbdv);

		    }

		    else if (CONFIG.getProperty("test_browser").toLowerCase().contains("safari")) {

			dc = DesiredCapabilities.safari();

		    }

		    else if (CONFIG.getProperty("test_browser").toLowerCase().contains("chrome")) {

			System.setProperty("webdriver.chrome.driver",
				System.getProperty("user.dir") + "/chromedriver.exe");
			dc = DesiredCapabilities.chrome();
			wbdv = new ChromeDriver(dc);
			driver = new EventFiringWebDriver(wbdv);

		    }

		}

	    }

	    catch (Throwable initBrowserException) {

		APPLICATION_LOGS
			.debug("Error came while creating a browser instance : " + initBrowserException.getMessage());

		return "Fail : Error came while creating a browser instance : " + initBrowserException.getMessage();

	    }

	}

	APPLICATION_LOGS.debug("Created browser instance successfully");

	try {

	    // Implicitly wait for 30 seconds for browser to open
	    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

	    // Delete all browser cookies
	    driver.manage().deleteAllCookies();

	    // Navigate to Curaspan application
	    driver.navigate().to(CONFIG.getProperty("dischargeCentralURL"));

	    // Handle certificate error
	    if (CONFIG.getProperty("test_browser").contains("Internet Explorer")) {

		if (driver.getTitle().contains(navigationBlockedTitle)) {

		    driver.navigate().to("javaScript:document.getElementById('overridelink').click()");

		    FunctionLibrary.waitForPageToLoad();

		}

	    }

	    // Maximize browser window
	    APPLICATION_LOGS.debug("Maximizing Browser window...");
	    driver.manage().window().maximize();
	    APPLICATION_LOGS.debug("Browser window is maximized");

	    if (CONFIG.getProperty("test_browser").contains("Firefox")) {
		WebElement html = driver.findElement(By.tagName("html"));
		html.sendKeys(Keys.chord(Keys.CONTROL, "0"));
	    }

	    if (FunctionLibrary.isElementDisplayed(locatorUnsupportBrowserDialog, nameUnsupportBrowserDialog)) {
		methodReturnResult = FunctionLibrary.clickLink(locatorUnsupportBrowserDialog,
			nameUnsupportBrowserDialog);
		if (methodReturnResult.contains(failTest)) {
		    return methodReturnResult;
		}
	    }
	}

	catch (Throwable navigationError) {

	    APPLICATION_LOGS.debug(
		    "Error came while navigating to the Discharge Central site : " + navigationError.getMessage());

	}

	// Verify Login page appears
	expectedTitle = dischargeCentralLoginPageTitle;
	methodReturnResult = FunctionLibrary.assertTitle(expectedTitle);
	if (methodReturnResult.contains(failTest)) {

	    // Log result
	    APPLICATION_LOGS.debug("Not navigated to Login page");
	    return methodReturnResult;

	}

	APPLICATION_LOGS.debug("Navigated to Login page");

	APPLICATION_LOGS.debug("Logging in to Discharge Central ...");

	String userName = null;
	String password = null;

	try {

	    userName = testData.getCellData("loginToDischargeCentral", "UserName", Data_Row_No);
	    password = testData.getCellData("loginToDischargeCentral", "Password", Data_Row_No);

	    APPLICATION_LOGS.debug("Successfully Retrieved data from Xls File :-  Username : " + userName
		    + " and Password : " + password);

	}

	catch (Throwable fetchExcelDataError) {

	    APPLICATION_LOGS.debug("Error while retrieving data from xls file" + fetchExcelDataError.getMessage());
	    return "Error while retrieving data from xls file" + fetchExcelDataError.getMessage();

	}

	// Clear Username input-box and input username
	methodReturnResult = FunctionLibrary.clearAndInput(locatorDCUserNameInputBox, nameDCUserNameInputBox, userName);

	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Clear Password input-box and input password
	methodReturnResult = FunctionLibrary.clearAndInput(locatorDCPasswordInputBox, nameDCPasswordInputBox, password);

	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Click on Sign In button
	methodReturnResult = FunctionLibrary.clickAndWait(locatorDCSignInButton, nameDCSignInButton);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Verify login was successful
	expectedTitle = dischargeCentralLoginPageTitle;
	String actualTitle = null;

	for (int i = 0; i < 3; i++) {

	    // Get page title
	    actualTitle = FunctionLibrary.getPageTitle();

	    if (actualTitle.equals(expectedTitle)) {

		// Log result
		APPLICATION_LOGS.debug("Failed to login. Retrying ...");

		// Clear Username input-box and input username
		FunctionLibrary.clearAndInput(locatorDCUserNameInputBox, nameUserNameInputBox, userName);

		// Clear Password input-box and input password
		FunctionLibrary.clearAndInput(locatorDCPasswordInputBox, namePasswordInputBox, password);

		// Click on Sign In button
		FunctionLibrary.clickAndWait(locatorDCSignInButton, nameDCSignInButton);

	    } else {
		break;
	    }

	}

	// Click on Continue button if present
	if (FunctionLibrary.isElementDisplayed(locatorContinueBtn, nameContinueBtn)) {
	    methodReturnResult = FunctionLibrary.clickLink(locatorContinueBtn, nameContinueBtn);
	    if (methodReturnResult.contains(failTest)) {
		return methodReturnResult;
	    }

	}
	FunctionLibrary.waitForElementToLoad(locatorDischargeTab);

	// Click on Remind later button
	if (FunctionLibrary.isElementDisplayed(By.id("profileUpdateReminderCancel"), "Profile update Reminder")) {
	    FunctionLibrary.clickLink(By.id("profileUpdateReminderCancel"), "Remind Later Btn");
	}

	APPLICATION_LOGS.debug("Logged into Discharge Central");

	APPLICATION_LOGS.debug("Navigating to Profile page ...");

	// Navigate to profile page
	methodReturnResult = navigateToProfilePage();
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Select the hospital dropdown
	methodReturnResult = FunctionLibrary.clickLink(locatorHospitalDropdown, nameHospitalDropdown);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Select the hospital from the dropdown
	methodReturnResult = FunctionLibrary.clickLink(By.xpath("//a[contains(text(),'" + referralHospitalName + "')]"),
		"Hospital dropdown");
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Wait for the result to appear
	if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
	    FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
	}

	// Wait for the Loading text to disappear
	if (FunctionLibrary.isElementDisplayed(locatorLoadingText, nameLoadingText)) {
	    FunctionLibrary.waitForElementToDisappear(locatorLoadingText, nameLoadingText);
	}

	APPLICATION_LOGS.debug("Navigated to Profile tab");

	return "Pass : Navigated to Login page, Logged into Discharge Central and Navigated to Profile tab and selected hospital from dropdown";

    }

    // Navigate to Discharge page
    public static String archivePatientFromReferralWorkbook(String dischargeDateString, String firstPatientName)
	    throws InterruptedException, BiffException, IOException {

	APPLICATION_LOGS.debug("Archive patient from Referral workbook page ...");

	// Click on action dropdown
	methodReturnResult = FunctionLibrary.clickLink(locatorActionDropdown, nameActionDropdown);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Click on archive referral action
	methodReturnResult = FunctionLibrary.clickLink(locatorArchiveReferralAction, nameArchiveReferralAction);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

	// Check for the presence of acceptance complete cancel button
	if (FunctionLibrary.isElementDisplayed(locatorAcceptancePanelWithNoResponse,
		nameAcceptancePanelWithNoResponse)) {

	    if (CONFIG.getProperty("test_browser").contains("Firefox")) {
		((JavascriptExecutor) driver).executeScript(
			"var el=arguments[0]; setTimeout(function() { el.click(); }, 100);",
			driver.findElement(locatorAcceptanceCancelBtn));
	    } else {
		methodReturnResult = FunctionLibrary.clickLink(locatorAcceptanceCancelBtn, nameAcceptanceCancelBtn);
		if (methodReturnResult.contains(failTest)) {
		    return methodReturnResult;
		}
	    }
	    // Click on the patient name
	    methodReturnResult = FunctionLibrary.clickAndWait(locatorReferralNameLink, nameReferralNameLink);
	    if (methodReturnResult.contains(failTest)) {
		return methodReturnResult;
	    }

	    // Wait for the patient to disappear
	    if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
		FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
	    }

	    // Wait for page to load
	    FunctionLibrary.waitForPageToLoad();

	    // Click on 'Acceptance' tab and wait for page to load
	    methodReturnResult = FunctionLibrary.clickAndWait(locatorAcceptanceTab, nameAcceptanceTab);
	    if (methodReturnResult.contains(failTest)) {
		return methodReturnResult;
	    }

	    // Wait for the result to appear
	    if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
		FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
	    }

	    // Search for the patient name
	    methodReturnResult = DischargeCompleteBaseLibrary.searchPatient(firstPatientName);
	    if (methodReturnResult.contains(failTest)) {
		return methodReturnResult;
	    }

	    // Click on action dropdown
	    methodReturnResult = FunctionLibrary.clickLink(locatorActionDropdown, nameActionDropdown);
	    if (methodReturnResult.contains(failTest)) {
		return methodReturnResult;
	    }

	    // Click on archive referral action
	    methodReturnResult = FunctionLibrary.clickLink(locatorArchiveReferralAction, nameArchiveReferralAction);
	    if (methodReturnResult.contains(failTest)) {
		return methodReturnResult;
	    }

	    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

	}

	if (FunctionLibrary.isElementDisplayed(locatorAdmissionDateDiv, nameAdmissionDateDiv)) {

	    // Wait for save and remove button to appear
	    if (!FunctionLibrary.isElementDisplayed(locatorSaveAndRemoveBtn, nameSaveAndRemoveBtn)) {
		return failTest + " : Save and remove button is not displayed after clicking on archive patient link";
	    }

	    // Click on admission date div and click on save and remove without
	    // giving the required information
	    methodReturnResult = FunctionLibrary.clickLink(locatorAdmissionDateDiv, nameAdmissionDateDiv);
	    if (methodReturnResult.contains(failTest)) {
		return methodReturnResult;
	    }
	    if (dischargeDateString.equals("")) {

		// Get the admission date
		Date newAdmissionDate = FunctionLibrary.subtractDate(2);

		// Input the admission date as today's date
		Format formatter = new SimpleDateFormat("MMMM");
		String admissionMonth = formatter.format(newAdmissionDate);
		Format yearFormatter = new SimpleDateFormat("yyyy");
		String admissionYear = yearFormatter.format(newAdmissionDate);
		Format dateFormatter = new SimpleDateFormat("d");
		String admissionDate = dateFormatter.format(newAdmissionDate);

		// Select the Calendar
		methodReturnResult = FunctionLibrary.clickLink(locatorAdmissionDateCalendar, nameAdmissionDateCalendar);
		if (methodReturnResult.contains("Fail")) {
		    return methodReturnResult;
		}

		// Click on month link
		methodReturnResult = FunctionLibrary.clickLink(locatorAdmissionMonthLink, nameAdmissionMonthLink);
		if (methodReturnResult.contains("Fail")) {
		    return methodReturnResult;
		}

		// Select the month from dropdown
		methodReturnResult = FunctionLibrary.selectValueByVisibleText(locatorAdmissionMonthDropdown,
			admissionMonth, nameAdmissionMonthDropdown);
		if (methodReturnResult.contains("Fail")) {
		    return methodReturnResult;
		}

		// Type the year in year textbox
		methodReturnResult = FunctionLibrary.clearAndInput(locatorAdmissionYearInput, nameAdmissionYearInput,
			admissionYear);
		if (methodReturnResult.contains("Fail")) {
		    return methodReturnResult;
		}

		// Click on ok button
		methodReturnResult = FunctionLibrary.clickLink(locatorAdmissionSubmitBtn, nameAdmissionSubmitBtn);
		if (methodReturnResult.contains("Fail")) {
		    return methodReturnResult;
		}

		// Select the date
		methodReturnResult = FunctionLibrary.clickLink(By.linkText(admissionDate), "Birth date");
		if (methodReturnResult.contains("Fail")) {
		    return methodReturnResult;
		}

	    }

	    // Click on select time button
	    methodReturnResult = FunctionLibrary.clickLink(locatorSelectTimeButton, nameSelectTimeButton);
	    if (methodReturnResult.contains("Fail")) {
		return methodReturnResult;
	    }

	    // Select the time
	    methodReturnResult = FunctionLibrary.clickLink(locatorTimeValue, nameTimeValue);
	    if (methodReturnResult.contains("Fail")) {
		return methodReturnResult;
	    }

	    // Click on save and remove button
	    methodReturnResult = FunctionLibrary.clickLink(locatorSaveAndRemoveBtn, nameSaveAndRemoveBtn);
	    if (methodReturnResult.contains(failTest)) {
		return methodReturnResult;
	    }

	    // Wait for the patient to disappear
	    if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
		FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
	    }

	}

	else {

	    // Wait for save and remove button to appear
	    if (!FunctionLibrary.isElementDisplayed(locatorSaveAndRemoveBtnWithoutDischargeDate,
		    nameSaveAndRemoveBtn)) {
		return failTest + " : Save and remove button is not displayed after clicking on archive patient link";
	    }

	    // Click on save and remove button
	    methodReturnResult = FunctionLibrary.clickLink(locatorSaveAndRemoveBtnWithoutDischargeDate,
		    nameSaveAndRemoveBtn);
	    if (methodReturnResult.contains(failTest)) {
		return methodReturnResult;
	    }

	    // Wait for the patient to disappear
	    if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
		FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
	    }
	}

	// Search for the deleted patient
	methodReturnResult = DischargeCompleteBaseLibrary.searchPatient(firstPatientName);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Check if the deleted patient exists in Referral workbook page
	if (FunctionLibrary.isElementDisplayed(locatorPatientLink, namePatientLink)) {
	    return failTest + " : Patient exists after the deletion from referral workbook page";
	}

	return "Pass : Navigated to Profile Page";

    }

    // Search for archived patient in Archive page of Referral Central
    public static String searchPatientInArchivePage(String patientName, String admitDateString)
	    throws InterruptedException, BiffException, IOException {

	APPLICATION_LOGS.debug("Search for archived patient in archive page of Referral Central...");

	String admitDateAfterDayRemoval = null;

	// Clear and input patient name in archive search input
	methodReturnResult = FunctionLibrary.clearAndInput(locatorArchivePatientFilterInput,
		nameArchivePatientFilterInput, patientName);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	int spacePos = admitDateString.indexOf(" ");
	if (spacePos > 0) {
	    admitDateAfterDayRemoval = admitDateString.substring(0, spacePos);
	}

	// Change the from date
	Date admitDate = FunctionLibrary.convertStringToDate(admitDateAfterDayRemoval, "MM-dd-yy");

	// Input the admission date as today's date
	Format formatter = new SimpleDateFormat("MMMM");
	String admitMonth = formatter.format(admitDate);
	Format yearFormatter = new SimpleDateFormat("yyyy");
	String admitYear = yearFormatter.format(admitDate);
	Format dateFormatter = new SimpleDateFormat("d");
	String admissionDate = dateFormatter.format(admitDate);

	// Select the Calendar
	methodReturnResult = FunctionLibrary.clickLink(locatorFromDateCalendar, nameFromDateCalendar);
	if (methodReturnResult.contains("Fail")) {
	    return methodReturnResult;
	}

	// Click on month link
	methodReturnResult = FunctionLibrary.clickLink(locatorFromMonthLink, nameFromMonthLink);
	if (methodReturnResult.contains("Fail")) {
	    return methodReturnResult;
	}

	// Select the month from dropdown
	methodReturnResult = FunctionLibrary.selectValueByVisibleText(locatorFromMonthDropdown, admitMonth,
		nameFromMonthDropdown);
	if (methodReturnResult.contains("Fail")) {
	    return methodReturnResult;
	}

	// Type the year in year textbox
	methodReturnResult = FunctionLibrary.clearAndInput(locatorFromYearInput, nameFromYearInput, admitYear);
	if (methodReturnResult.contains("Fail")) {
	    return methodReturnResult;
	}

	// Click on ok button
	methodReturnResult = FunctionLibrary.clickLink(locatorFromSubmitBtn, nameFromSubmitBtn);
	if (methodReturnResult.contains("Fail")) {
	    return methodReturnResult;
	}

	// Select the date
	methodReturnResult = FunctionLibrary.clickLink(By.linkText(admissionDate), "From date");
	if (methodReturnResult.contains("Fail")) {
	    return methodReturnResult;
	}

	// Click on search button
	methodReturnResult = FunctionLibrary.clickLink(locatorReferralFilter, nameReferralFilter);
	if (methodReturnResult.contains("Fail")) {
	    return methodReturnResult;
	}

	// Wait for the result to appear
	if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
	    FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
	}

	// check if the searched patient exists
	if (!FunctionLibrary.isElementDisplayed(
		By.xpath("//div[@id='Table']/div[3]/table/tbody[2]/tr/td[2]/div/div[text()='" + patientName + "']"),
		namePatientInArchivePage)) {
	    return failTest + " : Patient is not displayed in archive page after searching for archived patient";
	}

	return "Pass : Searched for archived patient in archive page of referral central";

    }

    // Discharge
    // Search for archived patient in Archive page of Referral Central
    public static String dischargePatientFromDischargeCentral(String patientName, String estimatedDischargeDate,
	    String referralHopsital) throws InterruptedException, BiffException, IOException {

	APPLICATION_LOGS.debug("Discharge a patient from discharge central...");

	String EDDAfterDayRemoval = null;

	// Clear and input patient name in archive search input
	methodReturnResult = FunctionLibrary.clickLink(locatorDischargeIcon, nameDischargeIcon);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	FunctionLibrary.waitForNewWindow(1);

	// Switch to New Window
	methodReturnResult = FunctionLibrary.switchToPopupWindow();
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Handle certificate error
	if (CONFIG.getProperty("test_browser").contains("Internet Explorer")) {

	    if (driver.getTitle().contains(navigationBlockedTitle)) {

		driver.navigate().to("javaScript:document.getElementById('overridelink').click()");

		FunctionLibrary.waitForPageToLoad();

	    }

	}

	// Verify we navigated to Config Entry Page
	expectedTitle = DischargeCompletionPageTitle;
	methodReturnResult = FunctionLibrary.assertTitle(expectedTitle);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	int spacePos = estimatedDischargeDate.indexOf(" ");
	if (spacePos > 0) {
	    EDDAfterDayRemoval = estimatedDischargeDate.substring(0, spacePos);
	}

	// Change the from date
	Date EDDDate = FunctionLibrary.convertStringToDate(EDDAfterDayRemoval, "MM-dd-yy");

	// Input the admission date as today's date
	Format formatter = new SimpleDateFormat("MMMM");
	String EDDMonth = formatter.format(EDDDate);
	Format yearFormatter = new SimpleDateFormat("yyyy");
	String EDDYear = yearFormatter.format(EDDDate);
	Format dateFormatter = new SimpleDateFormat("d");
	String EDDDay = dateFormatter.format(EDDDate);

	// Get the current Date
	String currentDateString = util.TestUtil.now("MMMM-dd-yyyy");

	// Change the from date
	Date currentDate = FunctionLibrary.convertStringToDate(currentDateString, "MMMM-dd-yyyy");

	// Input the admission date as today's date
	String currentMonth = formatter.format(currentDate);
	String currentYear = yearFormatter.format(currentDate);
	String currentDay = dateFormatter.format(currentDate);

	if (EDDDate.compareTo(currentDate) > 0) {
	    // Select the Calendar
	    methodReturnResult = FunctionLibrary.clickLink(locatorActualDischargeDateCalendar,
		    nameDischargeDateCalendar);
	    if (methodReturnResult.contains("Fail")) {
		return methodReturnResult;
	    }

	    // Click on month link
	    methodReturnResult = FunctionLibrary.clickLink(locatorActualDischargeMonthLink, nameDischargeMonthLink);
	    if (methodReturnResult.contains("Fail")) {
		return methodReturnResult;
	    }

	    // Select the month from dropdown
	    methodReturnResult = FunctionLibrary.selectValueByVisibleText(locatorActualDischargeMonthDropdown,
		    currentMonth, nameDischargeMonthDropdown);
	    if (methodReturnResult.contains("Fail")) {
		return methodReturnResult;
	    }

	    // Type the year in year textbox
	    methodReturnResult = FunctionLibrary.clearAndInput(locatorActualDischargeYearInput, nameDischargeYearInput,
		    currentYear);
	    if (methodReturnResult.contains("Fail")) {
		return methodReturnResult;
	    }

	    // Click on ok button
	    methodReturnResult = FunctionLibrary.clickLink(locatorActualDischargeSubmitBtn, nameDischargeSubmitBtn);
	    if (methodReturnResult.contains("Fail")) {
		return methodReturnResult;
	    }

	    // Select the date
	    methodReturnResult = FunctionLibrary.clickLink(By.linkText(currentDay), "From date");
	    if (methodReturnResult.contains("Fail")) {
		return methodReturnResult;
	    }
	}

	else {
	    // Select the Calendar
	    methodReturnResult = FunctionLibrary.clickLink(locatorActualDischargeDateCalendar,
		    nameDischargeDateCalendar);
	    if (methodReturnResult.contains("Fail")) {
		return methodReturnResult;
	    }

	    // Click on month link
	    methodReturnResult = FunctionLibrary.clickLink(locatorActualDischargeMonthLink, nameDischargeMonthLink);
	    if (methodReturnResult.contains("Fail")) {
		return methodReturnResult;
	    }

	    // Select the month from dropdown
	    methodReturnResult = FunctionLibrary.selectValueByVisibleText(locatorActualDischargeMonthDropdown, EDDMonth,
		    nameDischargeMonthDropdown);
	    if (methodReturnResult.contains("Fail")) {
		return methodReturnResult;
	    }

	    // Type the year in year textbox
	    methodReturnResult = FunctionLibrary.clearAndInput(locatorActualDischargeYearInput, nameDischargeYearInput,
		    EDDYear);
	    if (methodReturnResult.contains("Fail")) {
		return methodReturnResult;
	    }

	    // Click on ok button
	    methodReturnResult = FunctionLibrary.clickLink(locatorActualDischargeSubmitBtn, nameDischargeSubmitBtn);
	    if (methodReturnResult.contains("Fail")) {
		return methodReturnResult;
	    }

	    // Select the date
	    methodReturnResult = FunctionLibrary.clickLink(By.linkText(EDDDay), "From date");
	    if (methodReturnResult.contains("Fail")) {
		return methodReturnResult;
	    }
	}
	// Check referral provider checkbox
	methodReturnResult = FunctionLibrary
		.checkCheckBox(By.xpath(".//*[@id='providertab']/tbody/tr/td[2][contains(text(),'" + referralHopsital
			+ "')]/preceding-sibling::td/input"), "Referral Checkbox");
	if (methodReturnResult.contains("Fail")) {
	    return methodReturnResult;
	}

	// Click on save and keep
	methodReturnResult = FunctionLibrary.clickLink(locatorSaveAndKeepBtn, nameSaveAndKeepBtn);
	if (methodReturnResult.contains("Fail")) {
	    return methodReturnResult;
	}

	// Check if the confirm dialog is present
	if (FunctionLibrary.isElementDisplayed(locatorConfirmDialog, nameConfirmDialog)) {
	    methodReturnResult = FunctionLibrary.clickLink(locatorConfirmDischargeBtn, nameConfirmDischargeBtn);
	    if (methodReturnResult.contains("Fail")) {
		return methodReturnResult;
	    }
	}

	// Check for presence of processing div
	if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
	    FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
	}

	// Switch to default window
	FunctionLibrary.switchToDefaultWindow();

	// Search for the patient
	methodReturnResult = searchPatient(patientName);
	if (methodReturnResult.contains("Fail")) {
	    return methodReturnResult;
	}

	// Check if the Discharge date is present
	if (FunctionLibrary.retrieveText(locatorDischargeDateText, nameDischargeDateText).equals("")) {
	    return failTest + " : Discharge date is not updated in discharge workbook page after discharging patient";
	}

	return "Pass : Discharged a patient from Discharge central";

    }

    // Book a referral from implementation page
    public static String bookReferral() throws InterruptedException, BiffException, IOException {

	APPLICATION_LOGS.debug("Book a referral from implementation page...");

	// Select any provider which is not booked
	// Get the total number of providers
	int totalLOCInImplementationPage = driver.findElements(By.xpath(".//*[@id='showProvidersByLoc']/div")).size();

	int LOCWithNotBookedStatus = 0;
	int rowWithNotBookedStatus = 0;
	int totalProviderUnderLOC = 0;

	for (int i = 1; i <= totalLOCInImplementationPage; i++) {
	    // Get the number of providers in each LOC
	    totalProviderUnderLOC = driver
		    .findElements(By.xpath(
			    ".//*[@id='showProvidersByLoc']/div[" + i + "]/div/div[2]/div/div[3]/table/tbody[2]/tr"))
		    .size();
	    for (int j = 1; j <= totalProviderUnderLOC; j++) {
		// get the booking status
		if (!FunctionLibrary.isElementDisplayed(
			By.xpath(
				".//*[@id='showProvidersByLoc']/div[" + i + "]/div/div[2]/div/div[3]/table/tbody[2]/tr["
					+ j + "]/td[3]//*[contains(text(),'Booked')]"),
			"Provider with booked status")) {
		    LOCWithNotBookedStatus = i;
		    rowWithNotBookedStatus = j;
		    break;
		}
	    }

	}

	// Get the name of provider without booked status
	String providerWithNoBookedStatus = FunctionLibrary.retrieveText(
		By.xpath(".//*[@id='showProvidersByLoc']/div[" + LOCWithNotBookedStatus
			+ "]/div/div[2]/div/div[3]/table/tbody[2]/tr[" + rowWithNotBookedStatus + "]/td[2]//a/div"),
		"Provider with no booked status");

	// Check the checkbox and click on book referral button
	methodReturnResult = FunctionLibrary.checkCheckBox(
		By.xpath(".//*[@id='showProvidersByLoc']/div[" + LOCWithNotBookedStatus
			+ "]/div/div[2]/div/div[3]/table/tbody[2]/tr[" + rowWithNotBookedStatus + "]/td[1]//input"),
		"Checkbox of provider");
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Click on book referral button
	methodReturnResult = FunctionLibrary.clickLink(locatorBookReferralButton, nameBookReferralButton);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Wait for the result to appear
	if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
	    FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
	}

	// click on book button in book referral dialog
	methodReturnResult = FunctionLibrary.clickLink(locatorBookReferralButtonInBookReferralDialog,
		nameBookReferralButtonInBookReferralDialog);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Wait for the result to appear
	if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
	    FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
	}

	// Check for the status change for the provider
	if (!FunctionLibrary.isElementDisplayed(By.xpath(".//*[@id='showProvidersByLoc']/div[" + LOCWithNotBookedStatus
		+ "]/div/div[2]/div/div[3]/table/tbody[2]/tr[" + rowWithNotBookedStatus
		+ "]/td[3]//*[contains(text(),'Booked')]"), "Status change for the provider")) {
	    return failTest + " : Status of the provider is not changed to booked after booking it";
	}

	return providerWithNoBookedStatus;

    }

    public static ArrayList<String> getNamesOfProvidersWithBookingRequest(String MRN, String securityId,
	    String facilityId) {
	ResultSet rs = null;
	int providersCount = 0;
	ArrayList<String> results = new ArrayList<String>();

	APPLICATION_LOGS.debug("Getting the names of providers with booking request for MRN = " + MRN);

	String query = "SELECT name FROM facility WHERE id IN (SELECT facility_id FROM booking_request WHERE discharge_id = (SELECT id FROM discharge WHERE mrn='"
		+ MRN + "'" + " AND facility_id = " + facilityId + " AND security_id = " + securityId
		+ " AND active = 'yes'))";

	try {

	    // Connecting to Database
	    con = java.sql.DriverManager.getConnection(connectionString, dbUsername, dbPassword);

	    con.setAutoCommit(false);

	    // Create a Statement object for sending SQL statements to the
	    // database
	    stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);

	    // Execute the query
	    rs = stmt.executeQuery(query);
	    int ProviderCount = 0;
	    int i = 1;

	    while (rs.next()) {
		results.add(rs.getString(i));
	    }

	}

	catch (SQLException sqlException) {

	    APPLICATION_LOGS
		    .debug("Error came while trying to fetch names of subscribers : " + sqlException.getMessage());

	}

	APPLICATION_LOGS.debug(
		"The names of providers with booking request for the patient with MRN =" + MRN + " is retrieved");

	for (String abc : results) {
	    System.out.println(abc);
	}
	// Return subscribers count
	return results;

    }

    public static Date getDischargeDateUsingMRN(String MRN, String securityId, String facilityId) {
	ResultSet rs = null;
	Date result = null;

	APPLICATION_LOGS.debug("Getting the discharge date for MRN = " + MRN);

	String query = "SELECT actual_discharge FROM discharge WHERE id = (SELECT id FROM discharge WHERE mrn ='" + MRN
		+ "' AND facility_id = " + facilityId + " AND security_id = " + securityId + " AND active = 'yes')";

	try {

	    // Connecting to Database
	    con = java.sql.DriverManager.getConnection(connectionString, dbUsername, dbPassword);

	    con.setAutoCommit(false);

	    // Create a Statement object for sending SQL statements to the
	    // database
	    stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);

	    // Execute the query
	    rs = stmt.executeQuery(query);
	    while (rs.next()) {
		result = rs.getDate(1);

	    }
	}

	catch (SQLException sqlException) {

	    APPLICATION_LOGS
		    .debug("Error came while trying to fetch discharge date of Patient : " + sqlException.getMessage());

	}

	APPLICATION_LOGS.debug("The Discharge date for the patient with MRN =" + MRN + " is retrieved");

	// Return discharge date String
	return result;

    }

    public static String updateRecentlyDischargedPatient(String MRN, String securityId, String facilityId) {
	ResultSet rs = null;

	APPLICATION_LOGS.debug("Update recently discharged patient for MRN = " + MRN);

	String query = "UPDATE discharge SET last_update=SYSDATE,state=NULL,recently_discharged=SYSDATE WHERE id = (SELECT id FROM discharge WHERE mrn ='"
		+ MRN + "' AND facility_id = " + facilityId + " AND security_id = " + securityId
		+ " AND active = 'yes')";

	try {

	    // Connecting to Database
	    con = java.sql.DriverManager.getConnection(connectionString, dbUsername, dbPassword);
	    con.setAutoCommit(false);

	    // Create a Statement object for sending SQL statements to the
	    // database
	    stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);

	    // Execute the query
	    rs = stmt.executeQuery(query);
	    con.commit();
	}

	catch (SQLException sqlException) {

	    APPLICATION_LOGS.debug(
		    "Error came while trying to update recently discharged patient : " + sqlException.getMessage());
	    return failTest + " : Error came while trying to update recently discharged patient...";
	}

	APPLICATION_LOGS.debug("The recently discharged date for the patient with MRN =" + MRN + " is updated");
	return "Pass : Query to move patient from Workbook to Recently Discharge page ran successfully...";

    }

    /**
     * Set facility_config flag for the facility
     *
     * @param facilityId
     * @param configName
     * @param configValue
     * @return Pass/Fail
     *
     */
    public static String setFacilityConfig(String facilityId, String configName, String configValue) {

	APPLICATION_LOGS.debug("Setting " + configName + " facility config value to " + configValue + " ...");

	// Store the select query
	String getFacilityConfigQuery = "SELECT * FROM facility_config " + "WHERE facility_id = " + facilityId
		+ " AND config_default_id IN(SELECT id " + "FROM facility_config_default " + "WHERE config_name = '"
		+ configName + "')";

	// Store the update query
	String setFacilityConfigQuery = "UPDATE facility_config SET config_value = '" + configValue
		+ "' WHERE facility_id = " + facilityId + " AND config_default_id IN(SELECT id "
		+ "FROM facility_config_default " + "WHERE config_name = '" + configName + "')";

	// Store the insert query
	String insertFacilityConfigQuery = "INSERT INTO facility_config "
		+ "(id, facility_id, config_default_id, config_value, active, notes) VALUES "
		+ "(facility_config_seq.nextval, '" + facilityId
		+ "', (SELECT id FROM facility_config_default WHERE config_name = '" + configName + "'),  '"
		+ configValue + "', 'yes', NULL)";

	try {

	    // Connecting to Database
	    con = java.sql.DriverManager.getConnection(connectionString, dbUsername, dbPassword);
	    con.setAutoCommit(false);

	    // Create a Statement object for sending SQL statements
	    // to the database
	    Statement stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);

	    // Execute the update query if there is any record present to update
	    // Execute the select query
	    rs = stmt.executeQuery(getFacilityConfigQuery);
	    if (rs.next()) {

		APPLICATION_LOGS.debug("Updating record present in facility_config table ...");

		int updated = stmt.executeUpdate(setFacilityConfigQuery);
		APPLICATION_LOGS.debug("Updated : " + updated + " rows");

		// Commit the changes
		APPLICATION_LOGS.debug("Commiting changes ...");
		con.commit();
		APPLICATION_LOGS.debug("Committed changes successfully");

	    }

	    // Insert the record into facility_config table if there is no
	    // record present to update
	    else {

		APPLICATION_LOGS.debug("Inserting record into facility_config table ...");

		stmt.executeUpdate(insertFacilityConfigQuery);

		APPLICATION_LOGS.debug("Inserted record into facility_config table");

		// Commit the changes
		APPLICATION_LOGS.debug("Commiting changes ...");
		con.commit();
		APPLICATION_LOGS.debug("Committed changes successfully");

	    }

	}

	catch (SQLException sqlException) {

	    APPLICATION_LOGS.debug("Error came while running the sql : " + sqlException.getMessage());
	    return failTest + " : Set " + configName + " facility config value to " + configValue;

	}

	return "Pass : Set " + configName + " facility config value to " + configValue;

    }
    
    
    public static String updateFacilityConfig(String facilityId, String configName, String configValue) {

	APPLICATION_LOGS.debug("Setting " + configName + " facility config value to " + configValue + " ...");


	// Store the update query
	String setFacilityConfigQuery = "Update facility_config set CONFIG_VALUE='"+configValue +"' WHERE config_default_id =(Select Id From Facility_Config_Default WHERE config_name = '"+ 
			configName +"')and facility_id="+ facilityId ;


	try {

	    // Connecting to Database
	    con = java.sql.DriverManager.getConnection(connectionString, dbUsername, dbPassword);
	    con.setAutoCommit(false);

	    // Create a Statement object for sending SQL statements
	    // to the database
	    Statement stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);

	    // Execute the update query if there is any record present to update
	    // Execute the select query
	
	    

		APPLICATION_LOGS.debug("Updating record present in facility_config table ...");

		int updated = stmt.executeUpdate(setFacilityConfigQuery);
		APPLICATION_LOGS.debug("Updated : " + updated + " rows");

		// Commit the changes
		APPLICATION_LOGS.debug("Commiting changes ...");
		con.commit();
		APPLICATION_LOGS.debug("Committed changes successfully");



	    // Insert the record into facility_config table if there is no
	    // record present to update
	

	}

	catch (SQLException sqlException) {

	    APPLICATION_LOGS.debug("Error came while running the sql : " + sqlException.getMessage());
	    return failTest + " : Set " + configName + " facility config value to " + configValue;

	}

	return "Pass : Set " + configName + " facility config value to " + configValue;

    }
    
    public static String generateParamName(String param_name) {

    	APPLICATION_LOGS.debug("Retriving the value" + param_name);

    	String param_value = null;
    	ResultSet rs = null;
    	
    	// Store the update query
    	String query = "Select Description, Param_name, id , param_value from application_config where param_name='"+param_name+"'" ;


    	try {

    	    // Connecting to Database
    	    con = java.sql.DriverManager.getConnection(connectionString, dbUsername, dbPassword);
    	    con.setAutoCommit(false);

    	    // Create a Statement object for sending SQL statements
    	    // to the database
    	    Statement stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);


    		 rs = stmt.executeQuery(query);
    		 
    		 if (rs.next()) {
    			 	param_value = rs.getString("param_value");
    				}

    				APPLICATION_LOGS.debug("Fetched value from database : "
    				+ param_value);

    	}

    	catch (SQLException sqlException) {

    	    APPLICATION_LOGS.debug("Error came while running the sql : " + sqlException.getMessage());
    	   
    	}

    	return param_value;

        }
 

    @SuppressWarnings("unchecked")
    public static ArrayList<Integer> getfacilityforconfig(int configid) {
	ResultSet rs = null;
	int count = 0;
	ArrayList<Integer> facilityListfromDB = new ArrayList<Integer>();

	APPLICATION_LOGS.debug("Getting the facilities for the config with id = " + configid);

	String query = "SELECT facility_id FROM facility_config WHERE config_default_id=" + configid;
	try {

	    // Connecting to Database
	    con = java.sql.DriverManager.getConnection(connectionString, dbUsername, dbPassword);

	    con.setAutoCommit(false);

	    // Create a Statement object for sending SQL statements to the
	    // database
	    stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);

	    // Execute the query
	    rs = stmt.executeQuery(query);
	    while (rs.next()) {
		int facility = rs.getInt("facility_id");
		facilityListfromDB.add(facility);
	    }
	}

	catch (SQLException sqlException) {

	    APPLICATION_LOGS.debug("Error came while trying to facilities for the config" + sqlException.getMessage());

	}

	APPLICATION_LOGS.debug("The facilities for the config=" + configid + " is retrieved");

	// Return facility list
	return facilityListfromDB;
    }

    public static ArrayList<String> getDischargeStatus(String LocName) {
	ResultSet rs = null;

	ArrayList<String> DischargeStatusForLoc = new ArrayList<String>();

	APPLICATION_LOGS.debug("Getting the discharge status for LOC = " + LocName);

	String query = "SELECT ds.CODE,ds.NAME FROM DISCHARGE_STATE ds " + "INNER JOIN DISCHARGE_STATE_LOCUS dsl "
		+ "ON ds.ID = dsl.DISCHARGE_STATE_ID AND dsl.LOCUS_ID IN (SELECT id FROM LOCUS WHERE NAME= '" + LocName
		+ "')";

	try {

	    // Connecting to Database
	    con = java.sql.DriverManager.getConnection(connectionString, dbUsername, dbPassword);

	    con.setAutoCommit(false);

	    // Create a Statement object for sending SQL statements to the
	    // database
	    stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);

	    // Execute the query
	    rs = stmt.executeQuery(query);
	    while (rs.next()) {
		String status_code = rs.getString("CODE");
		System.out.println(status_code);
		String status_name = rs.getString("NAME");
		System.out.println(status_name);
		DischargeStatusForLoc.add(status_code + ": " + status_name);

	    }

	    System.out.println(DischargeStatusForLoc);
	}

	catch (SQLException sqlException) {

	    APPLICATION_LOGS.debug("Error came trying to retrieve discharge status " + sqlException.getMessage());

	}

	APPLICATION_LOGS.debug("Discharge status for the LOC = " + LocName + " is retrieved");

	// Return facility list
	return DischargeStatusForLoc;
    }

    // Move patient from Workbook to Recently Discharged
    public static String movePatientToRecentlyDischarged(String MRN, String securityId, String facilityId,
	    String actualDisc) {

	actualDisc = "SYSDATE + " + actualDisc;
	ResultSet rs = null;

	APPLICATION_LOGS.debug("Move patient from workbook to recently discharged page for MRN = " + MRN);

	String query = "UPDATE discharge SET recently_discharged = " + actualDisc + ", actual_discharge = " + actualDisc
		+ " WHERE mrn = '" + MRN + "' AND facility_id = " + facilityId + " AND security_id = " + securityId
		+ " AND active = 'yes'";
	try {

	    // Connecting to Database
	    con = java.sql.DriverManager.getConnection(connectionString, dbUsername, dbPassword);
	    con.setAutoCommit(false);

	    // Create a Statement object for sending SQL statements to the
	    // database
	    stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);

	    // Execute the query
	    rs = stmt.executeQuery(query);
	    con.commit();
	}

	catch (SQLException sqlException) {

	    APPLICATION_LOGS.debug(
		    "Error came while trying to move patient to recently discharged : " + sqlException.getMessage());
	    return failTest + " : Error came while trying to move patient to recently discharged page.";
	}

	return "Pass : Patient with MRN : " + MRN + "successfully moved to recently discharged page from workbook.";
    }

    // Set EDD for discharge patient from DB
    public static String setEDDOfDischargePatient(Date date, String MRN, String securityId, String facilityId) {
	APPLICATION_LOGS.debug("Updating EDD for Discharge patient..");

	Connection con = null;
	Statement stmt = null;

	String query = "UPDATE discharge SET estimated_discharge = " + date + " WHERE mrn = '" + MRN
		+ "' AND facility_id = " + facilityId + " AND security_id = " + securityId + " AND active = 'yes'";

	try {
	    con = java.sql.DriverManager.getConnection(connectionString, dbUsername, dbPassword);
	    con.setAutoCommit(false);

	    // Create a Statement object for sending SQL statements
	    // to the database
	    stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);

	    // Execute the delete query
	    int count = stmt.executeUpdate(query);
	    APPLICATION_LOGS.debug("Updated : " + count + " row");

	    // Commit the changes
	    APPLICATION_LOGS.debug("Commiting changes ...");
	    con.commit();
	    APPLICATION_LOGS.debug("Committed changes successfully");

	} catch (SQLException sqlException) {

	    APPLICATION_LOGS.debug("Error came while trying to to update patient's EDD column of discharge table : "
		    + sqlException.getMessage());
	    return failTest + " : Error came while trying to update patient's EDD column of discharge table.";
	}
	return "Pass : EDD for discharge patient successfully updated..";
    }

    // Logout from the Discharge application
    public static String logout() throws InterruptedException {

	// Check whether the logout link is present or not
	booleanReturnResult = FunctionLibrary.isElementDisplayed(locatorLogoutLink, nameLogoutLink);
	if (!booleanReturnResult)
	    return failTest + " : Logout link is not present on the page.";

	// Click Logout link
	methodReturnResult = FunctionLibrary.clickLink(locatorLogoutLink, nameLogoutLink);
	if (methodReturnResult.contains(failTest))
	    return methodReturnResult;

	// Wait for the result to appear
	FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);

	FunctionLibrary.waitForPageToLoad();

	String actualLogOutMsg = FunctionLibrary.retrieveText(locatorLogoutMessage, nameLogoutMessage);

	// Assert logged out message
	methodReturnResult = FunctionLibrary.assertText("Logged out message", actualLogOutMsg, Constants.LOGOUTMESSAGE);
	if (methodReturnResult.contains(failTest))
	    return methodReturnResult;

	return "Pass : Logged out successfully..";
    }

    // Function to return any column value from Discharge table
    public static String rerieveColumnValFromDischarge(String MRN, String columnName, String securityId,
	    String facilityId) {

	APPLICATION_LOGS.debug("Fetching " + columnName + " value from discharge table for MRN = " + MRN);

	ResultSet rs = null;
	Connection con = null;
	Statement stmt = null;
	String colVal = null;
	String query = "SELECT " + columnName + " FROM discharge WHERE mrn = '" + MRN + "' AND facility_id = "
		+ facilityId + " AND security_id = " + securityId + " AND active = 'yes'";
	System.out.println(query);
	try {

	    // Connecting to Database
	    con = java.sql.DriverManager.getConnection(connectionString, dbUsername, dbPassword);
	    con.setAutoCommit(false);

	    // Create a Statement object for sending SQL statements to the
	    // database
	    stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);

	    // Execute the query
	    rs = stmt.executeQuery(query);
	    while (rs.next()) {
		colVal = rs.getString(1);

	    }
	}

	catch (SQLException sqlException) {

	    APPLICATION_LOGS.debug("Error came while trying to get " + columnName + " value for MRN = " + MRN + " "
		    + sqlException.getMessage());
	    return failTest + " : Error came while trying to get " + columnName + " value for MRN = " + MRN;
	}

	return colVal;
    }

    // Check for the patient in Recently Discharged page, if patient is not
    // present add a
    // new patient
    public static String preRequisiteRecDischargedPageAddPatient(String patientFirstName, String patientLastName,
	    String securityId, String facilityId)
	    throws BiffException, InterruptedException, IOException, RowsExceededException, WriteException {

	APPLICATION_LOGS.debug(
		"Check for the patient in Recently Discharged page, if patient is not present add a new patient");

	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

	// Check for the presence of patient under workbook
	int totalPatient = driver
		.findElements(By.xpath(
			"//div[@id='recentlyDischargedPanelTable']/div[3]/table//tbody[contains(@class,'data')]/tr"))
		.size();
	if (totalPatient == 0) {

	    // Click workbook link to return to workbook
	    methodReturnResult = FunctionLibrary.clickLink(locatorDischargeWorkbookLink, nameDischargeWorkbookLink);
	    if (methodReturnResult.contains(failTest)) {
		return methodReturnResult;
	    }

	    // Wait for the result to appear
	    if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
		FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
	    }

	    FunctionLibrary.waitForPageToLoad();

	    // Wait for the Loading text to disappear
	    if (FunctionLibrary.isElementDisplayed(locatorLoadingText, nameLoadingText)) {
		FunctionLibrary.waitForElementToDisappear(locatorLoadingText, nameLoadingText);
	    }

	    // Add a patient if the total patient in recently discharged page is
	    // zero
	    methodReturnResult = addPatient(patientFirstName, patientLastName);
	    if (methodReturnResult.contains(failTest)) {
		return methodReturnResult;
	    }

	    // Move newly added patient from workbook to recently discharged
	    // page
	    methodReturnResult = DischargeCompleteBaseLibrary.movePatientToRecentlyDischarged(randomMRNNo, securityId,
		    facilityId, "0");
	    if (methodReturnResult.contains(failTest)) {
		return methodReturnResult;
	    }

	    // Navigate to recently discharged page
	    methodReturnResult = FunctionLibrary.clickAndWait(locatorRecentlyDischargedLink,
		    nameRecentlyDischargedLink);
	    if (methodReturnResult.contains(failTest)) {
		return methodReturnResult;
	    }

	    // Wait for processing text to disappear
	    if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
		FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
	    }

	    // Wait for the Loading text to disappear
	    if (FunctionLibrary.isElementDisplayed(locatorLoadingText, nameLoadingText)) {
		FunctionLibrary.waitForElementToDisappear(locatorLoadingText, nameLoadingText);
	    }

	    // Verify if the page is navigated to Recently Discharge page
	    if (!FunctionLibrary.isElementDisplayed(locatorPrintRecentDischarge, namePrintRecentDischarge)) {
		return failTest + " : Page is not navigated to Recently Discharged page.";
	    }

	}

	else {
	    APPLICATION_LOGS.debug("Patient is already present in Recently Discharged page");
	}

	return "Pass : Patient pre requisite succesfully done for Recently Discharged page.";
    }

    // Navigate to Search Patients Page
    public static String navigateToSearchPatient() throws InterruptedException {
	APPLICATION_LOGS.debug("Navigating to Search Patients page....");

	// navigate to Search Patients link
	methodReturnResult = FunctionLibrary.clickLink(locatorSearchPatient, namesearchpatient);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Wait for processing text to disappear
	if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
	    FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
	}

	// Wait for page to load
	FunctionLibrary.waitForPageToLoad();

	// Assert Search Patients Page title
	expectedTitle = SearchPatientPageTitle;
	methodReturnResult = FunctionLibrary.assertTitle(expectedTitle);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	APPLICATION_LOGS.debug("Navigated to Search Patients page....");

	return "Pass : Navigated to Search Patients page";
    }

    // Create a browser instance and navigate to the Business Central site
    public static String driver2NavigateToBusinessCentral(int Data_Row_No)
	    throws MalformedURLException, InterruptedException {

	APPLICATION_LOGS.debug("Creating a browser instance and navigating to the test site ...");

	// Disable log messages
	java.util.logging.Logger.getLogger("org.apache.http.impl.client").setLevel(java.util.logging.Level.WARNING);
	if (wbdv2 == null) {
	    try {

		if (CONFIG.getProperty("is_remote").equals("true")) {

		    // Generate Remote address
		    String remote_address = "http://" + CONFIG.getProperty("remote_ip") + ":4444/wd/hub";
		    remote_url = new URL(remote_address);

		    ProfilesIni allProfiles = new ProfilesIni();
		    FirefoxProfile profile = allProfiles.getProfile("default");
		    profile.setPreference("plugins.hide_infobar_for_missing_plugin", true);
		    dc = DesiredCapabilities.firefox();
		    dc.setCapability(FirefoxDriver.PROFILE, profile);
		    dc.setJavascriptEnabled(true);

		    wbdv2 = new RemoteWebDriver(remote_url, dc);
		    driver2 = new EventFiringWebDriver(wbdv2);
		    driver2.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		}

		else {

		    ProfilesIni allProfiles = new ProfilesIni();
		    FirefoxProfile profile = allProfiles.getProfile("default");
		    profile.setAcceptUntrustedCertificates(true);
		    profile.setAssumeUntrustedCertificateIssuer(false);
		    wbdv2 = new FirefoxDriver(profile);
		    driver2 = new EventFiringWebDriver(wbdv2);

		}

	    }

	    catch (Throwable initBrowserException) {

		APPLICATION_LOGS
			.debug("Error came while creating a browser instance : " + initBrowserException.getMessage());

		return "Fail : Error came while creating a browser instance : " + initBrowserException.getMessage();

	    }

	}

	APPLICATION_LOGS.debug("Created browser instance successfully");

	try {

	    // Implicitly wait for 30 seconds for browser to open
	    driver2.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

	    // Delete all browser cookies
	    driver2.manage().deleteAllCookies();

	    // Navigate to Curaspan application
	    driver2.navigate().to(CONFIG.getProperty("testSiteURL"));

	    // Maximize browser window
	    APPLICATION_LOGS.debug("Maximizing Browser window...");
	    driver2.manage().window().maximize();
	    APPLICATION_LOGS.debug("Browser window is maximized");

	    if (CONFIG.getProperty("test_browser").contains("Firefox")) {
		WebElement html = driver2.findElement(By.tagName("html"));
		html.sendKeys(Keys.chord(Keys.CONTROL, "0"));
	    }
	}

	catch (Throwable navigationError) {

	    APPLICATION_LOGS.debug("Error came while navigating to the test site : " + navigationError.getMessage());

	}

	// Verify Login page appears
	expectedTitle = businessCentralLoginPageTitle;
	methodReturnResult = FunctionLibrary.driver2AssertTitle(expectedTitle);
	if (methodReturnResult.contains(failTest)) {

	    // Log result
	    APPLICATION_LOGS.debug("Not navigated to Login page");
	    return methodReturnResult;

	}

	APPLICATION_LOGS.debug("Navigated to Login page");

	APPLICATION_LOGS.debug("Logging in to Business Central ...");

	String userName = null;
	String password = null;

	try {
	    System.out.println(Data_Row_No);
	    userName = testData.getCellData("loginToBusinessCentral", "UserName", Data_Row_No);
	    System.out.println(userName);
	    password = testData.getCellData("loginToBusinessCentral", "Password", Data_Row_No);

	    APPLICATION_LOGS.debug("Successfully Retrieved data from Xls File :-  Username : " + userName
		    + " and Password : " + password);

	}

	catch (Throwable fetchExcelDataError) {

	    APPLICATION_LOGS.debug("Error while retrieving data from xls file" + fetchExcelDataError.getMessage());
	    return "Error while retrieving data from xls file" + fetchExcelDataError.getMessage();

	}

	// Clear Username input-box and input username
	methodReturnResult = FunctionLibrary.driver2ClearAndInput(locatorUserNameInputBox, nameUserNameInputBox,
		userName);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Clear Password input-box and input password
	methodReturnResult = FunctionLibrary.driver2ClearAndInput(locatorPasswordInputBox, namePasswordInputBox,
		password);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Click on Sign In button
	methodReturnResult = FunctionLibrary.driver2ClickAndWait(locatorSignInButton, nameSignInButton);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Verify login was successful
	expectedTitle = businessCentralLoginPageTitle;
	String actualTitle = null;

	for (int i = 0; i < 3; i++) {

	    // Get page title
	    actualTitle = FunctionLibrary.driver2GetPageTitle();

	    if (actualTitle.equals(expectedTitle)) {

		// Log result
		APPLICATION_LOGS.debug("Failed to login. Retrying ...");

		// Clear Username input-box and input username
		FunctionLibrary.driver2ClearAndInput(locatorUserNameInputBox, nameUserNameInputBox, userName);

		// Clear Password input-box and input password
		FunctionLibrary.driver2ClearAndInput(locatorPasswordInputBox, namePasswordInputBox, password);

		// Click on Sign In button
		FunctionLibrary.driver2ClickAndWait(locatorSignInButton, nameSignInButton);

	    } else {
		break;
	    }

	}

	return "Pass : Logged into business central site";

    }

    // Navigate to different pages in provider configuration
    public static String driver2SearchForValidProviderIDInProviderSetup(String providerID)
	    throws InterruptedException, BiffException, IOException {

	APPLICATION_LOGS.debug("Search for valid Provider ID ...");

	// Clear Provider input-box and input Provider ID
	methodReturnResult = FunctionLibrary.driver2ClearAndInput(locatorProviderIDInputInProviderSetup,
		nameProviderIDInputInProviderSetup, providerID);
	if (methodReturnResult.contains(failTest))
	    return methodReturnResult;

	// Click Provider Set Up page Go button
	methodReturnResult = FunctionLibrary.driver2ClickAndWait(locatorProviderSetupGoBtn, nameProviderSetupGoBtn);
	if (methodReturnResult.contains(failTest))
	    return methodReturnResult;

	// Verify if Real Time Facility Update link is present
	booleanReturnResult = FunctionLibrary.driver2IsElementDisplayed(locatorRealtimeFacilityUpdate,
		nameRealtimeFacilityUpdate);
	if (booleanReturnResult.equals(false))
	    return failTest + " : Search result for Provider ID did not appear";

	return "Pass : Searched for Provider ID in Provider Setup";

    }

    // Navigate to Provider Setup in Business Central
    public static String driver2NavigateToProviderSetup() throws InterruptedException, BiffException, IOException {

	APPLICATION_LOGS.debug("Navigating to Provider Setup ...");

	// Click on 'Provider' tab and wait for page to load
	methodReturnResult = FunctionLibrary.driver2ClickLink(locatorProviderTab, nameProviderTab);
	if (methodReturnResult.contains(failTest))
	    return methodReturnResult;

	methodReturnResult = FunctionLibrary.driver2ClickAndWait(locatorProviderSetupMnu, nameProviderSetupMnu);
	if (methodReturnResult.contains(failTest))
	    return methodReturnResult;

	// Verify we navigated to Provider Setup
	expectedTitle = ProviderSetupPageTitle;
	methodReturnResult = FunctionLibrary.driver2AssertTitle(expectedTitle);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Click on 'Provider' tab and wait for page to load
	methodReturnResult = FunctionLibrary.driver2ClickLink(locatorProviderTab, nameProviderTab);
	if (methodReturnResult.contains(failTest))
	    return methodReturnResult;

	// Click Provider Set Up link from Provider dropdown
	methodReturnResult = FunctionLibrary.driver2ClickAndWait(locatorProviderSetupMnu, nameProviderSetupMnu);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;

	}

	// Verify we navigated to Provider Setup
	expectedTitle = ProviderSetupPageTitle;
	methodReturnResult = FunctionLibrary.driver2AssertTitle(expectedTitle);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	return "Pass : Navigated to Provider Setup";

    }

    // Add Training Patient from Business Central
    public static String preRequisiteForTrainingPatient(String providerID)
	    throws InterruptedException, BiffException, IOException {

	APPLICATION_LOGS.debug(
		"Check for the training patients in Search Patients page, if patient is not present add training patients from Business Central.");

	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

	// Check for the presence of training patients in Search Patients page
	int totalPatient = driver
		.findElements(
			By.xpath(".//*[@id='searchResultSection']/div[2]//table//tbody[contains(@class,'data')]/tr"))
		.size();
	if (totalPatient == 0) {

	    FunctionLibrary.waitForPageToLoad();
	    // Navigate and login to Business Central
	    methodReturnResult = DischargeCompleteBaseLibrary.driver2NavigateToBusinessCentral(1);
	    if (methodReturnResult.contains(failTest)) {
		return methodReturnResult;
	    }

	    // Navigate to Provider Set Up page
	    methodReturnResult = DischargeCompleteBaseLibrary.driver2NavigateToProviderSetup();
	    if (methodReturnResult.contains(failTest)) {
		return methodReturnResult;
	    }

	    // Search for Provider
	    methodReturnResult = DischargeCompleteBaseLibrary
		    .driver2SearchForValidProviderIDInProviderSetup(providerID);
	    if (methodReturnResult.contains(failTest)) {
		return methodReturnResult;
	    }

	    // Click 'Real Time Facility Update' link on Provider Set up page
	    methodReturnResult = FunctionLibrary.driver2ClickAndWait(locatorRealtimeFacilityUpdate,
		    nameRealtimeFacilityUpdate);
	    if (methodReturnResult.contains(failTest))
		return failTest + " : Search result for Provider ID didnot appear";

	    // Wait for the new window to appear
	    FunctionLibrary.driver2WaitForNewWindow(1);

	    // Switch to New Window
	    methodReturnResult = FunctionLibrary.driver2SwitchToPopupWindow();
	    if (methodReturnResult.contains(failTest))
		return methodReturnResult;

	    // Verify we navigated to provider profile Page
	    expectedTitle = providerProfilePageTitle;
	    methodReturnResult = FunctionLibrary.driver2AssertTitle(expectedTitle);
	    if (methodReturnResult.contains(failTest))
		return methodReturnResult;

	    // Click 'create training patients' link
	    methodReturnResult = FunctionLibrary.driver2ClickLink(locatorCreateTrainingPtnt, nameCreateTrainingPtnt);
	    if (methodReturnResult.contains(failTest))
		return methodReturnResult;

	    // Accept alert with message "Are you sure you want to CREATE
	    // TRAINING PATIENTS?"
	    methodReturnResult = FunctionLibrary
		    .driver2AcceptAlert("Are you sure you want to CREATE TRAINING PATIENTS?");
	    if (methodReturnResult.contains(failTest))
		return methodReturnResult;

	    // Wait for the new window to appear
	    FunctionLibrary.driver2WaitForNewWindow(2);

	    // Switch to New Window
	    methodReturnResult = FunctionLibrary.driver2SwitchToPopupWindow();
	    if (methodReturnResult.contains(failTest))
		return methodReturnResult;

	    String networkId = testData.getCellData("loginToDischargeCentral", "NetworkID", 1);
	    String networkLinkXPATH = "//a[contains(@href,'" + networkId + "')]";

	    // Click Network link in which Training patients to be added
	    methodReturnResult = FunctionLibrary.driver2ClickLink(By.xpath(networkLinkXPATH), "'Network' link");
	    if (methodReturnResult.contains(failTest))
		return methodReturnResult;

	    // Verify Success message
	    methodReturnResult = FunctionLibrary.assertText("'Training patient created message' text", FunctionLibrary
		    .driver2RetrieveText(By.xpath("//h3"), Constants.TRAINING_PATIENTS_CREATED_SUCCESS_MSG).trim(),
		    Constants.TRAINING_PATIENTS_CREATED_SUCCESS_MSG);
	    if (methodReturnResult.contains(failTest))
		return methodReturnResult;

	    // Click 'Close' button
	    methodReturnResult = FunctionLibrary.driver2ClickLink(By.id("close"), "'Close' button");
	    if (methodReturnResult.contains(failTest))
		return methodReturnResult;

	    FunctionLibrary.refreshPage();

	    // Expand Inpatient By Unit link
	    methodReturnResult = FunctionLibrary.clickLink(locatorInpatientbyUnit, nameInpatienbyUnit);
	    if (methodReturnResult.contains(failTest)) {
		return methodReturnResult;
	    }

	    // Wait for the result to appear
	    if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
		FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
	    }

	    // Expand the dropdown
	    methodReturnResult = FunctionLibrary.clickLink(locatorInpatientDropdown, nameInpatientDropdown);
	    if (methodReturnResult.contains(failTest)) {
		return methodReturnResult;
	    }

	    // Select training option from the dropdown
	    methodReturnResult = FunctionLibrary.clickLink(locatorInpatientOption, nameInpatientOption);
	    if (methodReturnResult.contains(failTest)) {
		return methodReturnResult;
	    }
	    // Click on search button
	    methodReturnResult = FunctionLibrary.clickLink(locatorSearchbutton, nameSearchbutton);
	    if (methodReturnResult.contains(failTest)) {
		return methodReturnResult;
	    }

	    // Wait for the result to appear
	    if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
		FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
	    }

	    // Fluent wait for Training Patient Records to populate
	    // FunctionLibrary.fluentWait(".//*[@id='searchResultSection']/div[2]//table//tbody[contains(@class,'data')]/tr");

	    Thread.sleep(10000L);
	    try {
		// Check for the presence of training patients in Search
		// Patients page
		totalPatient = driver
			.findElements(By.xpath(
				".//*[@id='searchResultSection']/div[2]//table//tbody[contains(@class,'data')]/tr"))
			.size();
		for (int i = 0; i < 3; i++) {
		    if (totalPatient == 0) {
			FunctionLibrary.refreshPage();

			Thread.sleep(5000L);

			// Expand Inpatient By Unit link
			methodReturnResult = FunctionLibrary.clickLink(locatorInpatientbyUnit, nameInpatienbyUnit);
			if (methodReturnResult.contains(failTest)) {
			    return methodReturnResult;
			}

			// Wait for the result to appear
			if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
			    FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
			}

			// Expand the dropdown
			methodReturnResult = FunctionLibrary.clickLink(locatorInpatientDropdown, nameInpatientDropdown);
			if (methodReturnResult.contains(failTest)) {
			    return methodReturnResult;
			}

			// Select training option from the dropdown
			methodReturnResult = FunctionLibrary.clickLink(locatorInpatientOption, nameInpatientOption);
			if (methodReturnResult.contains(failTest)) {
			    return methodReturnResult;
			}
			// Click on search button
			methodReturnResult = FunctionLibrary.clickLink(locatorSearchbutton, nameSearchbutton);
			if (methodReturnResult.contains(failTest)) {
			    return methodReturnResult;
			}

			// Wait for the result to appear
			if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText)) {
			    FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);
			}
			if (driver.findElements(By.xpath(
				".//*[@id='searchResultSection']/div[2]//table//tbody[contains(@class,'data')]/tr"))
				.size() > 0) {
			    break;
			}
		    }
		}

	    } catch (Throwable t) {
		return failTest + " : Error came while getting Training Patients from workbook panel table : "
			+ t.getMessage();
	    }
	} else {
	    APPLICATION_LOGS.debug("Training Patients are already present in Search Patients page.");
	}

	return "Pass : Patient pre requisite succesfully done for Training Patients.";

    }

    // Remove Training Patient from Business Central
    public static String removeTrainingPatientFromBC(String providerID)
	    throws InterruptedException, BiffException, IOException {

	APPLICATION_LOGS.debug("Removing Training Patients from Business Central..");

	// Navigate and login to Business Central
	methodReturnResult = DischargeCompleteBaseLibrary.driver2NavigateToBusinessCentral(1);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Navigate to Provider Set Up page
	methodReturnResult = DischargeCompleteBaseLibrary.driver2NavigateToProviderSetup();
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Search for Provider
	methodReturnResult = DischargeCompleteBaseLibrary.driver2SearchForValidProviderIDInProviderSetup(providerID);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Click 'Real Time Facility Update' link on Provider Set up page
	methodReturnResult = FunctionLibrary.driver2ClickAndWait(locatorRealtimeFacilityUpdate,
		nameRealtimeFacilityUpdate);
	if (methodReturnResult.contains(failTest))
	    return failTest + " : Search result for Provider ID didnot appear";

	// Wait for the new window to appear
	FunctionLibrary.driver2WaitForNewWindow(1);

	// Switch to New Window
	methodReturnResult = FunctionLibrary.driver2SwitchToPopupWindow();
	if (methodReturnResult.contains(failTest))
	    return methodReturnResult;

	// Verify we navigated to provider profile Page
	expectedTitle = providerProfilePageTitle;
	methodReturnResult = FunctionLibrary.driver2AssertTitle(expectedTitle);
	if (methodReturnResult.contains(failTest))
	    return methodReturnResult;

	// Click 'remove training patients' link
	methodReturnResult = FunctionLibrary.driver2ClickLink(locatorRemoveTrainingPtnt, nameRemoveTrainingPtnt);
	if (methodReturnResult.contains(failTest))
	    return methodReturnResult;

	// Accept alert with message "Are you sure you want to REMOVE
	// TRAINING PATIENTS?"
	methodReturnResult = FunctionLibrary.driver2AcceptAlert("Are you sure you want to REMOVE TRAINING PATIENTS?");
	if (methodReturnResult.contains(failTest))
	    return methodReturnResult;

	// Wait for the new window to appear
	FunctionLibrary.driver2WaitForNewWindow(2);

	// Switch to New Window
	methodReturnResult = FunctionLibrary.driver2SwitchToPopupWindow();
	if (methodReturnResult.contains(failTest))
	    return methodReturnResult;

	// Verify Success message after removing patients
	methodReturnResult = FunctionLibrary.assertText(
		"'Training patient removed message' text", FunctionLibrary
			.driver2RetrieveText(By.xpath("//h3"), Constants.TRAINING_PATIENTS_REMOVED_SUCCESS_MSG).trim(),
		Constants.TRAINING_PATIENTS_REMOVED_SUCCESS_MSG);
	if (methodReturnResult.contains(failTest))
	    return methodReturnResult;

	// Click 'Close' button
	methodReturnResult = FunctionLibrary.driver2ClickLink(By.id("close"), "'Close' button");
	if (methodReturnResult.contains(failTest))
	    return methodReturnResult;

	return "Pass : Training patients successfully removed from Business Central.";

    }

    // Getting row number from Search Patient Workbook panel table of Training
    // Patient which has no Primary Contact.
    public static String getTrainingPtntRowWithNoPrimaryContact() {

	APPLICATION_LOGS.debug(
		"Getting row number from Search Patient Workbook panel table of Training Patient which has no Primary Contact..");

	String rowNo = "All Training Patients have Primary Contact assigned.";
	try {
	    int tableSize = driver
		    .findElements(By.xpath("//div[@id='workbookPanelTable']//tbody[contains(@class,'data')]/tr"))
		    .size();

	    for (int i = 1; i <= tableSize; i++) {
		String rowXpath = "//div[@id='workbookPanelTable']//tbody[contains(@class,'data')]/tr[" + i + "]/td[8]";
		if (driver.findElement(By.xpath(rowXpath)).getText().equals("")) {
		    rowNo = String.valueOf(i);
		    break;
		}

	    }

	} catch (Throwable t) {
	    APPLICATION_LOGS
		    .debug("Error came while working with Search Patient page Workbook table." + t.getMessage());
	    return failTest + " : Error came while working with Search Patient page Workbook table." + t.getMessage();
	}
	System.out.println(rowNo);

	return rowNo;
    }

    // Get incomplete patients count for Case Managers present in Recently
    // Discharge page Case Manager Dropdown...
    public static String getIncomplPatntCountForCMOnRecDschrgePage(String facilityId) throws SQLException {
	APPLICATION_LOGS.debug(
		"Retrieving incomplete patients count for Case Managers present in Recently Discharge page Case Manager Dropdown...");

	String CMCount = null;
	int totalCount = 0;
	// Query String to fetch incomplete patients count
	String query = "WITH inCompleteByBooking AS" + " ( "
		+ "SELECT discharge_id, CASE WHEN MIN(complete_count) = 0 THEN 'N' ELSE 'Y' END AS complete "
		+ "FROM ( " + "SELECT	locus_id, " + "discharge_id, "
		+ "CASE WHEN booked_flag = 1 OR canceled_flag = 1 THEN 1 ELSE 0 END complete_count " + "FROM ( "
		+ "SELECT	locus_id, " + "discharge_id, " + "booked_flag, "
		+ "CASE WHEN locus_count = canceled_count THEN 1 ELSE 0 END canceled_flag " + "FROM ( "
		+ "SELECT br.locus_id, " + "br.discharge_id, " + "COUNT(br.locus_id) locus_count, "
		+ "COUNT(CASE WHEN br.request_state_id = 5 THEN br.locus_id END) canceled_count, "
		+ "MAX(CASE WHEN br.request_state_id = 2 THEN 1 ELSE 0 END) booked_flag " + "FROM booking_request br "
		+ "JOIN discharge d on d.id = br.discharge_id " + "WHERE d.active = 'yes' " + "AND d.facility_id = "
		+ facilityId + " AND d.recently_discharged IS NOT NULL AND sysdate < d.recently_discharged+7 "
		+ "GROUP BY br.locus_id, br.discharge_id " + ") " + ") " + ") " + "GROUP BY discharge_id " + ") "
		+ "SELECT " + "COUNT(DISTINCT id) AS patCount " + "FROM ( " + "SELECT "
		+ "dschr.security_id secid, dschr.id " + "FROM discharge dschr "
		+ "JOIN facility hosp ON dschr.facility_id = hosp.id "
		+ "LEFT JOIN discharge_state ds ON dschr.discharge_status_code = ds.id "
		+ "LEFT JOIN reference ref1 ON dschr.ref_discharge_disposition = ref1.id "
		+ "LEFT JOIN reference ref2 ON dschr.ref_discharge_delay_reason = ref2.id "
		+ "LEFT JOIN inCompleteByBooking incomplete ON dschr.id = incomplete.discharge_id "
		+ "WHERE dschr.active = 'yes' " + "AND dschr.facility_id = " + facilityId
		+ " AND dschr.recently_discharged IS NOT NULL AND sysdate < dschr.recently_discharged + 7 " + "AND ( "
		+ "(dschr.no_services = 'no' AND inComplete.discharge_id IS NOT NULL AND incomplete.complete = 'N') "
		+ "OR ds.name IS NULL " + "OR(ref1.external_value IS NULL AND LOWER(hosp.enable_user_dispo) = 'yes') "
		+ "OR(TRUNC(dschr.estimated_discharge) < TRUNC(dschr.actual_discharge) AND ref2.external_value IS NULL) "
		+ ")" + "UNION ALL " + "SELECT " + "dschr_scbr.security_id secid, dschr.id " + "FROM discharge dschr "
		+ "JOIN discharge_subscriber dschr_scbr ON dschr.id = dschr_scbr.discharge_id "
		+ "JOIN facility hosp ON dschr.facility_id = hosp.id "
		+ "LEFT JOIN discharge_state ds ON dschr.discharge_status_code = ds.id "
		+ "LEFT JOIN reference ref1 ON dschr.ref_discharge_disposition = ref1.id "
		+ "LEFT JOIN reference ref2 ON dschr.ref_discharge_delay_reason = ref2.id "
		+ "LEFT JOIN inCompleteByBooking  incomplete ON dschr.id = incomplete.discharge_id "
		+ "WHERE dschr.active = 'yes' " + "AND dschr.facility_id = " + facilityId
		+ " AND dschr.recently_discharged IS NOT NULL AND sysdate < dschr.recently_discharged + 7 " + "AND ( "
		+ "(dschr.no_services = 'no' AND inComplete.discharge_id IS NOT NULL AND incomplete.complete = 'N') "
		+ "OR ds.name IS NULL " + "OR(ref1.external_value IS NULL AND LOWER(hosp.enable_user_dispo) = 'yes') "
		+ "OR(TRUNC(estimated_discharge) < TRUNC(dschr.actual_discharge) AND ref2.external_value IS NULL) "
		+ ") " + ") " + "GROUP BY secid";
	System.out.println(query);

	try {
	    // Connecting to Database
	    con = DriverManager.getConnection(connectionString, dbUsername, dbPassword);
	    // Create a Statement object for sending SQL statements to the
	    // database
	    stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
	    // Execute the query
	    rs = stmt.executeQuery(query);
	    while (rs.next()) {
		CMCount = rs.getString(1);
		totalCount = totalCount + Integer.parseInt(CMCount);
	    }
	} catch (SQLException sqlException) {
	    APPLICATION_LOGS.debug("Error came while trying to fetch number of incomplete patients count : "
		    + sqlException.getMessage());

	}
	return String.valueOf(totalCount);
    }

    // Add new patient in Discharge
    public static String addPatient(int dateDiff)
	    throws InterruptedException, RowsExceededException, BiffException, WriteException, IOException {

	APPLICATION_LOGS.debug("Adding new patient in Discharge....");

	String patientFirstName = "AutoPatient";
	String patientLastName = "DoNotUse";
	String RUN_DATE = TestUtil.now("dd.MMM.yy hh.mm.ss").toString();

	// Click on Add patient button
	methodReturnResult = FunctionLibrary.clickAndWait(locatorAddPatientBtn, nameAddPatientBtn);
	if (methodReturnResult.contains(failTest)) {
	    return methodReturnResult;
	}

	// Verify whether the page is navigated to Add patient page
	if (!FunctionLibrary.isElementPresent(locatorPatientForm, namePatientForm)) {
	    methodReturnResult = FunctionLibrary.clickAndWait(locatorAddPatientBtn, nameAddPatientBtn);
	    if (methodReturnResult.contains(failTest)) {
		return methodReturnResult;
	    }
	}

	// Input the admission date as today's date
	Format formatter = new SimpleDateFormat("MMMM");

	// Input valid first name
	methodReturnResult = FunctionLibrary.clearAndInput(locatorPatientFirstNameInput, namePatientFirstNameInput,
		patientFirstName + " " + RUN_DATE);
	if (methodReturnResult.contains(failTest))
	    return methodReturnResult;

	Thread.sleep(1000L);

	// Input space in patient's last name
	methodReturnResult = FunctionLibrary.clearAndInput(locatorPatientLastNameInput, namePatientLastNameInput,
		patientLastName + " " + RUN_DATE);
	if (methodReturnResult.contains(failTest))
	    return methodReturnResult;

	Thread.sleep(1000L);

	// Select the gender from dropdown
	methodReturnResult = FunctionLibrary.selectValueByIndex(locatorGenderDropdown, 1, nameGenderDropdown);
	if (methodReturnResult.contains(failTest))
	    return methodReturnResult;

	// Input Random MRN
	String randomMRNNo = FunctionLibrary.generateRandomNumber(8);
	methodReturnResult = FunctionLibrary.clearAndInput(locatorMRNInput, nameMRNInput, randomMRNNo);
	if (methodReturnResult.contains("Fail")) {
	    return methodReturnResult;
	}

	Thread.sleep(1000L);

	// Input Random Account Number
	methodReturnResult = FunctionLibrary.clearAndInput(locatorAccountNoInput, nameAccountNoInput, randomMRNNo);
	if (methodReturnResult.contains("Fail")) {
	    return methodReturnResult;
	}

	Thread.sleep(1000L);

	// Select the Admit type from dropdown
	methodReturnResult = FunctionLibrary.selectValueByIndex(locatorAdmitTypeDropdown, 1, nameAdmitTypeDropdown);
	if (methodReturnResult.contains(failTest))
	    return methodReturnResult;

	Thread.sleep(1000L);

	// Get the DOB
	Date newDOBDate = FunctionLibrary.subtractDate(370);

	String dobMonth = formatter.format(newDOBDate);
	Format yearFormatter = new SimpleDateFormat("yyyy");
	String dobYear = yearFormatter.format(newDOBDate);
	Format dateFormatter = new SimpleDateFormat("d");
	String dobDate = dateFormatter.format(newDOBDate);

	// Select the Calendar
	methodReturnResult = FunctionLibrary.clickLink(locatorDOBCalendar, nameDOBCalendar);
	if (methodReturnResult.contains(failTest))
	    return methodReturnResult;

	// Click on month link
	methodReturnResult = FunctionLibrary.clickLink(locatorDOBMonthLink, nameDischargeMonthLink);
	if (methodReturnResult.contains(failTest))
	    return methodReturnResult;

	// Select the month from dropdown
	methodReturnResult = FunctionLibrary.selectValueByVisibleText(locatorDOBMonthDropdown, dobMonth,
		nameDOBMonthDropdown);
	if (methodReturnResult.contains(failTest))
	    return methodReturnResult;

	// Type the year in year textbox
	methodReturnResult = FunctionLibrary.clearAndInput(locatorDOBYearInput, nameDOBYearInput, dobYear);
	if (methodReturnResult.contains(failTest))
	    return methodReturnResult;

	// Click on ok button
	methodReturnResult = FunctionLibrary.clickLink(locatorDOBSubmitBtn, nameDOBSubmitBtn);
	if (methodReturnResult.contains(failTest))
	    return methodReturnResult;

	// Select the date
	methodReturnResult = FunctionLibrary.clickLink(By.linkText(dobDate), "Birth date");
	if (methodReturnResult.contains(failTest))
	    return methodReturnResult;

	Thread.sleep(3000L);

	String unitRoom = FunctionLibrary.generateRandomString(5);

	// Input Unit Room name
	methodReturnResult = FunctionLibrary.clearAndInput(locatorUnitRoom, nameUnitRoom, unitRoom);
	if (methodReturnResult.contains("Fail")) {
	    return methodReturnResult;
	}

	// Click on Admit date
	// Select the Calendar
	methodReturnResult = FunctionLibrary.clickLink(locatorAdmitDateCalendar, nameAdmitDateCalendar);
	if (methodReturnResult.contains(failTest))
	    return methodReturnResult;

	String admitDate = String.valueOf(dateDiff);

	// Select date
	methodReturnResult = FunctionLibrary.clickLink(By.linkText(admitDate), "Date in Calendar");
	if (methodReturnResult.contains(failTest))
	    return methodReturnResult;

	// Select the Calendar
	methodReturnResult = FunctionLibrary.clickLink(locatorDischargeDateCalendar, nameDischargeDateCalendar);
	if (methodReturnResult.contains(failTest))
	    return methodReturnResult;

	String estDate = String.valueOf(dateDiff + 2);

	// Select the date
	methodReturnResult = FunctionLibrary.clickLink(By.linkText(estDate), "Estimated Discharge Date");
	if (methodReturnResult.contains(failTest))
	    return methodReturnResult;

	Thread.sleep(3000L);

	// Select the value from primary diagnosis
	methodReturnResult = FunctionLibrary.selectValueByIndex(locatorPrimaryDiagnosisDrpdwn, 1,
		namePrimaryDiagnosisDrpdwn);
	if (methodReturnResult.contains(failTest))
	    return methodReturnResult;

	Thread.sleep(1000L);

	// Select the insurance plan ID
	methodReturnResult = FunctionLibrary.selectValueByIndex(locatorPlanId, 1, namePlanId);
	if (methodReturnResult.contains(failTest))
	    return methodReturnResult;

	Thread.sleep(1000L);

	// Click on save button
	methodReturnResult = FunctionLibrary.clickAndWait(locatorSaveBtn, nameSaveBtn);
	if (methodReturnResult.contains(failTest))
	    return methodReturnResult;

	Thread.sleep(1000L);

	// Wait for the result to appear
	if (FunctionLibrary.isElementDisplayed(locatorProcessingDiv, nameProcessingText))
	    FunctionLibrary.waitForElementToDisappear(locatorProcessingDiv, nameProcessingText);

	// Check if the duplicate patient dialogue is present or not
	if (FunctionLibrary.isElementDisplayed(locatorDuplicatePatientOverlay, nameDuplicatePatientOverlay)) {
	    methodReturnResult = FunctionLibrary.clickAndWait(locatorSavePatientBtn, nameSavePatientBtn);
	    if (methodReturnResult.contains(failTest)) {
		return methodReturnResult;
	    }
	}

	// Assert the success text
	String addPatientSuccessText = FunctionLibrary.retrieveText(locatorAddPatientSuccessText,
		nameAddPatientSuccessText);
	if (addPatientSuccessText.contains(failTest))
	    return addPatientSuccessText;

	methodReturnResult = FunctionLibrary.assertText("Add Patient Success Message", addPatientSuccessText,
		Constants.ADD_PATIENT_SUCCESS_MSG);
	if (methodReturnResult.contains(failTest))
	    return methodReturnResult;

	return "Pass : Successfully added a new patient in Discharge Central.";
    }

    public static String getDischargeStatusToolTip(String code) {

	ResultSet rs = null;
	Connection con = null;
	Statement stmt = null;
	String dischargeStatusDes = null;

	APPLICATION_LOGS.debug("Getting the discharge status for code = " + code);

	String query = "SELECT description FROM discharge_state WHERE code = " + code;

	try {

	    // Connecting to Database
	    con = java.sql.DriverManager.getConnection(connectionString, dbUsername, dbPassword);

	    con.setAutoCommit(false);

	    // Create a Statement object for sending SQL statements to the
	    // database
	    stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);

	    // Execute the query
	    rs = stmt.executeQuery(query);
	    while (rs.next()) {
		dischargeStatusDes = rs.getString(1);

	    }
	}

	catch (SQLException sqlException) {

	    APPLICATION_LOGS
		    .debug("Error came trying to retrieve discharge status description " + sqlException.getMessage());

	}

	APPLICATION_LOGS.debug("Discharge status for the code = " + code + " is retrieved");

	return dischargeStatusDes;
    }

    // Move patient from Rececntly Discharge page to Active workbook by setting
    // Discharge date as empty by calling loadHL7 stored procedure...
    public static String movePatientToActiveWorkbook(String MRN, String accountNo, String dschrgeDate,
	    String facilityID, String facilityInterface) {

	APPLICATION_LOGS.debug(
		"Moving patient from Rececntly Discharge page to Active workbook by setting Discharge date as empty by calling loadHL7 stored procedure...");

	try {

	    // Connecting to Database
	    Connection con = java.sql.DriverManager.getConnection(connectionString, dbUsername, dbPassword);

	    // Set auto commit as false
	    con.setAutoCommit(false);

	    // Create a Statement object for sending SQL statements to the
	    // database
	    Statement stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);

	    // Call the store procedure
	    CallableStatement storedProc = con
		    .prepareCall("{call TEST1.loadhl7(pidmrn=>?,pidaccount=>?,pv1DischargeDate=>?,"
			    + "mshfacility=>?,in_facility_id=>?,zedinterfacename=>?)}");

	    // Set parameters
	    storedProc.setString(1, MRN);
	    storedProc.setString(2, accountNo);
	    storedProc.setString(3, dschrgeDate);
	    storedProc.setString(4, facilityID);
	    storedProc.setString(5, facilityID);
	    storedProc.setString(6, facilityInterface);

	    // Execute store procedure
	    storedProc.execute();
	    storedProc.close();
	    con.commit();

	}

	catch (SQLException sqlException) {

	    System.out
		    .println("Error came while trying to call loadHL7 stored procedure : " + sqlException.getMessage());
	    return failTest + " : Error came while trying to call loadHL7 stored procedure.";
	}

	return "Pass : Successfully moved patient from Rececntly Discharge page to Active workbook by setting Discharge date as empty by calling loadHL7 stored procedure.";
    }

}
